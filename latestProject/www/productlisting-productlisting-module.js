(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["productlisting-productlisting-module"],{

/***/ "./src/app/productlisting/productlisting.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/productlisting/productlisting.module.ts ***!
  \*********************************************************/
/*! exports provided: ProductlistingPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductlistingPageModule", function() { return ProductlistingPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _productlisting_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./productlisting.page */ "./src/app/productlisting/productlisting.page.ts");







var routes = [
    {
        path: '',
        component: _productlisting_page__WEBPACK_IMPORTED_MODULE_6__["ProductlistingPage"]
    }
];
var ProductlistingPageModule = /** @class */ (function () {
    function ProductlistingPageModule() {
    }
    ProductlistingPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_productlisting_page__WEBPACK_IMPORTED_MODULE_6__["ProductlistingPage"]]
        })
    ], ProductlistingPageModule);
    return ProductlistingPageModule;
}());



/***/ }),

/***/ "./src/app/productlisting/productlisting.page.html":
/*!*********************************************************!*\
  !*** ./src/app/productlisting/productlisting.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Products</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-button color=\"dark\" (click)=\"goBack()\">\n        <ion-icon name=\"arrow-back\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-list *ngFor= \"let products of productArray\">\n    <ion-item (click)=\"goToDetail(products.id)\">\n        <img class=\"item-image\" src=\"https://www.niletechinnovations.com/eq8tor{{products.post_image_url}}\">\n      <ion-label no-border>{{products.post_title}}</ion-label>\n    </ion-item>\n  </ion-list>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/productlisting/productlisting.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/productlisting/productlisting.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".item-image {\n  width: 15%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL3Byb2R1Y3RsaXN0aW5nL3Byb2R1Y3RsaXN0aW5nLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFVBQVUsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3RsaXN0aW5nL3Byb2R1Y3RsaXN0aW5nLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pdGVtLWltYWdle1xuICAgIHdpZHRoOiAxNSU7XG59Il19 */"

/***/ }),

/***/ "./src/app/productlisting/productlisting.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/productlisting/productlisting.page.ts ***!
  \*******************************************************/
/*! exports provided: ProductlistingPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductlistingPage", function() { return ProductlistingPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Service_loader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Service/loader.service */ "./src/app/Service/loader.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _Service_homepage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Service/homepage.service */ "./src/app/Service/homepage.service.ts");
/* harmony import */ var _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Service/aler-service.service */ "./src/app/Service/aler-service.service.ts");







var ProductlistingPage = /** @class */ (function () {
    function ProductlistingPage(alert, route, homepageService, router, navCtrl, loadingService) {
        var _this = this;
        this.alert = alert;
        this.route = route;
        this.homepageService = homepageService;
        this.router = router;
        this.navCtrl = navCtrl;
        this.loadingService = loadingService;
        this.route.queryParams.subscribe(function (params) {
            console.log("test", JSON.stringify(params.id));
            _this.getProducts(params.id);
        });
    }
    ProductlistingPage.prototype.ngOnInit = function () {
    };
    ProductlistingPage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    ProductlistingPage.prototype.getProducts = function (itemName) {
        var _this = this;
        console.log("getProducts", itemName);
        this.homepageService.getCategoryProducts(itemName).subscribe(function (response) {
            var resp = response.data;
            _this.productArray = resp.products;
        }, function (err) {
            _this.alert.myAlertMethod(err.message, "Please try again later.", function (data) {
                console.log("hello showProducts");
            });
            _this.loadingService.dismiss();
        });
    };
    ProductlistingPage.prototype.goToDetail = function (id) {
        console.log("product id", id);
        for (var _i = 0, _a = this.productArray; _i < _a.length; _i++) {
            var item = _a[_i];
            if (item.id == id) {
                var navigationextras = {
                    queryParams: {
                        detail: JSON.stringify(item)
                    }
                };
                console.log(item);
                this.router.navigate(['product-detail'], navigationextras);
            }
        }
        // let data = this.productArray[id];
        // console.log("data data",data);
    };
    ProductlistingPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-productlisting',
            template: __webpack_require__(/*! ./productlisting.page.html */ "./src/app/productlisting/productlisting.page.html"),
            styles: [__webpack_require__(/*! ./productlisting.page.scss */ "./src/app/productlisting/productlisting.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_Service_aler_service_service__WEBPACK_IMPORTED_MODULE_6__["AlerServiceService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _Service_homepage_service__WEBPACK_IMPORTED_MODULE_5__["HomepageService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], _Service_loader_service__WEBPACK_IMPORTED_MODULE_3__["LoaderService"]])
    ], ProductlistingPage);
    return ProductlistingPage;
}());



/***/ })

}]);
//# sourceMappingURL=productlisting-productlisting-module.js.map