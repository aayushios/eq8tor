(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"],{

/***/ "./src/app/login/login.module.ts":
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");







var routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());



/***/ }),

/***/ "./src/app/login/login.page.html":
/*!***************************************!*\
  !*** ./src/app/login/login.page.html ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-button color=\"dark\" (click)=\"goBack()\">\n        <ion-icon name=\"arrow-back\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content padding>\n  <ion-label class=\"label\">Welcome</ion-label>\n  <div class=\"text\">\n    <p>Sign in to continue</p>\n    <button class=\"signUp\" ion-button (click)=\"goToSignUp()\">SignUp</button>\n  </div>\n  <form [formGroup]=\"credentialsForm\">\n    <div class=\"view-form\">\n      <ion-item class=\"user-input\" lines=\"none\">\n        <ion-input class=\"username form-control\" placeholder=\"Username / Email Address\" formControlName=\"email\">\n        </ion-input>\n      </ion-item>\n      <p class=\"validate-msg-alert\"\n        *ngIf=\"credentialsForm.controls.email.touched  && (!credentialsForm.controls.email.valid)\">\n        Please enter mandatory field\n      </p>\n      <ion-item class=\"password-input\" lines=\"none\">\n        <ion-input class=\"password form-control\" (keydown.space)=\"$event.preventDefault();\" [type]=\"passwordType1\" placeholder=\"Password\"\n          formControlName=\"password\"></ion-input>\n        <button class=\"showBtn\" [name]=\"passwordIcon\" item-right (click)='hideShowPassword()'>{{passwordIcon}}</button>\n      </ion-item>\n      <ion-item class=\"submit-btn\" lines=\"none \">\n        <ion-button class=\"Login\" type=\"submit\" [disabled]=\"!credentialsForm.valid\" (click)=\"onFormSubmit(credentialsForm.value)\">Login</ion-button>\n      </ion-item>\n    </div>\n  </form>\n  <div>\n    <!-- <ion-button class=\"Login\" (click)='login()' expand=\"block\">Login</ion-button> -->\n    <button ion-button class=\"forgot-password\" (click)='presentAlertPrompt()'>Forgot Password?</button>\n  </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/login/login.page.scss":
/*!***************************************!*\
  !*** ./src/app/login/login.page.scss ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ion-label.label {\n  display: block;\n  padding-top: 20px;\n  margin-left: 0px;\n  font-size: 20px;\n  font-weight: 600; }\n\n:host .item-interactive.item-has-focus {\n  --highlight-background:transparent;\n  height: auto; }\n\n:host .text {\n  margin-left: 0px;\n  display: block;\n  margin-top: -10px;\n  color: lightgray;\n  font-size: 14px;\n  font-weight: 500; }\n\n:host button.signUp {\n  float: right;\n  margin-top: -44px;\n  background: white;\n  font-size: 14px;\n  outline: none;\n  color: #b31117;\n  font-weight: 500; }\n\n:host button.showBtn {\n  margin-right: 10px;\n  background: #003a54;\n  color: white;\n  height: 20px;\n  position: absolute;\n  right: 5px;\n  z-index: 9;\n  font-weight: 500;\n  border-radius: 2px;\n  font-size: 12px; }\n\n:host ion-button.Login {\n  --border-radius: 2px;\n  margin-top: 30px;\n  height: 40px;\n  --background: #b31117;\n  color: white;\n  width: 100%;\n  font-weight: 600;\n  --padding-start: 0px !important;\n  --background-activated: var(--ion-color-shade, none);\n  --background-focused: var(--background); }\n\n:host ion-input.username {\n  border: 0.5px solid gray; }\n\n:host ion-input.password {\n  border: 0.5px solid gray; }\n\n:host .user-input,\n:host .password-input {\n  --inner-border-width: 0px 0px 0px 0px; }\n\n:host .user-input,\n:host .password-input {\n  --inner-border-width: 0px 0px 0px 0px;\n  margin-right: 0px;\n  margin-bottom: 8px;\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n  --border-color: #fff;\n  --padding-end: 0; }\n\n:host .user-input .item-highlight,\n:host .password-input .item-highlight {\n  background: transparent !important; }\n\n:host .username,\n:host .password {\n  --padding-start: 8px; }\n\n:host ion-item.submit-btn {\n  --padding-start: 0;\n  --inner-padding-end: 0; }\n\n:host .forgot-password {\n  margin-left: auto;\n  margin-right: auto;\n  display: block;\n  background: none;\n  outline: none;\n  margin-top: 20px;\n  font-size: 14px;\n  font-weight: 500; }\n\n:host .validate-msg-alert {\n  color: #FF545E;\n  font-size: 14px;\n  text-align: left;\n  padding: 0px 0;\n  margin-bottom: 10px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUVRLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixnQkFBZ0IsRUFBQTs7QUFOeEI7RUFVUSxrQ0FBdUI7RUFDdkIsWUFBWSxFQUFBOztBQVhwQjtFQWdCUSxnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGdCQUFnQixFQUFBOztBQXJCeEI7RUEwQlEsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGFBQWE7RUFDYixjQUFjO0VBQ2QsZ0JBQWdCLEVBQUE7O0FBaEN4QjtFQW9DUSxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixVQUFVO0VBQ1YsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixlQUFlLEVBQUE7O0FBN0N2QjtFQWlEUSxvQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixxQkFBYTtFQUNiLFlBQVk7RUFDWixXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLCtCQUFnQjtFQUNoQixvREFBdUI7RUFDdkIsdUNBQXFCLEVBQUE7O0FBMUQ3QjtFQStEUSx3QkFBd0IsRUFBQTs7QUEvRGhDO0VBbUVRLHdCQUF3QixFQUFBOztBQW5FaEM7O0VBd0VRLHFDQUFxQixFQUFBOztBQXhFN0I7O0VBNkVRLHFDQUFxQjtFQUNyQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLG9CQUFnQjtFQUNoQix3QkFBb0I7RUFDcEIsb0JBQWU7RUFDZixnQkFBYyxFQUFBOztBQW5GdEI7O0VBdUZRLGtDQUFpQyxFQUFBOztBQXZGekM7O0VBMkZjLG9CQUFnQixFQUFBOztBQTNGOUI7RUE2RlEsa0JBQWdCO0VBQ2hCLHNCQUFvQixFQUFBOztBQTlGNUI7RUFpR1EsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGdCQUFnQixFQUFBOztBQXhHeEI7RUE0R1EsY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLG1CQUFtQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG46aG9zdCB7XG4gICAgaW9uLWxhYmVsLmxhYmVsIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIHBhZGRpbmctdG9wOiAyMHB4O1xuICAgICAgICBtYXJnaW4tbGVmdDogMHB4O1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgfVxuXG4gICAgLml0ZW0taW50ZXJhY3RpdmUuaXRlbS1oYXMtZm9jdXMge1xuICAgICAgICAtLWhpZ2hsaWdodC1iYWNrZ3JvdW5kOnRyYW5zcGFyZW50O1xuICAgICAgICBoZWlnaHQ6IGF1dG87XG5cbiAgIH1cblxuICAgIC50ZXh0IHtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIG1hcmdpbi10b3A6IC0xMHB4O1xuICAgICAgICBjb2xvcjogbGlnaHRncmF5O1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG5cbiAgICB9XG5cbiAgICBidXR0b24uc2lnblVwIHtcbiAgICAgICAgZmxvYXQ6IHJpZ2h0O1xuICAgICAgICBtYXJnaW4tdG9wOiAtNDRweDtcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgb3V0bGluZTogbm9uZTtcbiAgICAgICAgY29sb3I6ICNiMzExMTc7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgfVxuXG4gICAgYnV0dG9uLnNob3dCdG4ge1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gICAgICAgIGJhY2tncm91bmQ6ICMwMDNhNTQ7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgaGVpZ2h0OiAyMHB4O1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHJpZ2h0OiA1cHg7XG4gICAgICAgIHotaW5kZXg6IDk7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgIH1cblxuICAgIGlvbi1idXR0b24uTG9naW4ge1xuICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDJweDtcbiAgICAgICAgbWFyZ2luLXRvcDogMzBweDtcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICAtLWJhY2tncm91bmQ6ICNiMzExMTc7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIC0tcGFkZGluZy1zdGFydDogMHB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6IHZhcigtLWlvbi1jb2xvci1zaGFkZSwgbm9uZSk7XG4gICAgICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkOiB2YXIoLS1iYWNrZ3JvdW5kKTtcbiAgICBcbiAgICB9XG5cbiAgICBpb24taW5wdXQudXNlcm5hbWUge1xuICAgICAgICBib3JkZXI6IDAuNXB4IHNvbGlkIGdyYXk7XG4gICAgfVxuXG4gICAgaW9uLWlucHV0LnBhc3N3b3JkIHtcbiAgICAgICAgYm9yZGVyOiAwLjVweCBzb2xpZCBncmF5O1xuICAgIH1cblxuICAgIC51c2VyLWlucHV0LFxuICAgIC5wYXNzd29yZC1pbnB1dCB7XG4gICAgICAgIC0taW5uZXItYm9yZGVyLXdpZHRoOiAwcHggMHB4IDBweCAwcHg7XG4gICAgfVxuXG4gICAgLnVzZXItaW5wdXQsXG4gICAgLnBhc3N3b3JkLWlucHV0ICB7XG4gICAgICAgIC0taW5uZXItYm9yZGVyLXdpZHRoOiAwcHggMHB4IDBweCAwcHg7XG4gICAgICAgIG1hcmdpbi1yaWdodDogMHB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiA4cHg7XG4gICAgICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xuICAgICAgICAtLWlubmVyLXBhZGRpbmctZW5kOiAwcHg7XG4gICAgICAgIC0tYm9yZGVyLWNvbG9yOiAjZmZmO1xuICAgICAgICAtLXBhZGRpbmctZW5kOiAwO1xuICAgIH1cbiAgICAudXNlci1pbnB1dCAuaXRlbS1oaWdobGlnaHQsXG4gICAgLnBhc3N3b3JkLWlucHV0IC5pdGVtLWhpZ2hsaWdodCB7XG4gICAgICAgIGJhY2tncm91bmQ6dHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbiAgICB9XG4gICAgXG4gICAgLnVzZXJuYW1lLFxuICAgIC5wYXNzd29yZHstLXBhZGRpbmctc3RhcnQ6IDhweDt9XG4gICAgaW9uLWl0ZW0uc3VibWl0LWJ0biB7XG4gICAgICAgIC0tcGFkZGluZy1zdGFydDogMDtcbiAgICAgICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMDtcbiAgICB9XG4gICAgLmZvcmdvdC1wYXNzd29yZCB7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBiYWNrZ3JvdW5kOiBub25lO1xuICAgICAgICBvdXRsaW5lOiBub25lO1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgfVxuXG4gICAgLnZhbGlkYXRlLW1zZy1hbGVydCB7XG4gICAgICAgIGNvbG9yOiAjRkY1NDVFO1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgIHBhZGRpbmc6IDBweCAwO1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/login/login.page.ts":
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Service/aler-service.service */ "./src/app/Service/aler-service.service.ts");
/* harmony import */ var _Service_user_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Service/user-service.service */ "./src/app/Service/user-service.service.ts");
/* harmony import */ var _Service_auth_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Service/auth.service */ "./src/app/Service/auth.service.ts");
/* harmony import */ var _Service_loader_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../Service/loader.service */ "./src/app/Service/loader.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");










var LoginPage = /** @class */ (function () {
    function LoginPage(authService, userService, storage, navCtrl, alert, loadingService, alertController, router, formBuilder) {
        this.authService = authService;
        this.userService = userService;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.alert = alert;
        this.loadingService = loadingService;
        this.alertController = alertController;
        this.router = router;
        this.formBuilder = formBuilder;
        this.passwordType1 = 'password';
        this.passwordIcon = 'Show';
        this.isLoading = false;
        this.credentialsForm = this.formBuilder.group({
            email: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            password: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
    }
    LoginPage.prototype.hideShowPassword = function () {
        this.passwordType1 = this.passwordType1 === 'text' ? 'password' : 'text';
        this.passwordIcon = this.passwordIcon === 'Show' ? 'Hide' : 'Show';
    };
    LoginPage.prototype.ngOnInit = function () {
        // this.authState$ = this.authService.getAuthStateObserver().subscribe();
    };
    LoginPage.prototype.goToSignUp = function () {
        console.log("goToSignUp");
        this.router.navigateByUrl('signup');
    };
    LoginPage.prototype.onFormSubmit = function (form) {
        var _this = this;
        this.loadingService.present();
        // this.router.navigate(['home']);
        this.authService.login(form)
            .subscribe(function (res) {
            if (res.data.access_token) {
                localStorage.setItem('access_token', res.data.access_token);
                // alert("test");
                // this.storage.set('access_token',res.data.access_token);
                // this.authService.checkToken();
                _this.router.navigate(['home']);
                _this.loadingService.dismiss();
            }
        }, function (err) {
            console.log(err);
            _this.alert.myAlertMethod("Error", err.error.message, function (data) {
                console.log("error");
            });
            _this.loadingService.dismiss();
        });
    };
    LoginPage.prototype.forgotPassword = function () {
        // this.alertService.myAlertMethod("test alert","test message",data => {
        //   console.log("",data);
        // })
    };
    LoginPage.prototype.presentAlertPrompt = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                // const alert = await this.alertController.create({
                //   header: 'Prompt!',
                //   inputs: [
                //     {
                //       name: 'name1',
                //       type: 'text',
                //       placeholder: 'Placeholder 1'
                //     },
                //     {
                //       name: 'name2',
                //       type: 'text',
                //       id: 'name2-id',
                //       value: 'hello',
                //       placeholder: 'Placeholder 2'
                //     },
                //     {
                //       name: 'name3',
                //       value: 'http://ionicframework.com',
                //       type: 'url',
                //       placeholder: 'Favorite site ever'
                //     },
                //     // input date with min & max
                //     {
                //       name: 'name4',
                //       type: 'date',
                //       min: '2017-03-01',
                //       max: '2018-01-12'
                //     },
                //     // input date without min nor max
                //     {
                //       name: 'name5',
                //       type: 'date'
                //     },
                //     {
                //       name: 'name6',
                //       type: 'number',
                //       min: -5,
                //       max: 10
                //     },
                //     {
                //       name: 'name7',
                //       type: 'number'
                //     }
                //   ],
                //   buttons: [
                //     {
                //       text: 'Cancel',
                //       role: 'cancel',
                //       cssClass: 'secondary',
                //       handler: () => {
                //         console.log('Confirm Cancel');
                //       }
                //     }, {
                //       text: 'Ok',
                //       handler: data => {
                //         console.log('Confirm Ok');
                //         console.log("hello",data.name1);
                //       }
                //     }
                //   ]
                // });
                // await alert.present();
                this.router.navigate(['forgotpassword']);
                return [2 /*return*/];
            });
        });
    };
    LoginPage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.page.html */ "./src/app/login/login.page.html"),
            styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/login/login.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_Service_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"],
            _Service_user_service_service__WEBPACK_IMPORTED_MODULE_6__["UserServiceService"], _ionic_storage__WEBPACK_IMPORTED_MODULE_9__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_5__["AlerServiceService"], _Service_loader_service__WEBPACK_IMPORTED_MODULE_8__["LoaderService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ })

}]);
//# sourceMappingURL=login-login-module.js.map