(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["signup-signup-module"],{

/***/ "./src/app/signup/signup.module.ts":
/*!*****************************************!*\
  !*** ./src/app/signup/signup.module.ts ***!
  \*****************************************/
/*! exports provided: SignupPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageModule", function() { return SignupPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _signup_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./signup.page */ "./src/app/signup/signup.page.ts");







var routes = [
    {
        path: '',
        component: _signup_page__WEBPACK_IMPORTED_MODULE_6__["SignupPage"]
    }
];
var SignupPageModule = /** @class */ (function () {
    function SignupPageModule() {
    }
    SignupPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_signup_page__WEBPACK_IMPORTED_MODULE_6__["SignupPage"]]
        })
    ], SignupPageModule);
    return SignupPageModule;
}());



/***/ }),

/***/ "./src/app/signup/signup.page.html":
/*!*****************************************!*\
  !*** ./src/app/signup/signup.page.html ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-button color=\"dark\" (click)=\"goBack()\">\n        <ion-icon name=\"arrow-back\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n \n<ion-content padding>\n  <ion-label class=\"label\">Sign Up</ion-label>\n  <div class=\"text\">\n    <p>Please Sign Up it's free and always will be.</p>\n  </div>\n\n  <form [formGroup]=\"signUpForm\" (ngSubmit)=\"onFormSubmit(signUpForm.value)\">\n    <div class=\"view-form\">\n      <ion-item class=\"user-input\" lines=\"none\">\n        <ion-input class=\"username form-control\" placeholder=\"Full Name\" formControlName=\"user_display_name\">\n        </ion-input>\n      </ion-item>\n      <ion-item class=\"user-input\" lines=\"none\">\n        <ion-input class=\"username form-control\" placeholder=\"Username\" formControlName=\"user_name\">\n        </ion-input>\n      </ion-item>\n      <ion-item class=\"user-input\" lines=\"none\">\n        <ion-input class=\"username form-control\" (keydown.space)=\"$event.preventDefault();\" placeholder=\"Email Address\"\n          formControlName=\"email_id\"\n          pattern=\"[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})\">\n        </ion-input>\n      </ion-item>\n      <ion-item class=\"user-input\" lines=\"none\"> \n        <ion-input class=\"username form-control\" (keydown.space)=\"$event.preventDefault();\" placeholder=\"Password\"\n          formControlName=\"password\">\n        </ion-input>\n      </ion-item>\n      <ion-item class=\"password-input\" lines=\"none\">\n        <ion-input class=\"password form-control\" (keydown.space)=\"$event.preventDefault();\" placeholder=\"Password\"\n\n          formControlName=\"password_confirmation\"></ion-input>\n        <!-- <button class=\"showBtn\" [name]=\"passwordIcon\" item-right (click)='hideShowPassword()'>{{passwordIcon}}</button> -->\n      </ion-item>\n    </div>\n    <div class=\"text\">\n      <p>by signing up you accept the <a target=\"_blank\" class=\"text-color\" rel=\"noopener\"\n          href=\"https://ionicframework.com/docs/\">Terms of Service and privacy policy</a>.</p>\n    </div>\n    <div>\n      <ion-button class=\"sign-up\" type=\"submit\" expand=\"block\" [disabled]=\"!signUpForm.valid\">Sign Up</ion-button>\n      <!-- <button ion-button class=\"forgot-password\">Forgot Password?</button> -->\n    </div>\n  </form>\n  <div class=\"text-last\">\n    <p style=\"color: darkgrey;font-weight: 500\">Already have an account?<a style=\"color: black\"\n        [routerLink]=\"['/login']\"> Sign In</a></p>\n  </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/signup/signup.page.scss":
/*!*****************************************!*\
  !*** ./src/app/signup/signup.page.scss ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ion-label.label {\n  display: block;\n  padding-top: 20px;\n  margin-left: 0px;\n  font-size: 20px;\n  font-weight: 600; }\n\n:host .item-interactive.item-has-focus {\n  --highlight-background:transparent;\n  height: auto; }\n\n:host .text {\n  margin-left: 0px;\n  display: block;\n  margin-top: -10px;\n  color: darkgray;\n  font-size: 13px;\n  font-weight: 600; }\n\n:host .text-last {\n  text-align: center;\n  margin-top: 80px;\n  font-size: 15px; }\n\n:host .view-form {\n  padding-top: 2%; }\n\n:host .sign-up {\n  margin-left: 0px;\n  margin-right: 0;\n  --border-radius: 0px;\n  margin-top: 0;\n  margin-bottom: 0;\n  height: 40px;\n  --background: #b31117;\n  color: white;\n  font-weight: 600;\n  --background-activated: var(--ion-color-shade, none); }\n\n:host ion-input.username {\n  border: 0.5px solid gray; }\n\n:host ion-input.password {\n  border: 0.5px solid gray; }\n\n:host .user-input,\n:host .password-input {\n  --inner-border-width: 0px 0px 0px 0px; }\n\n:host .user-input .sc-ion-input-ios-h,\n:host .password-input .sc-ion-input-ios-h {\n  --padding-start: 10px;\n  --padding-end: 50px;\n  margin-left: 0px;\n  --inner-padding-end: 0px;\n  font-weight: 600;\n  font-size: small; }\n\n:host .user-input,\n:host .password-input {\n  --inner-border-width: 0px 0px 0px 0px;\n  margin-right: 0px;\n  margin-bottom: 8px;\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n  --border-color: #fff;\n  --padding-end: 0; }\n\n:host .username,\n:host .password {\n  --padding-start: 8px; }\n\n:host ion-item.submit-btn {\n  --padding-start: 0;\n  --inner-padding-end: 0; }\n\n:host .text-color {\n  color: black; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL3NpZ251cC9zaWdudXAucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRVEsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGdCQUNKLEVBQUE7O0FBUEo7RUFTUSxrQ0FBdUI7RUFDdkIsWUFBWSxFQUFBOztBQVZwQjtFQWFRLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixlQUFlO0VBQ2YsZ0JBQ0osRUFBQTs7QUFuQko7RUFzQlEsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixlQUFlLEVBQUE7O0FBeEJ2QjtFQTRCUSxlQUFlLEVBQUE7O0FBNUJ2QjtFQWdDUSxnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLG9CQUFnQjtFQUNoQixhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixxQkFBYTtFQUNiLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsb0RBQXVCLEVBQUE7O0FBekMvQjtFQTZDUSx3QkFBd0IsRUFBQTs7QUE3Q2hDO0VBaURRLHdCQUF3QixFQUFBOztBQWpEaEM7O0VBc0RRLHFDQUFxQixFQUFBOztBQXREN0I7O0VBMkRRLHFCQUFnQjtFQUNoQixtQkFBYztFQUNkLGdCQUFnQjtFQUNoQix3QkFBb0I7RUFDcEIsZ0JBQWdCO0VBQ2hCLGdCQUFnQixFQUFBOztBQWhFeEI7O0VBcUVRLHFDQUFxQjtFQUNyQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLG9CQUFnQjtFQUNoQix3QkFBb0I7RUFDcEIsb0JBQWU7RUFDZixnQkFBYyxFQUFBOztBQTNFdEI7O0VBOEVjLG9CQUFnQixFQUFBOztBQTlFOUI7RUFnRlEsa0JBQWdCO0VBQ2hCLHNCQUFvQixFQUFBOztBQWpGNUI7RUFxRlEsWUFBWSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvc2lnbnVwL3NpZ251cC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG4gICAgaW9uLWxhYmVsLmxhYmVsIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIHBhZGRpbmctdG9wOiAyMHB4O1xuICAgICAgICBtYXJnaW4tbGVmdDogMHB4O1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDBcbiAgICB9XG4gICAgLml0ZW0taW50ZXJhY3RpdmUuaXRlbS1oYXMtZm9jdXMge1xuICAgICAgICAtLWhpZ2hsaWdodC1iYWNrZ3JvdW5kOnRyYW5zcGFyZW50O1xuICAgICAgICBoZWlnaHQ6IGF1dG87XG4gICB9ICAgICAgIFxuICAgIC50ZXh0IHtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIG1hcmdpbi10b3A6IC0xMHB4O1xuICAgICAgICBjb2xvcjogZGFya2dyYXk7XG4gICAgICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMFxuICAgIH1cblxuICAgIC50ZXh0LWxhc3Qge1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIG1hcmdpbi10b3A6IDgwcHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICB9XG5cbiAgICAudmlldy1mb3JtIHtcbiAgICAgICAgcGFkZGluZy10b3A6IDIlO1xuICAgIH1cblxuICAgIC5zaWduLXVwIHtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAwO1xuICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDBweDtcbiAgICAgICAgbWFyZ2luLXRvcDogMDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICAtLWJhY2tncm91bmQ6ICNiMzExMTc7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogdmFyKC0taW9uLWNvbG9yLXNoYWRlLCBub25lKTtcbiAgICB9XG5cbiAgICBpb24taW5wdXQudXNlcm5hbWUge1xuICAgICAgICBib3JkZXI6IDAuNXB4IHNvbGlkIGdyYXk7XG4gICAgfVxuXG4gICAgaW9uLWlucHV0LnBhc3N3b3JkIHtcbiAgICAgICAgYm9yZGVyOiAwLjVweCBzb2xpZCBncmF5O1xuICAgIH1cblxuICAgIC51c2VyLWlucHV0LFxuICAgIC5wYXNzd29yZC1pbnB1dCB7XG4gICAgICAgIC0taW5uZXItYm9yZGVyLXdpZHRoOiAwcHggMHB4IDBweCAwcHg7XG4gICAgfVxuXG4gICAgLnVzZXItaW5wdXQgLnNjLWlvbi1pbnB1dC1pb3MtaCxcbiAgICAucGFzc3dvcmQtaW5wdXQgLnNjLWlvbi1pbnB1dC1pb3MtaCB7XG4gICAgICAgIC0tcGFkZGluZy1zdGFydDogMTBweDtcbiAgICAgICAgLS1wYWRkaW5nLWVuZDogNTBweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgICAgICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICBmb250LXNpemU6IHNtYWxsO1xuICAgIH1cblxuICAgIC51c2VyLWlucHV0LFxuICAgIC5wYXNzd29yZC1pbnB1dCB7XG4gICAgICAgIC0taW5uZXItYm9yZGVyLXdpZHRoOiAwcHggMHB4IDBweCAwcHg7XG4gICAgICAgIG1hcmdpbi1yaWdodDogMHB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiA4cHg7XG4gICAgICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xuICAgICAgICAtLWlubmVyLXBhZGRpbmctZW5kOiAwcHg7XG4gICAgICAgIC0tYm9yZGVyLWNvbG9yOiAjZmZmO1xuICAgICAgICAtLXBhZGRpbmctZW5kOiAwO1xuICAgIH1cbiAgICAudXNlcm5hbWUsXG4gICAgLnBhc3N3b3Jkey0tcGFkZGluZy1zdGFydDogOHB4O31cbiAgICBpb24taXRlbS5zdWJtaXQtYnRuIHtcbiAgICAgICAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xuICAgICAgICAtLWlubmVyLXBhZGRpbmctZW5kOiAwO1xuICAgIH1cblxuICAgIC50ZXh0LWNvbG9yIHtcbiAgICAgICAgY29sb3I6IGJsYWNrO1xuICAgIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/signup/signup.page.ts":
/*!***************************************!*\
  !*** ./src/app/signup/signup.page.ts ***!
  \***************************************/
/*! exports provided: SignupPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPage", function() { return SignupPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _Service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Service/auth.service */ "./src/app/Service/auth.service.ts");
/* harmony import */ var _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Service/aler-service.service */ "./src/app/Service/aler-service.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _Service_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Service/loader.service */ "./src/app/Service/loader.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");








var SignupPage = /** @class */ (function () {
    function SignupPage(formBuilder, router, loadingService, navCtrl, authService, alertService) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.loadingService = loadingService;
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.alertService = alertService;
        this.signUpForm = this.formBuilder.group({
            email_id: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            password: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            user_name: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            user_display_name: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            password_confirmation: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    }
    SignupPage.prototype.ngOnInit = function () {
    };
    SignupPage.prototype.onFormSubmit = function (form) {
        var _this = this;
        this.loadingService.present();
        this.authService.register(form)
            .subscribe(function (successResponse) {
            console.log("success");
            _this.alertService.myAlertMethod(successResponse.message, "", function (data) {
                console.log("error");
                _this.router.navigate(['home']);
            });
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            _this.alertService.myAlertMethod("Error", err.error.message, function (data) {
                console.log("error");
            });
            _this.loadingService.dismiss();
        });
    };
    SignupPage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    SignupPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signup',
            template: __webpack_require__(/*! ./signup.page.html */ "./src/app/signup/signup.page.html"),
            styles: [__webpack_require__(/*! ./signup.page.scss */ "./src/app/signup/signup.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"], _Service_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"], _Service_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"], _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_4__["AlerServiceService"]])
    ], SignupPage);
    return SignupPage;
}());



/***/ })

}]);
//# sourceMappingURL=signup-signup-module.js.map