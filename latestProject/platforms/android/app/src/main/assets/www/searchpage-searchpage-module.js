(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["searchpage-searchpage-module"],{

/***/ "./src/app/Service/homepage.service.ts":
/*!*********************************************!*\
  !*** ./src/app/Service/homepage.service.ts ***!
  \*********************************************/
/*! exports provided: HomepageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomepageService", function() { return HomepageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");




var HomepageService = /** @class */ (function () {
    function HomepageService(http) {
        this.http = http;
        this.apiUrl = 'https://www.niletechinnovations.com/eq8tor/api/';
    }
    HomepageService.prototype.showCategories = function () {
        var _this = this;
        return this.http.post(this.apiUrl + 'categories', '')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('categories'); }));
    };
    HomepageService.prototype.showSubCategory = function (id) {
        var _this = this;
        return this.http.post(this.apiUrl + 'categories?' + 'parent_id=' + id, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('subcategories'); }));
    };
    HomepageService.prototype.log = function (message) {
        console.log(message);
    };
    HomepageService.prototype.getHomePageData = function () {
        return this.http.get('https://www.niletechinnovations.com/eq8tor/api/home').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(200));
    };
    HomepageService.prototype.getCategoryProducts = function (name) {
        var _this = this;
        return this.http.post(this.apiUrl + 'cat-products?' + 'slug=' + name, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('subcategories'); }));
    };
    HomepageService.prototype.productDetail = function (product) {
        var _this = this;
        return this.http.post(this.apiUrl + 'products/detail?' + 'slug=' + product, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('Detail'); }));
    };
    HomepageService.prototype.searchApi = function (textSearch) {
        var _this = this;
        return this.http.post(this.apiUrl + 'search?' + 'srch_term=' + textSearch, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('search api call'); }));
    };
    HomepageService.prototype.loadMoreData = function (textSearch) {
        var _this = this;
        return this.http.post(textSearch, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('search more api call'); }));
    };
    HomepageService.prototype.addToWishlist = function (textSearch) {
        var _this = this;
        return this.http.post(this.apiUrl + 'products/add/wishlist?' + 'product_id=' + textSearch, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('search api call'); }));
    };
    HomepageService.prototype.getWishlistProducts = function () {
        return this.http.get('https://www.niletechinnovations.com/eq8tor/api/user/wishlist-content').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(100));
    };
    HomepageService.prototype.getAllOrders = function () {
        return this.http.get('https://www.niletechinnovations.com/eq8tor/api/user/orders-content').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(100));
    };
    HomepageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], HomepageService);
    return HomepageService;
}());



/***/ }),

/***/ "./src/app/searchpage/searchpage.module.ts":
/*!*************************************************!*\
  !*** ./src/app/searchpage/searchpage.module.ts ***!
  \*************************************************/
/*! exports provided: SearchpagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchpagePageModule", function() { return SearchpagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _searchpage_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./searchpage.page */ "./src/app/searchpage/searchpage.page.ts");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");








var routes = [
    {
        path: '',
        component: _searchpage_page__WEBPACK_IMPORTED_MODULE_6__["SearchpagePage"]
    }
];
var SearchpagePageModule = /** @class */ (function () {
    function SearchpagePageModule() {
    }
    SearchpagePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_searchpage_page__WEBPACK_IMPORTED_MODULE_6__["SearchpagePage"]]
        })
    ], SearchpagePageModule);
    return SearchpagePageModule;
}());



/***/ }),

/***/ "./src/app/searchpage/searchpage.page.html":
/*!*************************************************!*\
  !*** ./src/app/searchpage/searchpage.page.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar class=\"new-background-color\">\n    <ion-title class=\"header-title\">Search Products</ion-title>\n    <ion-buttons slot=\"start\">\n        <ion-button color=\"dark\" (click)=\"goBack()\">\n          <ion-icon class=\"back-arrow\" name=\"arrow-back\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n  </ion-toolbar>\n  <ion-toolbar class=\"new-background-color\">\n    <ion-searchbar class=\"class-search\" placeholder=\"Filter Schedules\"\n      (ionChange)=\"userSearch($event)\"></ion-searchbar>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <!-- <div *ngIf=\"users && users.length > 0\">\n    <ion-list *ngIf=\"users.length === 0\">\n      <ion-item *ngFor=\"let user of [1,1,1,1,1,1,1,1,1,1,1,1,1]\">\n        <ion-label>\n          <ion-skeleton-text width=\"40%\" animated></ion-skeleton-text>\n          <ion-skeleton-text width=\"80%\" animated></ion-skeleton-text>\n        </ion-label>\n      </ion-item>\n    </ion-list>\n    <ion-list *ngIf=\"textToSearch.length > 0\">\n      <ion-item *ngFor=\"let user of users | filter:textToSearch\">\n        <ion-label>\n          <h3>{{user.name}}</h3>\n          <h5>{{user.email}}</h5>\n        </ion-label>\n      </ion-item>\n    </ion-list>\n  </div> -->\n\n  <ion-list *ngFor=\"let product of productsArray | filter:textToSearch\">\n      <ion-item >\n      <img class=\"item-image\" src=\"https://www.niletechinnovations.com/eq8tor{{product.image_url}}\">\n    <ion-label>\n      <h3 (click)=\"goToDetail(product)\">{{product.title}}</h3>\n      </ion-label>\n  </ion-item>\n  </ion-list>\n  \n  <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadData($event,nextUrl)\">\n    <ion-infinite-scroll-content\n      loadingSpinner=\"bubbles\"\n      loadingText=\"Loading more data...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/searchpage/searchpage.page.scss":
/*!*************************************************!*\
  !*** ./src/app/searchpage/searchpage.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .new-background-color {\n  --background: #b31117; }\n\n:host .class-search {\n  --background: white; }\n\n:host .item-image {\n  width: 15%;\n  padding: 10px; }\n\n:host .filters ion-col {\n  text-align: center;\n  font-size: 20px;\n  line-height: 20px; }\n\n:host .filters ion-col ion-icon {\n    color: #ccc; }\n\n:host .filters ion-col.col-with-arrow {\n    display: flex;\n    justify-content: center;\n    align-items: center; }\n\n:host .filters p {\n  color: black;\n  margin: 0;\n  font-size: 15px;\n  line-height: 15px;\n  --background: red; }\n\n:host .filters .selected {\n  font-weight: 10px; }\n\n:host .header-title {\n  color: #fff;\n  text-align: center; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL3NlYXJjaHBhZ2Uvc2VhcmNocGFnZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFFSSxxQkFBYSxFQUFBOztBQUZqQjtFQU1JLG1CQUFhLEVBQUE7O0FBTmpCO0VBU0ksVUFBVTtFQUNWLGFBQWEsRUFBQTs7QUFWakI7RUFlUSxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGlCQUFpQixFQUFBOztBQWpCekI7SUFvQlksV0FBVyxFQUFBOztBQXBCdkI7SUF3QlksYUFBYTtJQUNiLHVCQUF1QjtJQUN2QixtQkFBbUIsRUFBQTs7QUExQi9CO0VBZ0NRLFlBQVk7RUFDWixTQUFTO0VBQ1QsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixpQkFBYSxFQUFBOztBQXBDckI7RUF3Q1EsaUJBQWlCLEVBQUE7O0FBeEN6QjtFQTJDYyxXQUFVO0VBQUUsa0JBQWtCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9zZWFyY2hwYWdlL3NlYXJjaHBhZ2UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG46aG9zdCB7XG4ubmV3LWJhY2tncm91bmQtY29sb3Ige1xuICAgIC0tYmFja2dyb3VuZDogI2IzMTExNztcbn1cblxuLmNsYXNzLXNlYXJjaCB7XG4gICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cbi5pdGVtLWltYWdle1xuICAgIHdpZHRoOiAxNSU7XG4gICAgcGFkZGluZzogMTBweDtcbn1cbi5maWx0ZXJzIHtcblxuICAgIGlvbi1jb2wge1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XG5cbiAgICAgICAgaW9uLWljb24ge1xuICAgICAgICAgICAgY29sb3I6ICNjY2M7XG4gICAgICAgIH1cblxuICAgICAgICAmLmNvbC13aXRoLWFycm93IHtcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG5cbiAgICAgICAgfVxuICAgIH1cblxuICAgIHAge1xuICAgICAgICBjb2xvcjogYmxhY2s7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgZm9udC1zaXplOiAxNXB4O1xuICAgICAgICBsaW5lLWhlaWdodDogMTVweDtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiByZWQ7XG4gICAgfVxuXG4gICAgLnNlbGVjdGVkIHtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDEwcHg7XG4gICAgfVxufVxuLmhlYWRlci10aXRsZXtjb2xvcjojZmZmOyB0ZXh0LWFsaWduOiBjZW50ZXJ9XG5cbn0iXX0= */"

/***/ }),

/***/ "./src/app/searchpage/searchpage.page.ts":
/*!***********************************************!*\
  !*** ./src/app/searchpage/searchpage.page.ts ***!
  \***********************************************/
/*! exports provided: SearchpagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchpagePage", function() { return SearchpagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Service_user_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Service/user-service.service */ "./src/app/Service/user-service.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _Service_homepage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Service/homepage.service */ "./src/app/Service/homepage.service.ts");







var SearchpagePage = /** @class */ (function () {
    function SearchpagePage(router, navCtrl, service, homeService) {
        var _this = this;
        this.router = router;
        this.navCtrl = navCtrl;
        this.service = service;
        this.homeService = homeService;
        this.textToSearch = '';
        //productsArray: Results[] = [];
        this.productsArray = [];
        this.selectedPath = '';
        this.router.events.subscribe(function (event) {
            _this.selectedPath = event.url;
        });
        this.dynamicColor = 'redish';
        this.getSearchData('');
    }
    SearchpagePage.prototype.loadData = function (event, textSearch) {
        var _this = this;
        setTimeout(function () {
            console.log('Done');
            _this.homeService.loadMoreData(textSearch).subscribe(function (resp) {
                var moredata = resp;
                console.log(moredata);
                var products = moredata.data.results.data;
                _this.nextUrl = moredata.data.results.next_page_url;
                _this.totalNumberProducts = moredata.data.results.total;
                console.log(_this.totalNumberProducts);
                products.forEach(function (element) {
                    _this.productsArray.push(element);
                });
                ;
                console.log(_this.productsArray);
            });
            event.target.complete();
            console.log(_this.totalNumberProducts.length);
            // App logic to determine if all data is loaded
            // and disable the infinite scroll
            if (_this.productsArray.length == _this.totalNumberProducts) {
                event.target.disabled = true;
            }
        }, 500);
    };
    SearchpagePage.prototype.toggleInfiniteScroll = function () {
        this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
    };
    SearchpagePage.prototype.ngOnInit = function () {
    };
    SearchpagePage.prototype.userSearch = function (event) {
        //this.getUsers();
        var textSearch = event.target.value;
        this.textToSearch = textSearch;
        console.log(textSearch);
    };
    SearchpagePage.prototype.getUsers = function () {
        // this.service.getUsers()
        //   .subscribe(resp => { this.users = resp });
    };
    SearchpagePage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    SearchpagePage.prototype.getSearchData = function (searchText) {
        var _this = this;
        this.homeService.searchApi(searchText).subscribe(function (resp) {
            var dataReceived = resp;
            console.log(dataReceived);
            var products = dataReceived.data.results.data;
            console.log(products);
            _this.nextUrl = dataReceived.data.results.next_page_url;
            console.log(products);
            products.forEach(function (element) {
                _this.productsArray.push(element);
            });
            ;
            console.log(_this.productsArray);
        });
    };
    SearchpagePage.prototype.goToDetail = function (data) {
        console.log(data.slug);
        var title = data.slug;
        this.router.navigate(['product-detail'], { queryParams: { 'id': title } });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonInfiniteScroll"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonInfiniteScroll"])
    ], SearchpagePage.prototype, "infiniteScroll", void 0);
    SearchpagePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-searchpage',
            template: __webpack_require__(/*! ./searchpage.page.html */ "./src/app/searchpage/searchpage.page.html"),
            styles: [__webpack_require__(/*! ./searchpage.page.scss */ "./src/app/searchpage/searchpage.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], _Service_user_service_service__WEBPACK_IMPORTED_MODULE_3__["UserServiceService"], _Service_homepage_service__WEBPACK_IMPORTED_MODULE_5__["HomepageService"]])
    ], SearchpagePage);
    return SearchpagePage;
}());



/***/ })

}]);
//# sourceMappingURL=searchpage-searchpage-module.js.map