(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./src/app/Service/cart.service.ts":
/*!*****************************************!*\
  !*** ./src/app/Service/cart.service.ts ***!
  \*****************************************/
/*! exports provided: CartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartService", function() { return CartService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CartService = /** @class */ (function () {
    function CartService() {
        this.data = [
            {
                category: "Pizza",
                expanded: false,
                products: [
                    { id: 0, name: 'Salami', price: '8' },
                    { id: 1, name: 'Classic', price: '5' },
                    { id: 2, name: 'Tuna', price: '9' },
                    { id: 3, name: 'Hawai', price: '7' }
                ]
            },
            {
                category: 'Pasta',
                products: [
                    { id: 4, name: 'Mac & Cheese', price: '8' },
                    { id: 5, name: 'Bologenese', price: '6' }
                ]
            },
            {
                category: 'Salad',
                products: [
                    { id: 6, name: 'Ham & Egg', price: '8' },
                    { id: 7, name: 'Basic', price: '5' },
                    { id: 8, name: 'Ceaser', price: '9' }
                ]
            }
        ];
        this.cart = [];
    }
    CartService.prototype.getProducts = function () {
        return this.data;
    };
    CartService.prototype.getCart = function () {
        return this.cart;
    };
    CartService.prototype.addProduct = function (product) {
        this.cart.push(product);
    };
    CartService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CartService);
    return CartService;
}());



/***/ }),

/***/ "./src/app/Service/homepage.service.ts":
/*!*********************************************!*\
  !*** ./src/app/Service/homepage.service.ts ***!
  \*********************************************/
/*! exports provided: HomepageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomepageService", function() { return HomepageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");




var HomepageService = /** @class */ (function () {
    function HomepageService(http) {
        this.http = http;
        this.apiUrl = 'https://www.niletechinnovations.com/eq8tor/api/';
    }
    HomepageService.prototype.showCategories = function () {
        var _this = this;
        return this.http.post(this.apiUrl + 'categories', '')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('categories'); }));
    };
    HomepageService.prototype.showSubCategory = function (id) {
        var _this = this;
        return this.http.post(this.apiUrl + 'categories?' + 'parent_id=' + id, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('subcategories'); }));
    };
    HomepageService.prototype.log = function (message) {
        console.log(message);
    };
    HomepageService.prototype.getHomePageData = function () {
        return this.http.get('https://www.niletechinnovations.com/eq8tor/api/home').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(200));
    };
    HomepageService.prototype.getCategoryProducts = function (name) {
        var _this = this;
        return this.http.post(this.apiUrl + 'cat-products?' + 'slug=' + name, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('subcategories'); }));
    };
    HomepageService.prototype.productDetail = function (product) {
        var _this = this;
        return this.http.post(this.apiUrl + 'products/detail?' + 'slug=' + product, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('Detail'); }));
    };
    HomepageService.prototype.searchApi = function (textSearch) {
        var _this = this;
        return this.http.post(this.apiUrl + 'search?' + 'srch_term=' + textSearch, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('search api call'); }));
    };
    HomepageService.prototype.loadMoreData = function (textSearch) {
        var _this = this;
        return this.http.post(textSearch, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('search more api call'); }));
    };
    HomepageService.prototype.addToWishlist = function (textSearch) {
        var _this = this;
        return this.http.post(this.apiUrl + 'products/add/wishlist?' + 'product_id=' + textSearch, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('search api call'); }));
    };
    HomepageService.prototype.getWishlistProducts = function () {
        return this.http.get('https://www.niletechinnovations.com/eq8tor/api/user/wishlist-content').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(100));
    };
    HomepageService.prototype.getAllOrders = function () {
        return this.http.get('https://www.niletechinnovations.com/eq8tor/api/user/orders-content').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(100));
    };
    HomepageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], HomepageService);
    return HomepageService;
}());



/***/ }),

/***/ "./src/app/Service/loader.service.ts":
/*!*******************************************!*\
  !*** ./src/app/Service/loader.service.ts ***!
  \*******************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var LoaderService = /** @class */ (function () {
    function LoaderService(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
        this.isLoading = false;
    }
    LoaderService.prototype.present = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isLoading = true;
                        return [4 /*yield*/, this.loadingCtrl.create({ duration: 5000, }).then(function (a) {
                                a.present().then(function () {
                                    if (!_this.isLoading) {
                                        a.dismiss().then(function () { return console.log('abort presenting'); });
                                    }
                                });
                            })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    LoaderService.prototype.dismiss = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isLoading = false;
                        return [4 /*yield*/, this.loadingCtrl.dismiss().then(function () { return console.log('dismissed'); })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    LoaderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])
    ], LoaderService);
    return LoaderService;
}());



/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");








// import { IonicRatingModule } from 'ionic4-rating';
// import { BarRatingModule } from 'ngx-bar-rating'
var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                    }
                ])
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/home/home.page.html":
/*!*************************************!*\
  !*** ./src/app/home/home.page.html ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"new-background-color\">\n    <ion-title class=\"header-title\" (click)=\"RefreshPage()\">Home</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"color: white\" autoHide=\"false\"></ion-menu-button>\n    </ion-buttons>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"openCart()\">\n        <ion-badge class=\"badge-color\" *ngIf=\"cart.length > 0\">{{ cart.length }}</ion-badge>\n        <ion-icon slot=\"icon-only\" name=\"cart\" style=\"color: white\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n  <ion-toolbar class=\"new-background-color\">\n    <ion-searchbar class=\"class-search\" (click)=\"openSearchPage()\" placeholder=\"Filter Schedules\"></ion-searchbar>\n  </ion-toolbar>\n  <div class=\"container\">\n    <div *ngIf=\"categories.length > 0\" class=\"category-scroll\" scrollX=\"true\">\n      <span class=\"category-span\" *ngFor=\"let category of categories\"\n        (click)=\"categoryClick(category.name)\">{{category.name}}</span>\n    </div>\n  </div>\n</ion-header>\n\n<ion-content class=\"home-content\" padding>\n  <div>\n    <ion-slides *ngIf=\"bannerList && bannerList.length\" autoplay=\"5000\" loop=\"true\" speed=\"500\" class=\"slides\"\n      pager=\"true\">\n      <ion-slide *ngFor=\"let cat of bannerList\">\n        <div class=\"slide-content\" *ngIf=\"cat !== ''; else placeholder\">\n          <img class=\"image-wrap-slider\" src=\"https://www.niletechinnovations.com/eq8tor{{cat.img_url}}\">\n        </div>\n        <ng-template #placeholder><img src=\"../../assets/imgs/logo.png\"></ng-template>\n      </ion-slide>\n    </ion-slides>\n  </div>\n  <ion-row no-padding>\n    <ion-col class=\"heading-title\">\n      Latest Items\n    </ion-col>\n  </ion-row>\n\n  <ion-slides [options]=\"sliderConfig\" class=\"Products-slider\">\n    <ion-slide *ngFor=\"let cat of latestProducts; let i = index\">\n      <div class=\"slide-content\">\n        <ion-card class=\"product-card\">\n          <ion-card-content class=\"product-card-media\">\n            <img (click)=\"goToProductDetail(cat.post_slug)\" height=\"150\"\n              src=\"https://www.niletechinnovations.com/eq8tor{{cat.post_image_url}}\">\n          </ion-card-content>\n          <div>\n            <div class=\"add-Wishlist\" *ngIf=\"!wishListAdded || cat._wishlist == false\" (click)=\"like(cat.id,i)\">\n              <img src=\"../../assets/imgs/Wishlist.svg\" >\n            </div>\n          </div>\n          <div class=\"add-Wishlist\" *ngIf=\"wishListAdded || cat._wishlist == true\">\n            <img src=\"../../assets/imgs/Wishlisted.svg\" >\n          </div>\n        </ion-card>\n        <ion-card-content class=\"product-content\">\n          <ion-card-title class=\"product-title\">\n            {{cat.post_title}}\n            <div class=\"star-rating\">\n              <span style=\"width:50%\"></span>\n            </div>\n            <div class=\"product-price\">\n              <span>${{cat.post_price}}</span>\n            </div>\n          </ion-card-title>\n        </ion-card-content>\n      </div>\n    </ion-slide>\n  </ion-slides>\n\n  <ion-row no-padding>\n    <ion-col class=\"heading-title\">\n      Featured Items\n    </ion-col>\n  </ion-row>\n\n  <ion-slides [options]=\"sliderConfig\" class=\"Products-slider\">\n    <ion-slide *ngFor=\"let cat of featuredItems; let i = index\">\n      <div class=\"slide-content\">\n        <ion-card class=\"product-card\">\n          <ion-card-content class=\"product-card-media\">\n            <img (click)=\"goToProductDetail(cat.post_slug)\" height=\"150\"\n              src=\"https://www.niletechinnovations.com/eq8tor{{cat.post_image_url}}\">\n          </ion-card-content>\n          <div>\n            <div class=\"add-Wishlist\" *ngIf=\"!wishListAdded || cat._wishlist == false\" (click)=\"likeFeatured(cat.id,i)\">\n              <img src=\"../../assets/imgs/Wishlist.svg\" >\n            </div>\n          </div>\n          <div class=\"add-Wishlist\" *ngIf=\"wishListAdded || cat._wishlist == true\">\n            <img src=\"../../assets/imgs/Wishlisted.svg\" >\n          </div>\n        </ion-card>\n        <ion-card-content class=\"product-content\">\n          <ion-card-title class=\"product-title\">\n            {{cat.post_title}}\n          </ion-card-title>\n          <div class=\"star-rating\">\n            <span style=\"width:50%\"></span>\n          </div>\n          <div class=\"product-price\">\n            <span>${{cat.post_price}}</span>\n          </div>\n        </ion-card-content>\n      </div>\n    </ion-slide>\n  </ion-slides>\n\n\n  <!-- <div *ngIf=\"users && users.length > 0\">\n          <ion-list *ngIf=\"users.length === 0\">\n              <ion-item *ngFor=\"let user of [1,1,1,1,1,1,1,1,1,1,1,1,1]\">\n                <ion-label>\n                  <ion-skeleton-text width=\"40%\" animated></ion-skeleton-text>\n                  <ion-skeleton-text width=\"80%\" animated></ion-skeleton-text>\n                </ion-label>\n              </ion-item>\n            </ion-list>\n            <ion-list *ngIf=\"textToSearch.length > 0\">\n              <ion-item *ngFor=\"let user of users | filter:textToSearch\">\n                <ion-label>\n                  <h3>{{user.name}}</h3>\n                  <h5>{{user.email}}</h5>\n                </ion-label>\n              </ion-item>\n            </ion-list>\n      </div> -->\n</ion-content>"

/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .new-background-color {\n  --background: #b31117; }\n\n:host .header-title {\n  color: #fff;\n  text-align: center; }\n\n:host .class-search {\n  --background: white; }\n\n:host .searchbar-input {\n  --border-radius:2px !important; }\n\n:host .badge-color {\n  --background: #b31117;\n  color: white; }\n\n:host .filters ion-col {\n  text-align: center;\n  font-size: 20px;\n  line-height: 20px; }\n\n:host .filters ion-col ion-icon {\n    color: #ccc; }\n\n:host .filters ion-col.col-with-arrow {\n    display: flex;\n    justify-content: center;\n    align-items: center; }\n\n:host .filters p {\n  color: black;\n  margin: 0;\n  font-size: 15px;\n  line-height: 15px;\n  --background: red; }\n\n:host .filters .selected {\n  font-weight: 10px; }\n\n:host .container ::-webkit-scrollbar {\n  display: none; }\n\n:host .container .category-scroll {\n  overflow: auto;\n  background-color: #b31117;\n  white-space: nowrap;\n  padding: 10px 12px 10px 12px; }\n\n:host .heart-icon {\n  background: none;\n  --background: transparent;\n  --background-focused: transparent;\n  color: #b31117;\n  padding: 0;\n  -webkit-margin-start: 0;\n  margin-inline-start: 0;\n  -webkit-margin-end: 0;\n  margin-inline-end: 0;\n  --padding-start: 0;\n  --padding-end: 0; }\n\n:host .category-span {\n  margin-bottom: auto;\n  margin-left: 5px;\n  color: white;\n  background: #841014;\n  padding: 8px 10px;\n  border-radius: 2px;\n  font-weight: 600;\n  font-size: 12px; }\n\n:host ion-badge {\n  color: #fff;\n  position: absolute;\n  top: 0px;\n  right: 0px;\n  border-radius: 100%; }\n\n:host .category-block {\n  margin-bottom: 4px; }\n\n:host .category-banner {\n  border-left: 8px solid var(--ion-color-secondary);\n  background: var(--ion-color-light);\n  height: 40px;\n  padding: 10px;\n  font-weight: 500; }\n\n:host .card-title {\n  font-size: 12px;\n  font-weight: 300; }\n\n:host ion-slides {\n  color: #fff; }\n\n:host ion-slides ion-slide {\n    align-items: start;\n    text-align: center; }\n\n:host ion-slides ion-slide .slide-content .text p {\n      margin-top: 0;\n      margin-bottom: 10px; }\n\nion-content.home-content {\n  background: #f4f8fc;\n  --background: var(--ion-background-color,#f4f8fc)\n; }\n\n.heading-title {\n  color: #213051;\n  font-size: 16px;\n  font-weight: 600;\n  padding: 10px 20px 10px 0;\n  margin-bottom: 0;\n  display: inline-block; }\n\nion-card.product-card {\n  -webkit-margin-start: 0;\n  margin-inline-start: 0px;\n  background: #fff;\n  border-radius: 3px;\n  --background: var(--ion-item-background,#fff);\n  box-shadow: none;\n  margin-bottom: 0;\n  min-height: 200px;\n  -webkit-margin-end: 10px;\n  margin-inline-end: 10px;\n  margin-bottom: 10px; }\n\n.product-card-media {\n  -webkit-padding-start: 0px;\n  padding-inline-start: 0px;\n  -webkit-padding-end: 0px;\n  padding-inline-end: 0px;\n  padding-top: 0;\n  padding-bottom: 0;\n  height: 200px;\n  overflow: hidden; }\n\n.product-card-media img {\n  width: 100% !important;\n  max-height: none !important;\n  max-width: none !important; }\n\n.product-title {\n  color: #656c72;\n  font-size: 14px;\n  font-weight: 500;\n  line-height: 24px; }\n\n.product-content {\n  padding: 0; }\n\n.Products-slider {\n  height: auto;\n  /* slider height you want */ }\n\n.star-rating {\n  font-family: \"FontAwesome\";\n  font-size: 16px;\n  height: 20px;\n  overflow: hidden;\n  position: relative;\n  width: 78px; }\n\n.star-rating {\n  margin: 10px auto; }\n\n.star-rating::before {\n  color: #656c72;\n  content: \"\\2605\\2605\\2605\\2605\\2605\";\n  float: left;\n  font-size: 14px;\n  left: 0;\n  letter-spacing: 2px;\n  position: absolute;\n  top: 0; }\n\n.star-rating span {\n  float: left;\n  left: 0;\n  overflow: hidden;\n  padding-top: 1.5em;\n  position: absolute;\n  top: 0; }\n\n.star-rating span::before {\n  color: #fab902;\n  content: \"\\2605\\2605\\2605\\2605\\2605\";\n  font-size: 14px;\n  left: 0;\n  letter-spacing: 2px;\n  position: absolute;\n  top: 0; }\n\n.star-rating .rating {\n  display: none; }\n\n.product-price span {\n  color: #656c72;\n  font-size: 16px;\n  font-weight: 600; }\n\n.add-Wishlist {\n  position: absolute;\n  top: 0;\n  right: 2px; }\n\n.add-Wishlist img {\n  height: 16px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFHQSxxQkFBYSxFQUFBOztBQUhiO0VBTWMsV0FBVTtFQUFFLGtCQUFrQixFQUFBOztBQU41QztFQVFBLG1CQUFhLEVBQUE7O0FBUmI7RUFZSSw4QkFBZ0IsRUFBQTs7QUFacEI7RUFlQSxxQkFBYTtFQUNiLFlBQVksRUFBQTs7QUFoQlo7RUFvQkEsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixpQkFBaUIsRUFBQTs7QUF0QmpCO0lBeUJBLFdBQVcsRUFBQTs7QUF6Qlg7SUE2QkEsYUFBYTtJQUNiLHVCQUF1QjtJQUN2QixtQkFBbUIsRUFBQTs7QUEvQm5CO0VBcUNBLFlBQVk7RUFDWixTQUFTO0VBQ1QsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixpQkFBYSxFQUFBOztBQXpDYjtFQTZDQSxpQkFBaUIsRUFBQTs7QUE3Q2pCO0VBbURBLGFBQWEsRUFBQTs7QUFuRGI7RUF1REEsY0FBYztFQUNkLHlCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIsNEJBQTRCLEVBQUE7O0FBMUQ1QjtFQThESSxnQkFBZ0I7RUFDaEIseUJBQWE7RUFDYixpQ0FBcUI7RUFDckIsY0FBYztFQUNkLFVBQVU7RUFDVix1QkFBdUI7RUFDdkIsc0JBQXNCO0VBQ3RCLHFCQUFxQjtFQUNyQixvQkFBb0I7RUFDcEIsa0JBQWdCO0VBQ2hCLGdCQUFjLEVBQUE7O0FBeEVsQjtFQTRFQSxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsZUFBZSxFQUFBOztBQW5GZjtFQXlGQSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixVQUFVO0VBQ1YsbUJBQW1CLEVBQUE7O0FBN0ZuQjtFQWdHQSxrQkFBa0IsRUFBQTs7QUFoR2xCO0VBb0dBLGlEQUFpRDtFQUNqRCxrQ0FBa0M7RUFDbEMsWUFBWTtFQUNaLGFBQWE7RUFDYixnQkFBZ0IsRUFBQTs7QUF4R2hCO0VBMkdBLGVBQWU7RUFDZixnQkFBZ0IsRUFBQTs7QUE1R2hCO0VBZ0hBLFdBQVcsRUFBQTs7QUFoSFg7SUFtSEEsa0JBQWtCO0lBQ2xCLGtCQUFrQixFQUFBOztBQXBIbEI7TUE0SEEsYUFBYTtNQUNiLG1CQUFtQixFQUFBOztBQU9uQjtFQUNJLG1CQUFtQjtFQUNuQjtBQUFhLEVBQUE7O0FBR2pCO0VBQW1CLGNBQWM7RUFDakMsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQix5QkFBeUI7RUFDekIsZ0JBQWdCO0VBQ2hCLHFCQUFxQixFQUFBOztBQUVyQjtFQUNJLHVCQUF1QjtFQUN2Qix3QkFBd0I7RUFDeEIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQiw2Q0FBYTtFQUNiLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLHdCQUF3QjtFQUN4Qix1QkFBdUI7RUFDdkIsbUJBQW1CLEVBQUE7O0FBRXZCO0VBQXFCLDBCQUEwQjtFQUMzQyx5QkFBeUI7RUFDekIsd0JBQXdCO0VBQ3hCLHVCQUF1QjtFQUFFLGNBQWM7RUFDdkMsaUJBQWlCO0VBQUksYUFBYTtFQUNsQyxnQkFBZ0IsRUFBQTs7QUFDcEI7RUFDSSxzQkFBc0I7RUFDdEIsMkJBQTJCO0VBQzNCLDBCQUEwQixFQUFBOztBQUM5QjtFQUFtQixjQUFjO0VBQzdCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsaUJBQWlCLEVBQUE7O0FBQ3JCO0VBQWlCLFVBQVUsRUFBQTs7QUFFM0I7RUFDSSxZQUFZO0VBQUUsMkJBQUEsRUFBNEI7O0FBRTFDO0VBQ0ksMEJBQTBCO0VBQzFCLGVBQWU7RUFDZixZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixXQUFXLEVBQUE7O0FBR2I7RUFDRSxpQkFBaUIsRUFBQTs7QUFHbkI7RUFDRSxjQUFjO0VBQ2Qsb0NBQW9DO0VBQ3BDLFdBQVc7RUFDWCxlQUFjO0VBQ2QsT0FBTztFQUNQLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsTUFBTSxFQUFBOztBQUdQO0VBQ0MsV0FBVztFQUNYLE9BQU87RUFDUCxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixNQUFNLEVBQUE7O0FBRVA7RUFDQyxjQUFhO0VBQ2Isb0NBQW9DO0VBQ3BDLGVBQWU7RUFDZixPQUFPO0VBQ1AsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixNQUFNLEVBQUE7O0FBR1A7RUFDQyxhQUFhLEVBQUE7O0FBR2Y7RUFBd0IsY0FBYztFQUNwQyxlQUFlO0VBQ2YsZ0JBQWdCLEVBQUE7O0FBR2hCO0VBQ0Usa0JBQWtCO0VBQ2xCLE1BQU07RUFDTixVQUFTLEVBQUE7O0FBR2I7RUFBbUIsWUFBWSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcblxuLm5ldy1iYWNrZ3JvdW5kLWNvbG9yIHtcbi0tYmFja2dyb3VuZDogI2IzMTExNztcbn1cblxuLmhlYWRlci10aXRsZXtjb2xvcjojZmZmOyB0ZXh0LWFsaWduOiBjZW50ZXJ9XG4uY2xhc3Mtc2VhcmNoIHtcbi0tYmFja2dyb3VuZDogd2hpdGU7XG59XG5cbi5zZWFyY2hiYXItaW5wdXR7XG4gICAgLS1ib3JkZXItcmFkaXVzOjJweCAhaW1wb3J0YW50O1xufVxuLmJhZGdlLWNvbG9ye1xuLS1iYWNrZ3JvdW5kOiAjYjMxMTE3O1xuY29sb3I6IHdoaXRlO1xufVxuLmZpbHRlcnMge1xuaW9uLWNvbCB7XG50ZXh0LWFsaWduOiBjZW50ZXI7XG5mb250LXNpemU6IDIwcHg7XG5saW5lLWhlaWdodDogMjBweDtcblxuaW9uLWljb24ge1xuY29sb3I6ICNjY2M7XG59XG5cbiYuY29sLXdpdGgtYXJyb3cge1xuZGlzcGxheTogZmxleDtcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcblxufVxufVxuXG5wIHtcbmNvbG9yOiBibGFjaztcbm1hcmdpbjogMDtcbmZvbnQtc2l6ZTogMTVweDtcbmxpbmUtaGVpZ2h0OiAxNXB4O1xuLS1iYWNrZ3JvdW5kOiByZWQ7XG59XG5cbi5zZWxlY3RlZCB7XG5mb250LXdlaWdodDogMTBweDtcbn1cbn1cblxuLmNvbnRhaW5lciB7XG46Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbmRpc3BsYXk6IG5vbmU7XG59XG5cbi5jYXRlZ29yeS1zY3JvbGwge1xub3ZlcmZsb3c6IGF1dG87XG5iYWNrZ3JvdW5kLWNvbG9yOiAjYjMxMTE3O1xud2hpdGUtc3BhY2U6IG5vd3JhcDtcbnBhZGRpbmc6IDEwcHggMTJweCAxMHB4IDEycHg7XG59XG59XG4uaGVhcnQtaWNvbntcbiAgICBiYWNrZ3JvdW5kOiBub25lO1xuICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6IHRyYW5zcGFyZW50O1xuICAgIGNvbG9yOiAjYjMxMTE3O1xuICAgIHBhZGRpbmc6IDA7XG4gICAgLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IDA7XG4gICAgbWFyZ2luLWlubGluZS1zdGFydDogMDtcbiAgICAtd2Via2l0LW1hcmdpbi1lbmQ6IDA7XG4gICAgbWFyZ2luLWlubGluZS1lbmQ6IDA7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xuICAgIC0tcGFkZGluZy1lbmQ6IDA7XG5cbn1cbi5jYXRlZ29yeS1zcGFuIHtcbm1hcmdpbi1ib3R0b206IGF1dG87XG5tYXJnaW4tbGVmdDogNXB4O1xuY29sb3I6IHdoaXRlO1xuYmFja2dyb3VuZDogIzg0MTAxNDtcbnBhZGRpbmc6IDhweCAxMHB4O1xuYm9yZGVyLXJhZGl1czogMnB4O1xuZm9udC13ZWlnaHQ6IDYwMDtcbmZvbnQtc2l6ZTogMTJweDtcbn1cblxuXG5cbmlvbi1iYWRnZSB7XG5jb2xvcjogI2ZmZjtcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcbnRvcDogMHB4O1xucmlnaHQ6IDBweDtcbmJvcmRlci1yYWRpdXM6IDEwMCU7XG59XG4uY2F0ZWdvcnktYmxvY2sge1xubWFyZ2luLWJvdHRvbTogNHB4O1xufVxuXG4uY2F0ZWdvcnktYmFubmVyIHtcbmJvcmRlci1sZWZ0OiA4cHggc29saWQgdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XG5iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xuaGVpZ2h0OiA0MHB4O1xucGFkZGluZzogMTBweDtcbmZvbnQtd2VpZ2h0OiA1MDA7XG59XG4uY2FyZC10aXRsZXtcbmZvbnQtc2l6ZTogMTJweDtcbmZvbnQtd2VpZ2h0OiAzMDA7XG59XG5pb24tc2xpZGVzIHtcbi8vIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbmNvbG9yOiAjZmZmO1xuXG5pb24tc2xpZGUgeyBcbmFsaWduLWl0ZW1zOiBzdGFydDtcbnRleHQtYWxpZ246IGNlbnRlcjtcblxuLnNsaWRlLWNvbnRlbnQge1xuLy8gcGFkZGluZzogMHB4IDE0JTtcblxuXG5cbi50ZXh0IHAge1xubWFyZ2luLXRvcDogMDtcbm1hcmdpbi1ib3R0b206IDEwcHg7XG59XG59XG59XG59XG59XG5cbmlvbi1jb250ZW50LmhvbWUtY29udGVudCB7XG4gICAgYmFja2dyb3VuZDogI2Y0ZjhmYztcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yLCNmNGY4ZmMpXG59XG5cbi5oZWFkaW5nLXRpdGxleyAgICBjb2xvcjogIzIxMzA1MTtcbmZvbnQtc2l6ZTogMTZweDtcbmZvbnQtd2VpZ2h0OiA2MDA7XG5wYWRkaW5nOiAxMHB4IDIwcHggMTBweCAwO1xubWFyZ2luLWJvdHRvbTogMDtcbmRpc3BsYXk6IGlubGluZS1ibG9jazt9XG5cbmlvbi1jYXJkLnByb2R1Y3QtY2FyZCB7XG4gICAgLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IDA7XG4gICAgbWFyZ2luLWlubGluZS1zdGFydDogMHB4O1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWl0ZW0tYmFja2dyb3VuZCwjZmZmKTtcbiAgICBib3gtc2hhZG93OiBub25lO1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG4gICAgbWluLWhlaWdodDogMjAwcHg7XG4gICAgLXdlYmtpdC1tYXJnaW4tZW5kOiAxMHB4O1xuICAgIG1hcmdpbi1pbmxpbmUtZW5kOiAxMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG4ucHJvZHVjdC1jYXJkLW1lZGlheyAtd2Via2l0LXBhZGRpbmctc3RhcnQ6IDBweDtcbiAgICBwYWRkaW5nLWlubGluZS1zdGFydDogMHB4O1xuICAgIC13ZWJraXQtcGFkZGluZy1lbmQ6IDBweDtcbiAgICBwYWRkaW5nLWlubGluZS1lbmQ6IDBweDsgcGFkZGluZy10b3A6IDA7XG4gICAgcGFkZGluZy1ib3R0b206IDA7ICAgaGVpZ2h0OiAyMDBweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO31cbi5wcm9kdWN0LWNhcmQtbWVkaWEgaW1ne1xuICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgbWF4LWhlaWdodDogbm9uZSAhaW1wb3J0YW50O1xuICAgIG1heC13aWR0aDogbm9uZSAhaW1wb3J0YW50OyAgIH1cbi5wcm9kdWN0LXRpdGxleyAgICBjb2xvcjogIzY1NmM3MjtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICBsaW5lLWhlaWdodDogMjRweDt9XG4ucHJvZHVjdC1jb250ZW50e3BhZGRpbmc6IDA7fVxuXG4uUHJvZHVjdHMtc2xpZGVyIHtcbiAgICBoZWlnaHQ6IGF1dG87IC8qIHNsaWRlciBoZWlnaHQgeW91IHdhbnQgKi9cbiAgICB9XG4gICAgLnN0YXItcmF0aW5ne1xuICAgICAgICBmb250LWZhbWlseTogXCJGb250QXdlc29tZVwiO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIGhlaWdodDogMjBweDtcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICB3aWR0aDogNzhweDtcbiAgICAgIH1cbiAgICAgIFxuICAgICAgLnN0YXItcmF0aW5ne1xuICAgICAgICBtYXJnaW46IDEwcHggYXV0bzsgICAgXG4gICAgICB9XG4gICAgICBcbiAgICAgIC5zdGFyLXJhdGluZzo6YmVmb3Jle1xuICAgICAgICBjb2xvcjogIzY1NmM3MjtcbiAgICAgICAgY29udGVudDogXCJcXDI2MDVcXDI2MDVcXDI2MDVcXDI2MDVcXDI2MDVcIjtcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgIGZvbnQtc2l6ZToxNHB4O1xuICAgICAgICBsZWZ0OiAwO1xuICAgICAgICBsZXR0ZXItc3BhY2luZzogMnB4O1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHRvcDogMDtcbiAgICAgIH1cbiAgICAgIFxuICAgICAgIC5zdGFyLXJhdGluZyBzcGFue1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgbGVmdDogMDtcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgICAgcGFkZGluZy10b3A6IDEuNWVtO1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHRvcDogMDtcbiAgICAgIH1cbiAgICAgICAuc3Rhci1yYXRpbmcgc3Bhbjo6YmVmb3Jle1xuICAgICAgICBjb2xvcjojZmFiOTAyO1xuICAgICAgICBjb250ZW50OiBcIlxcMjYwNVxcMjYwNVxcMjYwNVxcMjYwNVxcMjYwNVwiO1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIGxlZnQ6IDA7XG4gICAgICAgIGxldHRlci1zcGFjaW5nOiAycHg7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgdG9wOiAwO1xuICAgICAgfVxuICAgICAgXG4gICAgICAgLnN0YXItcmF0aW5nIC5yYXRpbmd7XG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgICB9XG5cbiAgICAgIC5wcm9kdWN0LXByaWNlIHNwYW57ICAgIGNvbG9yOiAjNjU2YzcyO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIH1cblxuICAgICAgICAuYWRkLVdpc2hsaXN0e1xuICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICB0b3A6IDA7XG4gICAgICAgICAgcmlnaHQ6MnB4O1xuICAgICAgfVxuXG4gICAgICAuYWRkLVdpc2hsaXN0ICBpbWd7aGVpZ2h0OiAxNnB4O30iXX0= */"

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ionic-selectable */ "./node_modules/ionic-selectable/esm5/ionic-selectable.min.js");
/* harmony import */ var _Service_user_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Service/user-service.service */ "./src/app/Service/user-service.service.ts");
/* harmony import */ var _Service_cart_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Service/cart.service */ "./src/app/Service/cart.service.ts");
/* harmony import */ var _Service_homepage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Service/homepage.service */ "./src/app/Service/homepage.service.ts");
/* harmony import */ var _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../Service/aler-service.service */ "./src/app/Service/aler-service.service.ts");
/* harmony import */ var _Service_loader_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../Service/loader.service */ "./src/app/Service/loader.service.ts");










var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, events, loadingService, toastController, alert, homepageService, cartService, router, toastCtrl, service) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.events = events;
        this.loadingService = loadingService;
        this.toastController = toastController;
        this.alert = alert;
        this.homepageService = homepageService;
        this.cartService = cartService;
        this.router = router;
        this.toastCtrl = toastCtrl;
        this.service = service;
        this.sliderConfig = {
            initialSlide: 0,
            speed: 400,
            slidesPerView: 2,
            autoplay: false,
        };
        this.bannerSlider = {
            initialSlide: 0,
            speed: 2000,
            slidesPerView: 1,
            autoplay: true,
        };
        this.selectedPath = '';
        this.textToSearch = '';
        this.users = [];
        this.homePageModel = [];
        this.selectedCategory = [];
        this.wishListAdded = false;
        // menuArray = [];
        this.cart = [];
        this.items = [];
        this.liked = false;
        this.router.events.subscribe(function (event) {
            _this.selectedPath = event.url;
        });
        this.dynamicColor = 'redish';
        this.showCategories();
        this.getHomePage();
        this.showCategories123();
    }
    HomePage.prototype.openSearchPage = function () {
        this.router.navigate(['searchpage']);
    };
    HomePage.prototype.ngOnInit = function () {
        this.items = this.cartService.getProducts();
        this.cart = this.cartService.getCart();
    };
    HomePage.prototype.addToCart = function (product) {
        this.cartService.addProduct(product);
        console.log(product);
    };
    HomePage.prototype.RefreshPage = function () {
        this.showCategories();
        this.getHomePage();
    };
    HomePage.prototype.openCart = function () {
        this.router.navigate(['cart']);
    };
    HomePage.prototype.userChanged = function (event) {
        console.log('event', event);
    };
    HomePage.prototype.goToProductDetail = function (product) {
        this.router.navigate(['product-detail'], { queryParams: { 'id': product } });
        console.log("response product", product);
    };
    HomePage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    HomePage.prototype.getUsers = function () {
        console.log("resppppppp");
        this.service.getUsers()
            .subscribe(function (resp) {
            console.log("resppppppp", resp);
            // this.users = resp
        }, function (err) { console.log(err); });
    };
    HomePage.prototype.like = function (productId, id) {
        var _this = this;
        console.log(productId);
        this.homepageService.addToWishlist(productId).subscribe(function (resp) {
            console.log(resp);
            _this.presentToast(resp.message);
            _this.latestProducts[id]._wishlist = true;
        }, function (err) {
            console.log(err);
            _this.presentToast(err.message);
        });
    };
    HomePage.prototype.likeFeatured = function (productId, id) {
        var _this = this;
        this.homepageService.addToWishlist(productId).subscribe(function (resp) {
            console.log(resp);
            _this.presentToast(resp.message);
            _this.featuredItems[id]._wishlist = true;
        }, function (err) {
            console.log(err);
            _this.presentToast(err.message);
        });
    };
    HomePage.prototype.presentToast = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: message,
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.getHomePage = function () {
        var _this = this;
        this.loadingService.present();
        this.homepageService.getHomePageData().subscribe(function (response) {
            _this.homePageModel = response['data'];
            _this.featuredItems = _this.homePageModel["features_items"];
            _this.latestProducts = _this.homePageModel["latest_items"];
            // for(let i = 0;i < this.latestProducts.length; i++){
            //   this.wishListAdded = this.latestProducts[i]._wishlist;
            // }
            _this.bannerList = _this.homePageModel["slider"];
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            _this.alert.myAlertMethod("Network Error", err.error.message, function (data) {
                console.log("hello alert");
            });
            _this.loadingService.dismiss();
        });
    };
    HomePage.prototype.userSearch = function (event) {
        this.getUsers();
        var textSearch = event.target.value;
        this.textToSearch = textSearch;
        console.log(textSearch);
    };
    HomePage.prototype.showCategories = function () {
        var _this = this;
        this.loadingService.present();
        this.homepageService.showCategories().subscribe(function (resp) {
            console.log("resp", resp);
            var data = resp.data.categories;
            _this.categories = data;
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            _this.alert.myAlertMethod("Network Error", err.error.message, function (data) {
                console.log("hello alert");
            });
            _this.loadingService.dismiss();
        });
    };
    HomePage.prototype.slideChanged = function () {
        var currentIndex = this.slides.getActiveIndex();
        // this.showLeftButton = currentIndex !=== 0;
        // this.showRightButton = currentIndex !=== Math.ceil(this.slides.length['length'] / 3);
    };
    // Method that shows the next slide
    HomePage.prototype.slideNext = function () {
        this.slides.slideNext();
    };
    // Method that shows the previous slide
    HomePage.prototype.slidePrev = function () {
        this.slides.slidePrev();
    };
    HomePage.prototype.categoryClick = function (text) {
        console.log("hello", text);
        this.router.navigate(['subcategory'], { queryParams: { 'id': text } });
    };
    HomePage.prototype.showCategories123 = function () {
        var _this = this;
        this.homepageService.showCategories().subscribe(function (resp) {
            var menuArray = [];
            var data = resp.data.categories;
            _this.categories = data;
            for (var i = 0; i < _this.categories.length; i++) {
                console.log(_this.categories[i]);
                menuArray.push({
                    "title": _this.categories[i].name,
                    "url": "arrow-dropright",
                    "icon": "play"
                });
            }
            _this.events.publish('user:created', menuArray);
            console.log(menuArray);
        }, function (err) {
            console.log(err);
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonSlides"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonSlides"])
    ], HomePage.prototype, "slides", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('mySelect'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ionic_selectable__WEBPACK_IMPORTED_MODULE_4__["IonicSelectableComponent"])
    ], HomePage.prototype, "selectComponent", void 0);
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.page.html */ "./src/app/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Events"], _Service_loader_service__WEBPACK_IMPORTED_MODULE_9__["LoaderService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_8__["AlerServiceService"], _Service_homepage_service__WEBPACK_IMPORTED_MODULE_7__["HomepageService"], _Service_cart_service__WEBPACK_IMPORTED_MODULE_6__["CartService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _Service_user_service_service__WEBPACK_IMPORTED_MODULE_5__["UserServiceService"]])
    ], HomePage);
    return HomePage;
}());



/***/ })

}]);
//# sourceMappingURL=home-home-module.js.map