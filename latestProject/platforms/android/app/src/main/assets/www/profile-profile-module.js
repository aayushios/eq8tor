(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile-profile-module"],{

/***/ "./src/app/Service/loader.service.ts":
/*!*******************************************!*\
  !*** ./src/app/Service/loader.service.ts ***!
  \*******************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var LoaderService = /** @class */ (function () {
    function LoaderService(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
        this.isLoading = false;
    }
    LoaderService.prototype.present = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isLoading = true;
                        return [4 /*yield*/, this.loadingCtrl.create({ duration: 5000, }).then(function (a) {
                                a.present().then(function () {
                                    if (!_this.isLoading) {
                                        a.dismiss().then(function () { return console.log('abort presenting'); });
                                    }
                                });
                            })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    LoaderService.prototype.dismiss = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isLoading = false;
                        return [4 /*yield*/, this.loadingCtrl.dismiss().then(function () { return console.log('dismissed'); })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    LoaderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])
    ], LoaderService);
    return LoaderService;
}());



/***/ }),

/***/ "./src/app/profile/profile.module.ts":
/*!*******************************************!*\
  !*** ./src/app/profile/profile.module.ts ***!
  \*******************************************/
/*! exports provided: ProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile.page */ "./src/app/profile/profile.page.ts");







var routes = [
    {
        path: '',
        component: _profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]
    }
];
var ProfilePageModule = /** @class */ (function () {
    function ProfilePageModule() {
    }
    ProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]]
        })
    ], ProfilePageModule);
    return ProfilePageModule;
}());



/***/ }),

/***/ "./src/app/profile/profile.page.html":
/*!*******************************************!*\
  !*** ./src/app/profile/profile.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"new-background-color\">\n    <ion-title class=\"header-title\">Profile</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"color: white\" autoHide=\"false\"></ion-menu-button>\n    </ion-buttons>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"openCart()\">\n        <!-- <ion-badge class=\"badge-color\" *ngIf=\"cart.length > 0\">{{ cart.length }}</ion-badge> -->\n        <ion-icon slot=\"icon-only\" name=\"cart\" style=\"color: white\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content class=\"profile-content\">\n  <div class=\"profile-avatar-info\">\n    <!-- <ion-card class=\"profile-card\">\n      <ion-card-content class=\"profile-card-content\">\n        <div class=\"profile\" *ngIf=\"!edited\">\n          <ion-avatar class=\"profile-media\">\n            <img src=\"{{apiUrl}}{{UrlImage}}\">\n          </ion-avatar>\n          <ion-input value=\"name\" [readonly]=\"isReadOnly\" [(ngModel)]=\"name\"></ion-input>\n          <ion-input value=\"email\" [readonly]=\"isReadOnly\" [(ngModel)]=\"email\"></ion-input>\n          <ion-button (click)='editAction()'> {{editButton}} </ion-button>\n        </div>\n        <div class=\"profile\" *ngIf=\"edited\">\n          <ion-avatar class=\"profile-media\">\n            <!-- <img *ngIf=\"!fileUrl\" src=\"{{apiUrl}}{{UrlImage}}\"> -->\n            <!-- <img src=\"///Users/aayushkatiyar/Library/Developer/CoreSimulator/Devices/0899108A-9867-41D7-A0ED-7BDCB88B51BB/data/Containers/Data/Application/641A3AE1-18BA-445B-B87D-2EF74457786D/tmp/cdv_photo_003.jpg\">\n          </ion-avatar>\n          <ion-button color=\"medium\" size=\"large\" (click)=\"cropUpload()\">\n            <ion-icon slot=\"icon-only\" name=\"camera\"></ion-icon>\n          </ion-button>\n          <ion-input value=\"name\" [(ngModel)]=\"name\"></ion-input>\n          <ion-input value=\"email\" [(ngModel)]=\"email\"> hello</ion-input>\n          <ion-button (click)='editAction()'> {{editButton}} </ion-button>\n        </div>\n      </ion-card-content>\n    </ion-card> -->\n    <ion-grid>\n      <ion-row>\n        <ion-col text-center>\n          <h3>Image Picker</h3>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col text-center>\n          <ion-button (click)=\"getImages()\">Choose Images</ion-button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <!-- More Pinterest floating gallery style -->\n          <div class=\"images\">\n            <div class=\"one-image\" *ngFor=\"let img of imageResponse\">\n              <img src=\"{{img}}\" alt=\"\" srcset=\"\">\n            </div>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <div>\n    <ion-grid>\n      <ion-row>\n        <ion-col col-6>\n          <div>\n            <button ion-button full>\n            </button>\n            <img src=\"../../assets/imgs/home.png\"><br>\n            <ion-label class=\"label\">My Address</ion-label>\n\n          </div>\n\n        </ion-col>\n        <ion-col col-6>\n          <div>\n            <button ion-button full>\n            </button>\n            <img src=\"../../assets/imgs/order.png\"><br>\n            <ion-label class=\"label\">My Orders</ion-label>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/profile/profile.page.scss":
/*!*******************************************!*\
  !*** ./src/app/profile/profile.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".new-background-color {\n  --background: #b31117; }\n\n.header-title {\n  color: #fff;\n  text-align: center; }\n\n.badge-color {\n  --background: #b31117;\n  color: white; }\n\n.profile-content {\n  background: #f4f8fc;\n  --background: var(--ion-background-color,#f4f8fc); }\n\n.profile-avatar-info {\n  padding: 10px 0;\n  background: #fff;\n  border: none; }\n\n.profile-card {\n  --background: #fff;\n  text-align: center;\n  -webkit-margin-start: 0;\n  margin-inline-start: 0;\n  -webkit-margin-end: 0;\n  margin-inline-end: 0;\n  margin-top: 0;\n  margin-bottom: 0;\n  box-shadow: none; }\n\n.profile-media {\n  margin: 0 auto; }\n\n.image-container {\n  min-height: 200px;\n  background-size: cover; }\n\n@media (min-width: 0px) {\n  .images {\n    -webkit-column-count: 2;\n       -moz-column-count: 2;\n            column-count: 2; } }\n\n@media (min-width: 420px) {\n  .images {\n    -webkit-column-count: 3;\n       -moz-column-count: 3;\n            column-count: 3; } }\n\n@media (min-width: 720px) {\n  .images {\n    -webkit-column-count: 4;\n       -moz-column-count: 4;\n            column-count: 4; } }\n\n.one-image {\n  margin: 2px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL3Byb2ZpbGUvcHJvZmlsZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxxQkFBYSxFQUFBOztBQUdqQjtFQUNJLFdBQVc7RUFDWCxrQkFDSixFQUFBOztBQUVBO0VBQ0kscUJBQWE7RUFDYixZQUFZLEVBQUE7O0FBSWhCO0VBQ0UsbUJBQW1CO0VBQ2pCLGlEQUFhLEVBQUE7O0FBR2pCO0VBQXNCLGVBQWU7RUFBRSxnQkFBZ0I7RUFBRSxZQUFZLEVBQUE7O0FBRXJFO0VBQWtCLGtCQUFhO0VBQzNCLGtCQUFrQjtFQUNsQix1QkFBdUI7RUFDdkIsc0JBQXNCO0VBQ3RCLHFCQUFxQjtFQUNyQixvQkFBb0I7RUFDcEIsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixnQkFBZ0IsRUFBQTs7QUFFcEI7RUFBZSxjQUFjLEVBQUE7O0FBRTdCO0VBQ0ksaUJBQWlCO0VBQ2pCLHNCQUFzQixFQUFBOztBQUd4QjtFQUNFO0lBQ0UsdUJBQWU7T0FBZixvQkFBZTtZQUFmLGVBQWUsRUFBQSxFQUNoQjs7QUFHSDtFQUNFO0lBQ0UsdUJBQWU7T0FBZixvQkFBZTtZQUFmLGVBQWUsRUFBQSxFQUNoQjs7QUFHSDtFQUNFO0lBQ0UsdUJBQWU7T0FBZixvQkFBZTtZQUFmLGVBQWUsRUFBQSxFQUNoQjs7QUFHSDtFQUNFLFdBQVcsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3Byb2ZpbGUvcHJvZmlsZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubmV3LWJhY2tncm91bmQtY29sb3Ige1xuICAgIC0tYmFja2dyb3VuZDogI2IzMTExNztcbn1cblxuLmhlYWRlci10aXRsZSB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyXG59XG5cbi5iYWRnZS1jb2xvciB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjYjMxMTE3O1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cblxuXG4ucHJvZmlsZS1jb250ZW50e1xuICBiYWNrZ3JvdW5kOiAjZjRmOGZjO1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IsI2Y0ZjhmYyk7XG59XG5cbi5wcm9maWxlLWF2YXRhci1pbmZveyBwYWRkaW5nOiAxMHB4IDA7IGJhY2tncm91bmQ6ICNmZmY7IGJvcmRlcjogbm9uZX1cblxuLnByb2ZpbGUtY2FyZHsgICAgLS1iYWNrZ3JvdW5kOiAjZmZmO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAtd2Via2l0LW1hcmdpbi1zdGFydDogMDtcbiAgICBtYXJnaW4taW5saW5lLXN0YXJ0OiAwO1xuICAgIC13ZWJraXQtbWFyZ2luLWVuZDogMDtcbiAgICBtYXJnaW4taW5saW5lLWVuZDogMDtcbiAgICBtYXJnaW4tdG9wOiAwO1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG4gICAgYm94LXNoYWRvdzogbm9uZTt9XG5cbi5wcm9maWxlLW1lZGlhe21hcmdpbjogMCBhdXRvO31cblxuLmltYWdlLWNvbnRhaW5lciB7XG4gICAgbWluLWhlaWdodDogMjAwcHg7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgfVxuICAgXG4gIEBtZWRpYSAobWluLXdpZHRoOiAwcHgpIHtcbiAgICAuaW1hZ2VzIHtcbiAgICAgIGNvbHVtbi1jb3VudDogMjtcbiAgICB9XG4gIH1cbiAgIFxuICBAbWVkaWEgKG1pbi13aWR0aDogNDIwcHgpIHtcbiAgICAuaW1hZ2VzIHtcbiAgICAgIGNvbHVtbi1jb3VudDogMztcbiAgICB9XG4gIH1cbiAgIFxuICBAbWVkaWEgKG1pbi13aWR0aDogNzIwcHgpIHtcbiAgICAuaW1hZ2VzIHtcbiAgICAgIGNvbHVtbi1jb3VudDogNDtcbiAgICB9XG4gIH1cbiAgIFxuICAub25lLWltYWdlIHtcbiAgICBtYXJnaW46IDJweDtcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/profile/profile.page.ts":
/*!*****************************************!*\
  !*** ./src/app/profile/profile.page.ts ***!
  \*****************************************/
/*! exports provided: ProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePage", function() { return ProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/image-picker/ngx */ "./node_modules/@ionic-native/image-picker/ngx/index.js");
/* harmony import */ var _Service_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Service/auth.service */ "./src/app/Service/auth.service.ts");
/* harmony import */ var _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Service/aler-service.service */ "./src/app/Service/aler-service.service.ts");
/* harmony import */ var _Service_loader_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Service/loader.service */ "./src/app/Service/loader.service.ts");
/* harmony import */ var _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/crop/ngx */ "./node_modules/@ionic-native/crop/ngx/index.js");
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ "./node_modules/@ionic-native/file-transfer/ngx/index.js");










var ProfilePage = /** @class */ (function () {
    function ProfilePage(navCtrl, crop, transfer, imagePicker, loadingService, alert, authService, router, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.crop = crop;
        this.transfer = transfer;
        this.imagePicker = imagePicker;
        this.loadingService = loadingService;
        this.alert = alert;
        this.authService = authService;
        this.router = router;
        this.toastCtrl = toastCtrl;
        this.apiUrl = 'https://www.niletechinnovations.com/eq8tor';
        this.selectedPath = '';
        this.name = '';
        this.email = '';
        this.UrlImage = '';
        this.isReadOnly = true;
        this.editButton = "Edit";
        this.profilePageModel = [];
        this.fileUrl = null;
        this.router.events.subscribe(function (event) {
            _this.selectedPath = event.url;
        });
        this.dynamicColor = 'redish';
        this.getProfileDetail();
    }
    ProfilePage.prototype.ngOnInit = function () {
        console.log('ng init call');
    };
    ProfilePage.prototype.getProfileDetail = function () {
        var _this = this;
        this.loadingService.present();
        this.authService.profile().subscribe(function (response) {
            _this.profilePageModel = response['data'];
            _this.name = _this.profilePageModel['name'];
            _this.email = _this.profilePageModel['email'];
            _this.UrlImage = _this.profilePageModel['user_photo_url'];
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            _this.alert.myAlertMethod(err.message, "Please try again later.", function (data) {
                console.log("hello alert");
            });
            _this.loadingService.dismiss();
        });
    };
    ProfilePage.prototype.getImages = function () {
        var _this = this;
        this.options = {
            // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
            // selection of a single image, the plugin will return it.
            maximumImagesCount: 1,
            // max width and height to allow the images to be.  Will keep aspect
            // ratio no matter what.  So if both are 800, the returned image
            // will be at most 800 pixels wide and 800 pixels tall.  If the width is
            // 800 and height 0 the image will be 800 pixels wide if the source
            // is at least that wide.
            width: 200,
            //height: 200,
            // quality of resized image, defaults to 100
            quality: 25,
            // output type, defaults to FILE_URIs.
            // available options are 
            // window.imagePicker.OutputType.FILE_URI (0) or 
            // window.imagePicker.OutputType.BASE64_STRING (1)
            outputType: 1
        };
        this.imageResponse = [];
        this.imagePicker.getPictures(this.options).then(function (results) {
            for (var i = 0; i < results.length; i++) {
                _this.imageResponse.push('data:image/jpeg;base64,' + results[i]);
            }
        }, function (err) {
            alert(err);
        });
    };
    ProfilePage.prototype.cropUpload = function () {
        var _this = this;
        this.imagePicker.getPictures({ maximumImagesCount: 1, outputType: 0 }).then(function (results) {
            for (var i = 0; i < results.length; i++) {
                console.log('Image URI: ' + results[i]);
                _this.crop.crop(results[i], { quality: 100 })
                    .then(function (newImage) {
                    console.log('new image path is: ' + newImage);
                    _this.fileUrl = newImage;
                    var fileTransfer = _this.transfer.create();
                    var uploadOpts = {
                        fileKey: 'file',
                        fileName: newImage.substr(newImage.lastIndexOf('/') + 1)
                    };
                    fileTransfer.upload(newImage, 'http://192.168.0.7:3000/api/upload', uploadOpts)
                        .then(function (data) {
                        console.log(data);
                        _this.respData = JSON.parse(data.response);
                        console.log(_this.respData);
                        _this.fileUrl = _this.respData.fileUrl;
                        console.log("this.file url", _this.fileUrl);
                    }, function (err) {
                        console.log(err);
                    });
                }, function (error) { return console.error('Error cropping image', error); });
            }
        }, function (err) { console.log("errorrorororo", err); });
    };
    ProfilePage.prototype.editAction = function () {
        if (this.editButton === "Edit") {
            this.editButton = "Save";
            this.edited = true;
        }
        else {
            this.editButton = "Edit";
            this.edited = false;
        }
    };
    ProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__(/*! ./profile.page.html */ "./src/app/profile/profile.page.html"),
            styles: [__webpack_require__(/*! ./profile.page.scss */ "./src/app/profile/profile.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_8__["Crop"],
            _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_9__["FileTransfer"], _ionic_native_image_picker_ngx__WEBPACK_IMPORTED_MODULE_4__["ImagePicker"], _Service_loader_service__WEBPACK_IMPORTED_MODULE_7__["LoaderService"], _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_6__["AlerServiceService"], _Service_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])
    ], ProfilePage);
    return ProfilePage;
}());



/***/ })

}]);
//# sourceMappingURL=profile-profile-module.js.map