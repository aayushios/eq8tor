(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["address-address-module"],{

/***/ "./src/app/address/address.module.ts":
/*!*******************************************!*\
  !*** ./src/app/address/address.module.ts ***!
  \*******************************************/
/*! exports provided: AddressPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddressPageModule", function() { return AddressPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _address_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./address.page */ "./src/app/address/address.page.ts");







var routes = [
    {
        path: '',
        component: _address_page__WEBPACK_IMPORTED_MODULE_6__["AddressPage"]
    }
];
var AddressPageModule = /** @class */ (function () {
    function AddressPageModule() {
    }
    AddressPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_address_page__WEBPACK_IMPORTED_MODULE_6__["AddressPage"]]
        })
    ], AddressPageModule);
    return AddressPageModule;
}());



/***/ }),

/***/ "./src/app/address/address.page.html":
/*!*******************************************!*\
  !*** ./src/app/address/address.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"new-background-color\">\n    <ion-title class=\"header-title\">MY ADDRESSES</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-button color=\"dark\" (click)=\"goBack()\">\n        <ion-icon class=\"header-arrow\" name=\"arrow-back\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <div class=\"text-right\">\n    <ion-button class=\"btn-view\" (click)=\"editAddress()\">\n      Edit Address\n    </ion-button>\n  </div>\n  <ion-card class=\"address-Card\">\n    <h2> Billing Address</h2>\n    <ion-label>{{billingAddressFirstName }} {{billingAddressLastName}}</ion-label><br>\n    <ion-card-content class=\"content-Card\">\n      <ion-label> Company: {{billCompanyName}} </ion-label><br>\n      <ion-label> Address: {{billAddress}} </ion-label><br>\n      <ion-label> City: {{billCity}} </ion-label><br>\n      <ion-label> Post Code: {{billPostCode}} </ion-label><br>\n      <ion-label> Country: {{billCountry}} </ion-label><br>\n      <ion-label> Phone: {{billPhone}} </ion-label><br>\n      <ion-label> Email: {{billEmail}} </ion-label><br>\n    </ion-card-content>\n  </ion-card>\n  <ion-card class=\"address-Card\">\n    <h2> Shipping Address</h2>\n    <ion-label>{{shippingAddressFirstName }} {{shippingAddressLastName}}</ion-label><br>\n    <ion-card-content class=\"content-Card\">\n      <ion-label> Company: {{shipCompanyName}} </ion-label><br>\n      <ion-label> Address: {{shipAddress}} </ion-label><br>\n      <ion-label> City: {{shipCity}} </ion-label><br>\n      <ion-label> Post Code: {{shipPostCode}} </ion-label><br>\n      <ion-label> Country: {{shipCountry}} </ion-label><br>\n      <ion-label> Phone: {{shipPhone}} </ion-label><br>\n      <ion-label> Email: {{shipEmail}} </ion-label><br>\n    </ion-card-content>\n  </ion-card>\n</ion-content>"

/***/ }),

/***/ "./src/app/address/address.page.scss":
/*!*******************************************!*\
  !*** ./src/app/address/address.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".new-background-color {\n  --background: #b31117; }\n\n.header-title {\n  color: #fff;\n  text-align: center; }\n\n.badge-color {\n  --background: #b31117;\n  color: white; }\n\n.header-arrow {\n  color: white; }\n\n.address-Card {\n  background: #ffffff;\n  padding: 15px;\n  -webkit-margin-start: 0;\n  margin-inline-start: 0;\n  -webkit-margin-end: 0;\n  margin-inline-end: 0; }\n\n.address-Card h2 {\n  font-size: 18px;\n  font-weight: 600;\n  padding-top: 0;\n  margin-top: 0;\n  color: #000000; }\n\n.content-Card {\n  padding: 0px 0 0 0;\n  margin-top: 0;\n  color: #000000;\n  font-weight: 250;\n  font-size: 13px; }\n\n.btn-view {\n  color: #fff;\n  font-weight: bold;\n  font-size: 14px;\n  --border-radius: 2px;\n  display: inline-block;\n  margin: 0;\n  height: auto;\n  --padding-top: 15px;\n  --padding-bottom: 15px;\n  --padding-start: 30px;\n  --padding-end: 30px;\n  --background: #b31117;\n  --background-activated:#b31117; }\n\n.text-right {\n  text-align: right; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL2FkZHJlc3MvYWRkcmVzcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxxQkFBYSxFQUFBOztBQUdqQjtFQUNJLFdBQVc7RUFDWCxrQkFDSixFQUFBOztBQUVBO0VBQ0kscUJBQWE7RUFDYixZQUFZLEVBQUE7O0FBRWhCO0VBRUksWUFBWSxFQUFBOztBQUVoQjtFQUVJLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLHNCQUFzQjtFQUN0QixxQkFBcUI7RUFDckIsb0JBQW9CLEVBQUE7O0FBRXhCO0VBQ0ksZUFBZTtFQUNmLGdCQUFlO0VBQ2YsY0FBYztFQUNkLGFBQVk7RUFDWixjQUFjLEVBQUE7O0FBSWxCO0VBQ0ksa0JBQWtCO0VBQ2xCLGFBQVk7RUFDWixjQUFhO0VBQ2IsZ0JBQWU7RUFDZixlQUFlLEVBQUE7O0FBRW5CO0VBQWMsV0FBVztFQUNyQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLG9CQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsU0FBUztFQUNULFlBQVk7RUFDWixtQkFBYztFQUNkLHNCQUFpQjtFQUNqQixxQkFBZ0I7RUFDaEIsbUJBQWM7RUFDZCxxQkFBYTtFQUNiLDhCQUF1QixFQUFBOztBQUV6QjtFQUNJLGlCQUFpQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvYWRkcmVzcy9hZGRyZXNzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5uZXctYmFja2dyb3VuZC1jb2xvciB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjYjMxMTE3O1xufVxuXG4uaGVhZGVyLXRpdGxlIHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXJcbn1cblxuLmJhZGdlLWNvbG9yIHtcbiAgICAtLWJhY2tncm91bmQ6ICNiMzExMTc7XG4gICAgY29sb3I6IHdoaXRlO1xufVxuLmhlYWRlci1hcnJvd3tcblxuICAgIGNvbG9yOiB3aGl0ZTtcbn1cbi5hZGRyZXNzLUNhcmR7XG5cbiAgICBiYWNrZ3JvdW5kIDojZmZmZmZmO1xuICAgIHBhZGRpbmc6IDE1cHg7XG4gICAgLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IDA7XG4gICAgbWFyZ2luLWlubGluZS1zdGFydDogMDtcbiAgICAtd2Via2l0LW1hcmdpbi1lbmQ6IDA7XG4gICAgbWFyZ2luLWlubGluZS1lbmQ6IDA7XG59XG4uYWRkcmVzcy1DYXJkIGgye1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBmb250LXdlaWdodDo2MDA7XG4gICAgcGFkZGluZy10b3A6IDA7XG4gICAgbWFyZ2luLXRvcDowO1xuICAgIGNvbG9yOiAjMDAwMDAwO1xuXG59XG5cbi5jb250ZW50LUNhcmR7XG4gICAgcGFkZGluZzogMHB4IDAgMCAwO1xuICAgIG1hcmdpbi10b3A6MDtcbiAgICBjb2xvcjojMDAwMDAwO1xuICAgIGZvbnQtd2VpZ2h0OjI1MDtcbiAgICBmb250LXNpemU6IDEzcHg7XG59XG4uYnRuLXZpZXd7ICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDJweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWFyZ2luOiAwO1xuICAgIGhlaWdodDogYXV0bztcbiAgICAtLXBhZGRpbmctdG9wOiAxNXB4O1xuICAgIC0tcGFkZGluZy1ib3R0b206IDE1cHg7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAzMHB4O1xuICAgIC0tcGFkZGluZy1lbmQ6IDMwcHg7XG4gICAgLS1iYWNrZ3JvdW5kOiAjYjMxMTE3O1xuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6I2IzMTExNztcbiAgfVxuICAudGV4dC1yaWdodHtcbiAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICB9XG5cbiJdfQ== */"

/***/ }),

/***/ "./src/app/address/address.page.ts":
/*!*****************************************!*\
  !*** ./src/app/address/address.page.ts ***!
  \*****************************************/
/*! exports provided: AddressPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddressPage", function() { return AddressPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Service_loader_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Service/loader.service */ "./src/app/Service/loader.service.ts");
/* harmony import */ var _Service_homepage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Service/homepage.service */ "./src/app/Service/homepage.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _edit_address_edit_address_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../edit-address/edit-address.page */ "./src/app/edit-address/edit-address.page.ts");






var AddressPage = /** @class */ (function () {
    function AddressPage(homePageService, modalController, loadingService, navCtrl) {
        this.homePageService = homePageService;
        this.modalController = modalController;
        this.loadingService = loadingService;
        this.navCtrl = navCtrl;
        this.billCompanyName = "";
        this.billAddress = "";
        this.billCity = "";
        this.billPostCode = "";
        this.billCountry = "";
        this.billPhone = "";
        this.billEmail = "";
        this.billingAddressFirstName = "";
        this.billingAddressLastName = "";
        this.shipCompanyName = "";
        this.shipAddress = "";
        this.shipCity = "";
        this.shipPostCode = "";
        this.shipCountry = "";
        this.shipPhone = "";
        this.shipEmail = "";
        this.shippingAddressFirstName = "";
        this.shippingAddressLastName = "";
        this.getWishlist();
    }
    AddressPage.prototype.ngOnInit = function () {
    };
    AddressPage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    AddressPage.prototype.getWishlist = function () {
        var _this = this;
        this.loadingService.present();
        this.homePageService.getWishlistProducts().subscribe(function (resp) {
            _this.billCompanyName = resp.data.address_details.account_bill_company_name;
            _this.billAddress = resp.data.address_details.account_bill_adddress_line_1;
            _this.billCity = resp.data.address_details.account_bill_title;
            _this.billPostCode = resp.data.address_details.account_bill_zip_or_postal_code;
            _this.billCountry = resp.data.address_details.account_bill_select_country;
            _this.billPhone = resp.data.address_details.account_bill_phone_number;
            _this.billEmail = resp.data.address_details.account_bill_email_address;
            _this.billingAddressFirstName = resp.data.address_details.account_bill_first_name;
            _this.billingAddressLastName = resp.data.address_details.account_bill_last_name;
            _this.shipCompanyName = resp.data.address_details.account_shipping_company_name;
            _this.shipAddress = resp.data.address_details.account_shipping_adddress_line_1;
            _this.shipCity = resp.data.address_details.account_shipping_title;
            _this.shipPostCode = resp.data.address_details.account_shipping_zip_or_postal_code;
            _this.shipCountry = resp.data.address_details.account_shipping_select_country;
            _this.shipPhone = resp.data.address_details.account_shipping_phone_number;
            _this.shipEmail = resp.data.address_details.account_shipping_email_address;
            _this.shippingAddressFirstName = resp.data.address_details.account_shipping_first_name;
            _this.shippingAddressLastName = resp.data.address_details.account_shipping_last_name;
            _this.loadingService.dismiss();
        }, function (err) {
            console.log("error", err);
            _this.loadingService.dismiss();
        });
    };
    AddressPage.prototype.editAddress = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _edit_address_edit_address_page__WEBPACK_IMPORTED_MODULE_5__["EditAddressPage"],
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (sizeChartResponse) {
                            if (sizeChartResponse !== null) {
                                //this.sizeChartResponse = sizeChartResponse.data;
                            }
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    AddressPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-address',
            template: __webpack_require__(/*! ./address.page.html */ "./src/app/address/address.page.html"),
            styles: [__webpack_require__(/*! ./address.page.scss */ "./src/app/address/address.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_Service_homepage_service__WEBPACK_IMPORTED_MODULE_3__["HomepageService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"], _Service_loader_service__WEBPACK_IMPORTED_MODULE_2__["LoaderService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]])
    ], AddressPage);
    return AddressPage;
}());



/***/ })

}]);
//# sourceMappingURL=address-address-module.js.map