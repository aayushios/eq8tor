(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["forgotpassword-forgotpassword-module"],{

/***/ "./src/app/forgotpassword/forgotpassword.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/forgotpassword/forgotpassword.module.ts ***!
  \*********************************************************/
/*! exports provided: ForgotpasswordPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotpasswordPageModule", function() { return ForgotpasswordPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _forgotpassword_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./forgotpassword.page */ "./src/app/forgotpassword/forgotpassword.page.ts");







var routes = [
    {
        path: '',
        component: _forgotpassword_page__WEBPACK_IMPORTED_MODULE_6__["ForgotpasswordPage"]
    }
];
var ForgotpasswordPageModule = /** @class */ (function () {
    function ForgotpasswordPageModule() {
    }
    ForgotpasswordPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_forgotpassword_page__WEBPACK_IMPORTED_MODULE_6__["ForgotpasswordPage"]]
        })
    ], ForgotpasswordPageModule);
    return ForgotpasswordPageModule;
}());



/***/ }),

/***/ "./src/app/forgotpassword/forgotpassword.page.html":
/*!*********************************************************!*\
  !*** ./src/app/forgotpassword/forgotpassword.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-button color=\"dark\" (click)=\"goBack()\">\n        <ion-icon name=\"arrow-back\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-label class=\"label\">Forgot Password?</ion-label>\n  <p class=\"p-text\">If you have forgotten your password - reset it here.</p>\n  <form [formGroup]=\"forgotForm\">\n    <div class=\"view-form\">\n      <ion-item class=\"user-input\" lines=\"none\">\n        <ion-input class=\"username form-control\" placeholder=\"Email Address\" formControlName=\"forgot_email_id\"\n          pattern=\"[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})\">\n        </ion-input>\n      </ion-item>\n      <p class=\"validate-msg-alert\"\n        *ngIf=\"forgotForm.controls.forgot_email_id.touched  && (!forgotForm.controls.forgot_email_id.valid)\">\n        Please enter mandatory field\n      </p>\n      <ion-item class=\"password-input\" lines=\"none\">\n        <ion-input class=\"password form-control\"  (keydown.space)=\"$event.preventDefault();\" [type]=\"passwordType1\" placeholder=\"New Password\"\n          formControlName=\"forgot_password\"></ion-input>\n        <button class=\"showBtn\" item-right (click)='hideShowPassword1()'>{{passwordIcon1}}</button>\n      </ion-item>\n      <p class=\"validate-msg-alert\"\n        *ngIf=\"forgotForm.controls.forgot_password.touched  && (!forgotForm.controls.forgot_password.valid)\">\n        Please enter new password\n      </p>\n      <ion-item class=\"password-input\" lines=\"none\">\n        <ion-input class=\"password form-control\" (keydown.space)=\"$event.preventDefault();\" [type]=\"passwordType2\" placeholder=\"Retype Password\"\n          formControlName=\"forgot_repeat_password\"></ion-input>\n        <button class=\"showBtn\" item-right (click)='hideShowPassword2()'>{{passwordIcon2}}</button>\n      </ion-item>\n      <p class=\"validate-msg-alert\"\n        *ngIf=\"forgotForm.controls.forgot_password.touched  && (!forgotForm.controls.forgot_password.valid)\">\n        Please enter password again\n      </p>\n      <ion-item class=\"submit-btn\" lines=\"none\">\n        <ion-button class=\"Login\" type=\"submit\" [disabled]=\"!forgotForm.valid\" (click)=\"onFormSubmit(forgotForm.value)\">\n          Reset My Password</ion-button>\n      </ion-item>\n    </div>\n  </form>\n</ion-content>"

/***/ }),

/***/ "./src/app/forgotpassword/forgotpassword.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/forgotpassword/forgotpassword.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ion-label.label {\n  display: block;\n  padding-top: 20px;\n  margin-left: 0px;\n  font-size: 21px;\n  font-weight: 500; }\n\n:host .item-interactive.item-has-focus {\n  --highlight-background:transparent;\n  height: auto; }\n\n:host .text {\n  margin-left: 0px;\n  display: block;\n  color: lightgray;\n  font-size: 15px;\n  font-weight: 500; }\n\n:host .p-text {\n  color: lightgray; }\n\n:host button.signUp {\n  background: white;\n  font-size: 14px;\n  outline: none;\n  color: #b31117;\n  font-weight: 500; }\n\n:host button.showBtn {\n  margin-right: 10px;\n  background: #003a54;\n  color: white;\n  height: 20px;\n  position: absolute;\n  right: 5px;\n  z-index: 9;\n  font-weight: 500;\n  border-radius: 2px;\n  font-size: 12px; }\n\n:host ion-button.Login {\n  --border-radius: 2px;\n  margin-top: 30px;\n  height: 40px;\n  --background: #b31117;\n  color: white;\n  width: 100%;\n  font-weight: 600;\n  --padding-start: 0px !important;\n  --background-activated: var(--ion-color-shade, none);\n  --background-focused: var(--background); }\n\n:host ion-input.username {\n  border: 0.5px solid gray; }\n\n:host ion-input.password {\n  border: 0.5px solid gray; }\n\n:host .user-input,\n:host .password-input {\n  --inner-border-width: 0px 0px 0px 0px;\n  margin-right: 0px;\n  margin-bottom: 8px;\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n  --border-color: #fff;\n  --padding-end: 0; }\n\n:host .user-input .item-highlight,\n:host .password-input .item-highlight {\n  background: transparent !important; }\n\n:host .username,\n:host .password {\n  --padding-start: 8px; }\n\n:host ion-item.submit-btn {\n  --padding-start: 0;\n  --inner-padding-end: 0; }\n\n:host .user-input,\n:host .password-input {\n  --inner-border-width: 0px 0px 0px 0px;\n  margin-right: 0px;\n  margin-bottom: 8px;\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n  --border-color: #fff;\n  --padding-end: 0; }\n\n:host .forgot-password {\n  margin-left: auto;\n  margin-right: auto;\n  display: block;\n  background: none;\n  outline: none;\n  margin-top: 20px;\n  font-size: 15px; }\n\n:host .validate-msg-alert {\n  color: #FF545E;\n  font-size: 14px;\n  text-align: left;\n  padding: 0px 0;\n  margin-bottom: 10px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL2ZvcmdvdHBhc3N3b3JkL2ZvcmdvdHBhc3N3b3JkLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUdRLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixnQkFBZ0IsRUFBQTs7QUFQeEI7RUFVUSxrQ0FBdUI7RUFDdkIsWUFBWSxFQUFBOztBQVhwQjtFQWNRLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixnQkFBZ0IsRUFBQTs7QUFsQnhCO0VBc0JRLGdCQUFnQixFQUFBOztBQXRCeEI7RUEwQlEsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixhQUFhO0VBQ2IsY0FBYztFQUNkLGdCQUFnQixFQUFBOztBQTlCeEI7RUFrQ1Esa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsVUFBVTtFQUNWLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsZUFBZSxFQUFBOztBQTNDdkI7RUFnRFEsb0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1oscUJBQWE7RUFDYixZQUFZO0VBQ1osV0FBVztFQUNYLGdCQUFnQjtFQUNoQiwrQkFBZ0I7RUFDaEIsb0RBQXVCO0VBQ3ZCLHVDQUFxQixFQUFBOztBQXpEN0I7RUE2RFEsd0JBQXdCLEVBQUE7O0FBN0RoQztFQWlFUSx3QkFBd0IsRUFBQTs7QUFqRWhDOztFQXVFUSxxQ0FBcUI7RUFDckIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixvQkFBZ0I7RUFDaEIsd0JBQW9CO0VBQ3BCLG9CQUFlO0VBQ2YsZ0JBQWMsRUFBQTs7QUE3RXRCOztFQWdGUSxrQ0FBaUMsRUFBQTs7QUFoRnpDOztFQW1GYyxvQkFBZ0IsRUFBQTs7QUFuRjlCO0VBcUZRLGtCQUFnQjtFQUNoQixzQkFBb0IsRUFBQTs7QUF0RjVCOztFQTBGUSxxQ0FBcUI7RUFDckIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixvQkFBZ0I7RUFDaEIsd0JBQW9CO0VBQ3BCLG9CQUFlO0VBQ2YsZ0JBQWMsRUFBQTs7QUFoR3RCO0VBbUdRLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLGVBQWUsRUFBQTs7QUF6R3ZCO0VBNkdRLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxtQkFBbUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2ZvcmdvdHBhc3N3b3JkL2ZvcmdvdHBhc3N3b3JkLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcbiAgICBcbiAgICBpb24tbGFiZWwubGFiZWwge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgcGFkZGluZy10b3A6IDIwcHg7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwcHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMjFweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICB9XG4gICAgLml0ZW0taW50ZXJhY3RpdmUuaXRlbS1oYXMtZm9jdXMge1xuICAgICAgICAtLWhpZ2hsaWdodC1iYWNrZ3JvdW5kOnRyYW5zcGFyZW50O1xuICAgICAgICBoZWlnaHQ6IGF1dG87XG4gICB9XG4gICAgLnRleHQge1xuICAgICAgICBtYXJnaW4tbGVmdDogMHB4O1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgY29sb3I6IGxpZ2h0Z3JheTtcbiAgICAgICAgZm9udC1zaXplOiAxNXB4O1xuICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgIH1cblxuICAgIC5wLXRleHQge1xuICAgICAgICBjb2xvcjogbGlnaHRncmF5O1xuICAgIH1cblxuICAgIGJ1dHRvbi5zaWduVXAge1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICBvdXRsaW5lOiBub25lO1xuICAgICAgICBjb2xvcjogI2IzMTExNztcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICB9XG5cbiAgICBidXR0b24uc2hvd0J0biB7XG4gICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgICAgICAgYmFja2dyb3VuZDogIzAwM2E1NDtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICBoZWlnaHQ6IDIwcHg7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgcmlnaHQ6IDVweDtcbiAgICAgICAgei1pbmRleDogOTtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMnB4O1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG5cbiAgICB9XG5cbiAgICBpb24tYnV0dG9uLkxvZ2luIHtcbiAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAycHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDMwcHg7XG4gICAgICAgIGhlaWdodDogNDBweDtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjYjMxMTE3O1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDBweCAhaW1wb3J0YW50O1xuICAgICAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiB2YXIoLS1pb24tY29sb3Itc2hhZGUsIG5vbmUpO1xuICAgICAgICAtLWJhY2tncm91bmQtZm9jdXNlZDogdmFyKC0tYmFja2dyb3VuZCk7XG4gICAgfVxuXG4gICAgaW9uLWlucHV0LnVzZXJuYW1lIHtcbiAgICAgICAgYm9yZGVyOiAwLjVweCBzb2xpZCBncmF5O1xuICAgIH1cblxuICAgIGlvbi1pbnB1dC5wYXNzd29yZCB7XG4gICAgICAgIGJvcmRlcjogMC41cHggc29saWQgZ3JheTtcbiAgICB9XG5cblxuICAgIC51c2VyLWlucHV0LFxuICAgIC5wYXNzd29yZC1pbnB1dCAge1xuICAgICAgICAtLWlubmVyLWJvcmRlci13aWR0aDogMHB4IDBweCAwcHggMHB4O1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogOHB4O1xuICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcbiAgICAgICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xuICAgICAgICAtLWJvcmRlci1jb2xvcjogI2ZmZjtcbiAgICAgICAgLS1wYWRkaW5nLWVuZDogMDtcbiAgICB9LnVzZXItaW5wdXQgLml0ZW0taGlnaGxpZ2h0LFxuICAgIC5wYXNzd29yZC1pbnB1dCAuaXRlbS1oaWdobGlnaHQge1xuICAgICAgICBiYWNrZ3JvdW5kOnRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG4gICAgfVxuICAgIC51c2VybmFtZSxcbiAgICAucGFzc3dvcmR7LS1wYWRkaW5nLXN0YXJ0OiA4cHg7fVxuICAgIGlvbi1pdGVtLnN1Ym1pdC1idG4ge1xuICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gICAgICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDA7XG4gICAgfVxuICAgIC51c2VyLWlucHV0LFxuICAgIC5wYXNzd29yZC1pbnB1dCAge1xuICAgICAgICAtLWlubmVyLWJvcmRlci13aWR0aDogMHB4IDBweCAwcHggMHB4O1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogOHB4O1xuICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcbiAgICAgICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xuICAgICAgICAtLWJvcmRlci1jb2xvcjogI2ZmZjtcbiAgICAgICAgLS1wYWRkaW5nLWVuZDogMDtcbiAgICB9XG4gICAgLmZvcmdvdC1wYXNzd29yZCB7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBiYWNrZ3JvdW5kOiBub25lO1xuICAgICAgICBvdXRsaW5lOiBub25lO1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgICBmb250LXNpemU6IDE1cHg7XG4gICAgfVxuXG4gICAgLnZhbGlkYXRlLW1zZy1hbGVydCB7XG4gICAgICAgIGNvbG9yOiAjRkY1NDVFO1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgIHBhZGRpbmc6IDBweCAwO1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/forgotpassword/forgotpassword.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/forgotpassword/forgotpassword.page.ts ***!
  \*******************************************************/
/*! exports provided: ForgotpasswordPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotpasswordPage", function() { return ForgotpasswordPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _Service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Service/auth.service */ "./src/app/Service/auth.service.ts");
/* harmony import */ var _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Service/aler-service.service */ "./src/app/Service/aler-service.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");






var ForgotpasswordPage = /** @class */ (function () {
    function ForgotpasswordPage(formBuilder, navCtrl, alert, authService) {
        this.formBuilder = formBuilder;
        this.navCtrl = navCtrl;
        this.alert = alert;
        this.authService = authService;
        this.passwordType1 = 'password';
        this.passwordIcon1 = 'Show';
        this.passwordType2 = 'password';
        this.passwordIcon2 = 'Show';
        this.forgotForm = this.formBuilder.group({
            forgot_email_id: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            forgot_password: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            forgot_repeat_password: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    }
    ForgotpasswordPage.prototype.ngOnInit = function () {
    };
    ForgotpasswordPage.prototype.hideShowPassword1 = function () {
        this.passwordType1 = this.passwordType1 === 'text' ? 'password' : 'text';
        this.passwordIcon1 = this.passwordIcon1 === 'Show' ? 'Hide' : 'Show';
    };
    ForgotpasswordPage.prototype.hideShowPassword2 = function () {
        this.passwordType2 = this.passwordType2 === 'text' ? 'password' : 'text';
        this.passwordIcon2 = this.passwordIcon2 === 'Show' ? 'Hide' : 'Show';
    };
    ForgotpasswordPage.prototype.onFormSubmit = function (form) {
        var _this = this;
        if (!(form['forgot_password'] == form['forgot_repeat_password'])) {
            this.alert.myAlertMethod("Error", "Password not same", function (data) {
                console.log("test forgot error");
            });
        }
        else {
            this.authService.forgot(form).subscribe(function (res) {
                console.log("forgot password api called successfull.");
            }, function (err) {
                _this.alert.myAlertMethod("Error", err.error.message, function (data) {
                    console.log("test forgot error");
                });
                console.log("error forgot", err.error.message);
            });
        }
    };
    ForgotpasswordPage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    ForgotpasswordPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-forgotpassword',
            template: __webpack_require__(/*! ./forgotpassword.page.html */ "./src/app/forgotpassword/forgotpassword.page.html"),
            styles: [__webpack_require__(/*! ./forgotpassword.page.scss */ "./src/app/forgotpassword/forgotpassword.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"], _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_4__["AlerServiceService"], _Service_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], ForgotpasswordPage);
    return ForgotpasswordPage;
}());



/***/ })

}]);
//# sourceMappingURL=forgotpassword-forgotpassword-module.js.map