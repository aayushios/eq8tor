(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["product-detail-product-detail-module"],{

/***/ "./src/app/Service/cart.service.ts":
/*!*****************************************!*\
  !*** ./src/app/Service/cart.service.ts ***!
  \*****************************************/
/*! exports provided: CartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartService", function() { return CartService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CartService = /** @class */ (function () {
    function CartService() {
        this.data = [
            {
                category: "Pizza",
                expanded: false,
                products: [
                    { id: 0, name: 'Salami', price: '8' },
                    { id: 1, name: 'Classic', price: '5' },
                    { id: 2, name: 'Tuna', price: '9' },
                    { id: 3, name: 'Hawai', price: '7' }
                ]
            },
            {
                category: 'Pasta',
                products: [
                    { id: 4, name: 'Mac & Cheese', price: '8' },
                    { id: 5, name: 'Bologenese', price: '6' }
                ]
            },
            {
                category: 'Salad',
                products: [
                    { id: 6, name: 'Ham & Egg', price: '8' },
                    { id: 7, name: 'Basic', price: '5' },
                    { id: 8, name: 'Ceaser', price: '9' }
                ]
            }
        ];
        this.cart = [];
    }
    CartService.prototype.getProducts = function () {
        return this.data;
    };
    CartService.prototype.getCart = function () {
        return this.cart;
    };
    CartService.prototype.addProduct = function (product) {
        this.cart.push(product);
    };
    CartService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CartService);
    return CartService;
}());



/***/ }),

/***/ "./src/app/product-detail/product-detail.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/product-detail/product-detail.module.ts ***!
  \*********************************************************/
/*! exports provided: ProductDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailPageModule", function() { return ProductDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _product_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./product-detail.page */ "./src/app/product-detail/product-detail.page.ts");







var routes = [
    {
        path: '',
        component: _product_detail_page__WEBPACK_IMPORTED_MODULE_6__["ProductDetailPage"]
    }
];
var ProductDetailPageModule = /** @class */ (function () {
    function ProductDetailPageModule() {
    }
    ProductDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_product_detail_page__WEBPACK_IMPORTED_MODULE_6__["ProductDetailPage"]]
        })
    ], ProductDetailPageModule);
    return ProductDetailPageModule;
}());



/***/ }),

/***/ "./src/app/product-detail/product-detail.page.html":
/*!*********************************************************!*\
  !*** ./src/app/product-detail/product-detail.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content class=\"home-content\">\n  <ion-toolbar class=\"product-single-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-button color=\"dark\" (click)=\"goBack()\">\n        <ion-icon class=\"header-arrow\" name=\"arrow-back\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n  <div *ngIf=\"detailsContent.post_image_url === ''\">\n    <ion-card-content class=\"product-card-media\">\n      <img src=\"../../assets/imgs/icon.png\">\n    </ion-card-content>\n  </div>\n  <div *ngIf=\"!detailsContent.post_image_url == ''\" class=\"product-single-slider\">\n    <ion-slides autoplay=\"5000\" loop=\"true\" speed=\"500\" class=\"slides\" pager=\"true\">\n      <ion-slide *ngFor=\"let cat of detailImageList\">\n        <img src=\"{{apiUrl}}{{cat.url}}\">\n      </ion-slide>\n    </ion-slides>\n  </div>\n  <div class=\"product-single-content\">\n    <h2>\n      {{detailsContent.post_title}}\n    </h2>\n    <div class=\"star-rating\">\n      <span style=\"width:50%\"></span>\n    </div>\n    <!-- <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs.\n      The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of\n      Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.\n    </p> -->\n    <p>{{detailsContent.post_content}}\n      </p>\n  </div>\n  <div class=\"product-single-size\">\n\n    \n    <h2>Size</h2>\n    <div class=\"size-scroll\" scrollX=\"true\">\n        <button [ngClass]=\"{'selected':category.isSelected}\"  class=\"btn-size size-span\" *ngFor=\"let category of sizeArray;let i = index\" (click)=\"btnActivate(i)\">{{category.title}}</button>\n    </div>\n    <ion-button class=\"btn-size-chart\" (click)=\"openSizeChart()\">\n      See Size Guide\n    </ion-button>\n    <button clear (click)=\"incrementQty()\"><ion-icon name=\"add-circle\" ></ion-icon></button>{{qty}}\n    <button clear (click)=\"decrementQty()\"><ion-icon name=\"remove-circle\" ></ion-icon></button>\n\n</div>\n</ion-content>\n<ion-footer class=\"product-single-footer\">\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"8\">\n        <ion-button *ngIf=\"!cartAdded\" class=\"addtocart-btn\" (click)=\"addToCart(detailsContent.id,qty)\">\n          Add To Cart\n        </ion-button>\n        <ion-button *ngIf=\"cartAdded\" class=\"addtocart-btn\" routerLink=\"/cart\">\n            Go To Cart\n          </ion-button>\n      </ion-col>\n      <ion-col size=\"4\">\n        <div class=\"single-product-price\">\n          <ion-label class=\"price\" (click)=\"getCartContent()\">\n            ${{detailsContent.post_price}}\n          </ion-label>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-footer>"

/***/ }),

/***/ "./src/app/product-detail/product-detail.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/product-detail/product-detail.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":root {\n  --ion-item-background-activated: #051b35; }\n\n.header-title {\n  color: #fff;\n  text-align: center; }\n\n.new-background-color {\n  --background: #b31117; }\n\n.selected {\n  background-color: #b31117 !important;\n  color: white !important;\n  outline: none;\n  box-shadow: none; }\n\n.star-rating {\n  font-family: \"FontAwesome\";\n  font-size: 16px;\n  height: 20px;\n  overflow: hidden;\n  position: relative;\n  width: 90px; }\n\n.star-rating {\n  margin: 0px auto; }\n\n.star-rating::before {\n  color: #656c72;\n  content: \"\\2605\\2605\\2605\\2605\\2605\";\n  float: left;\n  font-size: 16px;\n  left: 0;\n  letter-spacing: 2px;\n  position: absolute;\n  top: 0; }\n\n.star-rating span {\n  float: left;\n  left: 0;\n  overflow: hidden;\n  padding-top: 1.5em;\n  position: absolute;\n  top: 0; }\n\n.star-rating span::before {\n  color: #fab902;\n  content: \"\\2605\\2605\\2605\\2605\\2605\";\n  font-size: 16px;\n  left: 0;\n  letter-spacing: 2px;\n  position: absolute;\n  top: 0; }\n\n.star-rating .rating {\n  display: none; }\n\n.container ::-webkit-scrollbar {\n  display: none; }\n\n.container .size-scroll {\n  overflow: auto;\n  background-color: #b31117;\n  white-space: nowrap;\n  padding: 10px 12px 10px 12px; }\n\n.size-span {\n  margin-bottom: auto;\n  margin-left: 5px;\n  color: white;\n  background: #841014;\n  padding: 8px 10px;\n  border-radius: 2px;\n  font-weight: 600;\n  font-size: 12px; }\n\n.highlight-color {\n  color: blue; }\n\nion-slides {\n  color: #fff; }\n\nion-slides ion-slide {\n    align-items: start;\n    text-align: center; }\n\nion-slides ion-slide .slide-content .text p {\n      margin-top: 0;\n      margin-bottom: 10px; }\n\nion-content.home-content {\n  background: #f4f8fc;\n  --background: var(--ion-background-color, #f4f8fc)\n; }\n\nion-toolbar.product-single-toolbar {\n  position: absolute;\n  top: 25px;\n  right: 0;\n  left: 0;\n  --background: transparent;\n  --ion-color-base: transparent !important; }\n\n.product-single-content {\n  padding: 20px;\n  background: #fff;\n  margin-bottom: 10px; }\n\n.product-single-content h2 {\n  color: #213051;\n  font-size: 16px;\n  font-weight: 600;\n  margin-top: 0;\n  padding-top: 0; }\n\n.product-single-content p {\n  color: #213051;\n  font-size: 13px;\n  font-weight: 500;\n  line-height: 24px; }\n\n.product-single-size {\n  padding: 20px;\n  background: #fff;\n  margin-bottom: 10px; }\n\n.product-single-size h2 {\n  color: #213051;\n  font-size: 16px;\n  font-weight: 600;\n  margin-top: 0;\n  padding-top: 0; }\n\n.btn-size-chart {\n  color: #80889a;\n  font-size: 11px;\n  font-weight: normal;\n  line-height: 24px;\n  background: none;\n  border: none;\n  --background: transparent;\n  padding: 0;\n  -webkit-margin-start: 0;\n  margin-inline-start: 0;\n  -webkit-margin-end: 0;\n  margin-inline-end: 0;\n  text-decoration: underline; }\n\n.btn-size-chart.activated {\n  --background-activated: transparent;\n  --color: #80889a;\n  --color-activated: #80889a; }\n\nion-footer.product-single-footer {\n  background: #fff;\n  padding: 0px 20px;\n  box-shadow: 0 -4px 12px 0 rgba(158, 169, 181, 0.55); }\n\n.addtocart-btn {\n  color: #fff;\n  font-weight: bold;\n  text-transform: uppercase;\n  font-size: 14px;\n  --border-radius: 2px;\n  display: inline-block;\n  margin: 0;\n  height: auto;\n  --padding-top: 15px;\n  --padding-bottom: 15px;\n  --padding-start: 30px;\n  --padding-end: 30px;\n  --background: #b31117;\n  width: 100%; }\n\n.single-product-price {\n  padding: 10px 0;\n  text-align: center; }\n\n.single-product-price .price {\n  font-size: 20px;\n  font-weight: bold;\n  color: #213051; }\n\n.btn-size {\n  background: white;\n  border: 1px solid #d4dee8;\n  color: #213051;\n  --background-activated: var(--ion-item-background-activated); }\n\n.button.active {\n  background-color: green !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL3Byb2R1Y3QtZGV0YWlsL3Byb2R1Y3QtZGV0YWlsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHdDQUFnQyxFQUFBOztBQUdwQztFQUNJLFdBQVc7RUFDWCxrQkFDSixFQUFBOztBQUVBO0VBQ0kscUJBQWEsRUFBQTs7QUFHakI7RUFDSSxvQ0FBb0M7RUFDcEMsdUJBQXVCO0VBQ3ZCLGFBQWE7RUFDYixnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDSSwwQkFBMEI7RUFDMUIsZUFBZTtFQUNmLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFdBQVcsRUFBQTs7QUFHZjtFQUNJLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLGNBQWM7RUFDZCxvQ0FBb0M7RUFDcEMsV0FBVztFQUNYLGVBQWU7RUFDZixPQUFPO0VBQ1AsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixNQUFNLEVBQUE7O0FBR1Y7RUFDSSxXQUFXO0VBQ1gsT0FBTztFQUNQLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLE1BQU0sRUFBQTs7QUFHVjtFQUNJLGNBQWM7RUFDZCxvQ0FBb0M7RUFDcEMsZUFBZTtFQUNmLE9BQU87RUFDUCxtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLE1BQU0sRUFBQTs7QUFHVjtFQUNJLGFBQWEsRUFBQTs7QUFHakI7RUFFUSxhQUFhLEVBQUE7O0FBRnJCO0VBTVEsY0FBYztFQUNkLHlCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIsNEJBQTRCLEVBQUE7O0FBSXBDO0VBQ0ksbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLGVBQWUsRUFBQTs7QUFHbkI7RUFDSSxXQUFXLEVBQUE7O0FBR2Y7RUFDSSxXQUFXLEVBQUE7O0FBRGY7SUFJUSxrQkFBa0I7SUFDbEIsa0JBQWtCLEVBQUE7O0FBTDFCO01BYWdCLGFBQWE7TUFDYixtQkFBbUIsRUFBQTs7QUFNbkM7RUFDSSxtQkFBbUI7RUFDbkI7QUFBYSxFQUFBOztBQUdqQjtFQUNJLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsUUFBUTtFQUNSLE9BQU87RUFDUCx5QkFBYTtFQUNiLHdDQUFpQixFQUFBOztBQUdyQjtFQUNJLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsbUJBQW1CLEVBQUE7O0FBR3ZCO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsYUFBYTtFQUNiLGNBQWMsRUFBQTs7QUFHbEI7RUFDSSxjQUFjO0VBQ2QsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixpQkFBaUIsRUFBQTs7QUFHckI7RUFDSSxhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixjQUFjLEVBQUE7O0FBR2xCO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1oseUJBQWE7RUFDYixVQUFVO0VBQ1YsdUJBQXVCO0VBQ3ZCLHNCQUFzQjtFQUN0QixxQkFBcUI7RUFDckIsb0JBQW9CO0VBQ3BCLDBCQUEwQixFQUFBOztBQUc5QjtFQUNJLG1DQUF1QjtFQUN2QixnQkFBUTtFQUNSLDBCQUFrQixFQUFBOztBQUd0QjtFQUNJLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFFakIsbURBQW1ELEVBQUE7O0FBR3ZEO0VBQ0ksV0FBVztFQUNYLGlCQUFpQjtFQUNqQix5QkFBeUI7RUFDekIsZUFBZTtFQUNmLG9CQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsU0FBUztFQUNULFlBQVk7RUFDWixtQkFBYztFQUNkLHNCQUFpQjtFQUNqQixxQkFBZ0I7RUFDaEIsbUJBQWM7RUFDZCxxQkFBYTtFQUNiLFdBQVcsRUFBQTs7QUFHZjtFQUNJLGVBQWU7RUFDZixrQkFDSixFQUFBOztBQUVBO0VBQ0ksZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixjQUFjLEVBQUE7O0FBR2xCO0VBQ0ksaUJBQWlCO0VBQ2pCLHlCQUF5QjtFQUN6QixjQUFjO0VBQ2QsNERBQXVCLEVBQUE7O0FBRzNCO0VBQ0ksa0NBQWtDLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wcm9kdWN0LWRldGFpbC9wcm9kdWN0LWRldGFpbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6cm9vdCB7XG4gICAgLS1pb24taXRlbS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogIzA1MWIzNTtcbn1cblxuLmhlYWRlci10aXRsZSB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyXG59XG5cbi5uZXctYmFja2dyb3VuZC1jb2xvciB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjYjMxMTE3O1xufVxuXG4uc2VsZWN0ZWQge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNiMzExMTcgIWltcG9ydGFudDtcbiAgICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbiAgICBvdXRsaW5lOiBub25lO1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG59XG5cbi5zdGFyLXJhdGluZyB7XG4gICAgZm9udC1mYW1pbHk6IFwiRm9udEF3ZXNvbWVcIjtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgaGVpZ2h0OiAyMHB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiA5MHB4O1xufVxuXG4uc3Rhci1yYXRpbmcge1xuICAgIG1hcmdpbjogMHB4IGF1dG87XG59XG5cbi5zdGFyLXJhdGluZzo6YmVmb3JlIHtcbiAgICBjb2xvcjogIzY1NmM3MjtcbiAgICBjb250ZW50OiBcIlxcMjYwNVxcMjYwNVxcMjYwNVxcMjYwNVxcMjYwNVwiO1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBsZWZ0OiAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAycHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbn1cblxuLnN0YXItcmF0aW5nIHNwYW4ge1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIGxlZnQ6IDA7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBwYWRkaW5nLXRvcDogMS41ZW07XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbn1cblxuLnN0YXItcmF0aW5nIHNwYW46OmJlZm9yZSB7XG4gICAgY29sb3I6ICNmYWI5MDI7XG4gICAgY29udGVudDogXCJcXDI2MDVcXDI2MDVcXDI2MDVcXDI2MDVcXDI2MDVcIjtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgbGVmdDogMDtcbiAgICBsZXR0ZXItc3BhY2luZzogMnB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG59XG5cbi5zdGFyLXJhdGluZyAucmF0aW5nIHtcbiAgICBkaXNwbGF5OiBub25lO1xufVxuXG4uY29udGFpbmVyIHtcbiAgICA6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9XG5cbiAgICAuc2l6ZS1zY3JvbGwge1xuICAgICAgICBvdmVyZmxvdzogYXV0bztcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2IzMTExNztcbiAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICAgICAgcGFkZGluZzogMTBweCAxMnB4IDEwcHggMTJweDtcbiAgICB9XG59XG5cbi5zaXplLXNwYW4ge1xuICAgIG1hcmdpbi1ib3R0b206IGF1dG87XG4gICAgbWFyZ2luLWxlZnQ6IDVweDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgYmFja2dyb3VuZDogIzg0MTAxNDtcbiAgICBwYWRkaW5nOiA4cHggMTBweDtcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEycHg7XG59XG5cbi5oaWdobGlnaHQtY29sb3Ige1xuICAgIGNvbG9yOiBibHVlO1xufVxuXG5pb24tc2xpZGVzIHtcbiAgICBjb2xvcjogI2ZmZjtcblxuICAgIGlvbi1zbGlkZSB7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBzdGFydDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICAgICAgIC5zbGlkZS1jb250ZW50IHtcbiAgICAgICAgICAgIC8vIHBhZGRpbmc6IDBweCAxNCU7XG5cblxuXG4gICAgICAgICAgICAudGV4dCBwIHtcbiAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAwO1xuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG5cbmlvbi1jb250ZW50LmhvbWUtY29udGVudCB7XG4gICAgYmFja2dyb3VuZDogI2Y0ZjhmYztcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yLCAjZjRmOGZjKVxufVxuXG5pb24tdG9vbGJhci5wcm9kdWN0LXNpbmdsZS10b29sYmFyIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAyNXB4O1xuICAgIHJpZ2h0OiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAtLWlvbi1jb2xvci1iYXNlOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xufVxuXG4ucHJvZHVjdC1zaW5nbGUtY29udGVudCB7XG4gICAgcGFkZGluZzogMjBweDtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG5cbi5wcm9kdWN0LXNpbmdsZS1jb250ZW50IGgyIHtcbiAgICBjb2xvcjogIzIxMzA1MTtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBtYXJnaW4tdG9wOiAwO1xuICAgIHBhZGRpbmctdG9wOiAwO1xufVxuXG4ucHJvZHVjdC1zaW5nbGUtY29udGVudCBwIHtcbiAgICBjb2xvcjogIzIxMzA1MTtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICBsaW5lLWhlaWdodDogMjRweDtcbn1cblxuLnByb2R1Y3Qtc2luZ2xlLXNpemUge1xuICAgIHBhZGRpbmc6IDIwcHg7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuXG4ucHJvZHVjdC1zaW5nbGUtc2l6ZSBoMiB7XG4gICAgY29sb3I6ICMyMTMwNTE7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgbWFyZ2luLXRvcDogMDtcbiAgICBwYWRkaW5nLXRvcDogMDtcbn1cblxuLmJ0bi1zaXplLWNoYXJ0IHtcbiAgICBjb2xvcjogIzgwODg5YTtcbiAgICBmb250LXNpemU6IDExcHg7XG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICBsaW5lLWhlaWdodDogMjRweDtcbiAgICBiYWNrZ3JvdW5kOiBub25lO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIHBhZGRpbmc6IDA7XG4gICAgLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IDA7XG4gICAgbWFyZ2luLWlubGluZS1zdGFydDogMDtcbiAgICAtd2Via2l0LW1hcmdpbi1lbmQ6IDA7XG4gICAgbWFyZ2luLWlubGluZS1lbmQ6IDA7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG59XG5cbi5idG4tc2l6ZS1jaGFydC5hY3RpdmF0ZWQge1xuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6IHRyYW5zcGFyZW50O1xuICAgIC0tY29sb3I6ICM4MDg4OWE7XG4gICAgLS1jb2xvci1hY3RpdmF0ZWQ6ICM4MDg4OWE7XG59XG5cbmlvbi1mb290ZXIucHJvZHVjdC1zaW5nbGUtZm9vdGVyIHtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIHBhZGRpbmc6IDBweCAyMHB4O1xuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMCAtNHB4IDEycHggMCByZ2JhKDE1OCwgMTY5LCAxODEsIDAuNTUpO1xuICAgIGJveC1zaGFkb3c6IDAgLTRweCAxMnB4IDAgcmdiYSgxNTgsIDE2OSwgMTgxLCAwLjU1KTtcbn1cblxuLmFkZHRvY2FydC1idG4ge1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIC0tYm9yZGVyLXJhZGl1czogMnB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBtYXJnaW46IDA7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIC0tcGFkZGluZy10b3A6IDE1cHg7XG4gICAgLS1wYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDMwcHg7XG4gICAgLS1wYWRkaW5nLWVuZDogMzBweDtcbiAgICAtLWJhY2tncm91bmQ6ICNiMzExMTc7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5zaW5nbGUtcHJvZHVjdC1wcmljZSB7XG4gICAgcGFkZGluZzogMTBweCAwO1xuICAgIHRleHQtYWxpZ246IGNlbnRlclxufVxuXG4uc2luZ2xlLXByb2R1Y3QtcHJpY2UgLnByaWNlIHtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgY29sb3I6ICMyMTMwNTE7XG59XG5cbi5idG4tc2l6ZSB7XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2Q0ZGVlODtcbiAgICBjb2xvcjogIzIxMzA1MTtcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiB2YXIoLS1pb24taXRlbS1iYWNrZ3JvdW5kLWFjdGl2YXRlZCk7XG59XG5cbi5idXR0b24uYWN0aXZlIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBncmVlbiAhaW1wb3J0YW50O1xufVxuXG4iXX0= */"

/***/ }),

/***/ "./src/app/product-detail/product-detail.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/product-detail/product-detail.page.ts ***!
  \*******************************************************/
/*! exports provided: ProductDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailPage", function() { return ProductDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Service_homepage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Service/homepage.service */ "./src/app/Service/homepage.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _sizechart_modal_sizechart_modal_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../sizechart-modal/sizechart-modal.page */ "./src/app/sizechart-modal/sizechart-modal.page.ts");
/* harmony import */ var _Service_cart_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Service/cart.service */ "./src/app/Service/cart.service.ts");
/* harmony import */ var _Service_loader_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Service/loader.service */ "./src/app/Service/loader.service.ts");
/* harmony import */ var _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../Service/aler-service.service */ "./src/app/Service/aler-service.service.ts");










var ProductDetailPage = /** @class */ (function () {
    function ProductDetailPage(alert, loadingService, route, toastController, cartService, homepageService, navCtrl, modalController) {
        var _this = this;
        this.alert = alert;
        this.loadingService = loadingService;
        this.route = route;
        this.toastController = toastController;
        this.cartService = cartService;
        this.homepageService = homepageService;
        this.navCtrl = navCtrl;
        this.modalController = modalController;
        this.bannerSlider = {
            initialSlide: 0,
            speed: 2000,
            slidesPerView: 1,
            autoplay: true,
        };
        this.apiUrl = "https://www.niletechinnovations.com/eq8tor";
        this.sizeArray = [
            {
                "title": "S",
                "isSelected": false
            },
            {
                "title": "M",
                "isSelected": false
            },
            {
                "title": "L",
                "isSelected": false
            },
            {
                "title": "XL",
                "isSelected": false
            },
            {
                "title": "XXL",
                "isSelected": false
            }
        ];
        this.route.queryParams.subscribe(function (params) {
            console.log(params);
            var productName = params.id;
            console.log("product name", productName);
            _this.getProductDetail(productName);
        });
        this.qty = 1;
    }
    ProductDetailPage.prototype.ngOnInit = function () {
    };
    ProductDetailPage.prototype.getProductDetail = function (product) {
        var _this = this;
        this.loadingService.present();
        this.homepageService.productDetail(product).subscribe(function (response) {
            var resp = response.data.single_product_details;
            _this.detailsContent = resp;
            _this.detailImageList = resp._product_related_images_url.product_gallery_images;
            console.log("strHtml detail", _this.detailsContent);
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            _this.alert.myAlertMethod("Network Error", err.error.message, function (data) {
                console.log("hello alert");
            });
            _this.loadingService.dismiss();
        });
    };
    ProductDetailPage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    ProductDetailPage.prototype.getCartContent = function () {
        console.log("getCartContent");
        this.homepageService.getCartProducts().subscribe(function (resp) {
            console.log("getCartContent", JSON.stringify(resp));
        }, function (err) {
            console.log("getCartContent error", err);
        });
    };
    ProductDetailPage.prototype.openSizeChart = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _sizechart_modal_sizechart_modal_page__WEBPACK_IMPORTED_MODULE_5__["SizechartModalPage"],
                            componentProps: {
                                "testParam": 123,
                                "testurl": "https://www.google.com"
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (sizeChartResponse) {
                            if (sizeChartResponse !== null) {
                                _this.sizeChartResponse = sizeChartResponse.data;
                            }
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ProductDetailPage.prototype.btnActivate = function (ind) {
        for (var i = 0; i < this.sizeArray.length; i++) {
            this.sizeArray[i].isSelected = false;
        }
        this.sizeArray[ind].isSelected = true;
    };
    ProductDetailPage.prototype.incrementQty = function () {
        console.log(this.qty + 1);
        this.qty += 1;
    };
    ProductDetailPage.prototype.decrementQty = function () {
        if (this.qty - 1 < 1) {
            this.qty = 1;
            console.log('1->' + this.qty);
        }
        else {
            this.qty -= 1;
            console.log('2->' + this.qty);
        }
    };
    ProductDetailPage.prototype.addToCart = function (productID, Quantity) {
        var _this = this;
        if (productID) {
            this.homepageService.addToCart(productID, Quantity).subscribe(function (resp) {
                console.log("response add to cart", resp);
                _this.presentToast();
            }, function (err) {
                console.log("error add to cart", err);
            });
            // this.cartService.addProduct(product);
            // this.presentToast();
            // console.log(product);
            this.cartAdded = true;
        }
    };
    ProductDetailPage.prototype.presentToast = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Product Added to cart.',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonSlides"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonSlides"])
    ], ProductDetailPage.prototype, "slides", void 0);
    ProductDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-product-detail',
            template: __webpack_require__(/*! ./product-detail.page.html */ "./src/app/product-detail/product-detail.page.html"),
            styles: [__webpack_require__(/*! ./product-detail.page.scss */ "./src/app/product-detail/product-detail.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_Service_aler_service_service__WEBPACK_IMPORTED_MODULE_8__["AlerServiceService"], _Service_loader_service__WEBPACK_IMPORTED_MODULE_7__["LoaderService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"], _Service_cart_service__WEBPACK_IMPORTED_MODULE_6__["CartService"], _Service_homepage_service__WEBPACK_IMPORTED_MODULE_3__["HomepageService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]])
    ], ProductDetailPage);
    return ProductDetailPage;
}());



/***/ })

}]);
//# sourceMappingURL=product-detail-product-detail-module.js.map