(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["order-details-order-details-module"],{

/***/ "./src/app/Service/homepage.service.ts":
/*!*********************************************!*\
  !*** ./src/app/Service/homepage.service.ts ***!
  \*********************************************/
/*! exports provided: HomepageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomepageService", function() { return HomepageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");




var HomepageService = /** @class */ (function () {
    function HomepageService(http) {
        this.http = http;
        this.apiUrl = 'https://www.niletechinnovations.com/eq8tor/api/';
    }
    HomepageService.prototype.showCategories = function () {
        var _this = this;
        return this.http.post(this.apiUrl + 'categories', '')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('categories'); }));
    };
    HomepageService.prototype.showSubCategory = function (id) {
        var _this = this;
        return this.http.post(this.apiUrl + 'categories?' + 'parent_id=' + id, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('subcategories'); }));
    };
    HomepageService.prototype.log = function (message) {
        console.log(message);
    };
    HomepageService.prototype.getHomePageData = function () {
        return this.http.get('https://www.niletechinnovations.com/eq8tor/api/home').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(200));
    };
    HomepageService.prototype.getCategoryProducts = function (name) {
        var _this = this;
        return this.http.post(this.apiUrl + 'cat-products?' + 'slug=' + name, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('subcategories'); }));
    };
    HomepageService.prototype.productDetail = function (product) {
        var _this = this;
        return this.http.post(this.apiUrl + 'products/detail?' + 'slug=' + product, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('Detail'); }));
    };
    HomepageService.prototype.searchApi = function (textSearch) {
        var _this = this;
        return this.http.post(this.apiUrl + 'search?' + 'srch_term=' + textSearch, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('search api call'); }));
    };
    HomepageService.prototype.loadMoreData = function (textSearch) {
        var _this = this;
        return this.http.post(textSearch, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('search more api call'); }));
    };
    HomepageService.prototype.addToWishlist = function (textSearch) {
        var _this = this;
        return this.http.post(this.apiUrl + 'products/add/wishlist?' + 'product_id=' + textSearch, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log('search api call'); }));
    };
    HomepageService.prototype.getWishlistProducts = function () {
        return this.http.get(this.apiUrl + 'user/wishlist-content').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(100));
    };
    HomepageService.prototype.getAllOrders = function () {
        return this.http.get(this.apiUrl + 'user/orders-content').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(100));
    };
    HomepageService.prototype.removeFromWishList = function (productId) {
        return this.http.post(this.apiUrl + 'product/remove/wishlist?' + 'product_id=' + productId, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(100));
    };
    HomepageService.prototype.getCartProducts = function () {
        return this.http.get(this.apiUrl + 'cart/products').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(10));
    };
    HomepageService.prototype.addToCart = function (product_id, qty) {
        return this.http.post(this.apiUrl + 'product/addtocart?' + 'product_id=' + product_id + '&qty=' + qty, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(100));
    };
    HomepageService.prototype.viewOrderDetails = function (processId, orderid) {
        return this.http.get(this.apiUrl + 'user/order/details?' + 'order_process_id=' + processId + '&order_id=' + orderid);
    };
    HomepageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], HomepageService);
    return HomepageService;
}());



/***/ }),

/***/ "./src/app/Service/loader.service.ts":
/*!*******************************************!*\
  !*** ./src/app/Service/loader.service.ts ***!
  \*******************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var LoaderService = /** @class */ (function () {
    function LoaderService(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
        this.isLoading = false;
    }
    LoaderService.prototype.present = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isLoading = true;
                        return [4 /*yield*/, this.loadingCtrl.create({ duration: 5000, }).then(function (a) {
                                a.present().then(function () {
                                    if (!_this.isLoading) {
                                        a.dismiss().then(function () { return console.log('abort presenting'); });
                                    }
                                });
                            })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    LoaderService.prototype.dismiss = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isLoading = false;
                        return [4 /*yield*/, this.loadingCtrl.dismiss().then(function () { return console.log('dismissed'); })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    LoaderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])
    ], LoaderService);
    return LoaderService;
}());



/***/ }),

/***/ "./src/app/order-details/order-details.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/order-details/order-details.module.ts ***!
  \*******************************************************/
/*! exports provided: OrderDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderDetailsPageModule", function() { return OrderDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _order_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./order-details.page */ "./src/app/order-details/order-details.page.ts");







var routes = [
    {
        path: '',
        component: _order_details_page__WEBPACK_IMPORTED_MODULE_6__["OrderDetailsPage"]
    }
];
var OrderDetailsPageModule = /** @class */ (function () {
    function OrderDetailsPageModule() {
    }
    OrderDetailsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_order_details_page__WEBPACK_IMPORTED_MODULE_6__["OrderDetailsPage"]]
        })
    ], OrderDetailsPageModule);
    return OrderDetailsPageModule;
}());



/***/ })

}]);
//# sourceMappingURL=order-details-order-details-module.js.map