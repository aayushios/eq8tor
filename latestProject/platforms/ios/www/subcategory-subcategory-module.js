(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["subcategory-subcategory-module"],{

/***/ "./src/app/subcategory/subcategory.module.ts":
/*!***************************************************!*\
  !*** ./src/app/subcategory/subcategory.module.ts ***!
  \***************************************************/
/*! exports provided: SubcategoryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubcategoryPageModule", function() { return SubcategoryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _subcategory_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./subcategory.page */ "./src/app/subcategory/subcategory.page.ts");







var routes = [
    {
        path: '',
        component: _subcategory_page__WEBPACK_IMPORTED_MODULE_6__["SubcategoryPage"]
    }
];
var SubcategoryPageModule = /** @class */ (function () {
    function SubcategoryPageModule() {
    }
    SubcategoryPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_subcategory_page__WEBPACK_IMPORTED_MODULE_6__["SubcategoryPage"]]
        })
    ], SubcategoryPageModule);
    return SubcategoryPageModule;
}());



/***/ }),

/***/ "./src/app/subcategory/subcategory.page.html":
/*!***************************************************!*\
  !*** ./src/app/subcategory/subcategory.page.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"new-background-color\">\n    <ion-title class=\"header-title\">{{headerTitle}}</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-button color=\"dark\" (click)=\"goBack()\">\n        <ion-icon class=\"back-arrow\" name=\"arrow-back\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"home-content\">\n    <div *ngIf=\"subCategoryArray && subCategoryArray.length \" class=\"container\">\n        <div class=\"category-scroll\" scrollX=\"true\">\n          <span class=\"category-span\"\n            *ngFor=\"let subCategory of subCategoryArray\" (click)=\"filterProduct(subCategory.slug)\">{{subCategory.name}}</span>\n        </div>\n      </div>\n    <ion-grid>\n         <ion-row>\n           <ion-col size=\"6\" *ngFor=\"let products of productsArray\">\n                <ion-card class=\"product-card\">\n                  <div *ngIf=\"products.post_image_url === ''; else placeholder\">\n                    <ion-card-content class=\"product-card-media\">\n                      <img (click)=\"goToProductDetail(products.post_slug)\"  src=\"../../assets/imgs/icon.png\">\n                    </ion-card-content>\n                  </div>\n                  <ng-template #placeholder>\n                    <ion-card-content class=\"product-card-media\" >\n                        <img (click)=\"goToProductDetail(products.post_slug)\"  src=\"https://www.niletechinnovations.com/eq8tor{{products.post_image_url}}\">\n                    </ion-card-content>\n                  </ng-template>\n                </ion-card>\n                <ion-card-content class=\"product-content\">\n                    <ion-card-title class=\"product-title\">\n                        {{products.post_title}}\n                    </ion-card-title>\n                    <div class=\"star-rating\">\n                      <span style=\"width:50%\"></span>\n                    </div>\n                    <div class=\"product-price\">\n                      <span>${{products.post_price}}</span>\n                    </div>\n                </ion-card-content>\n           </ion-col>\n         </ion-row>\n     </ion-grid>\n   </ion-content>\n"

/***/ }),

/***/ "./src/app/subcategory/subcategory.page.scss":
/*!***************************************************!*\
  !*** ./src/app/subcategory/subcategory.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container ::-webkit-scrollbar {\n  display: none; }\n\n.container .category-scroll {\n  overflow: auto;\n  background-color: #b31117;\n  white-space: nowrap;\n  padding: 10px 12px 10px 12px; }\n\n.category-span {\n  margin-bottom: auto;\n  margin-left: 5px;\n  color: white;\n  background: #841014;\n  padding: 8px 10px;\n  border-radius: 2px;\n  font-weight: 600;\n  font-size: 12px; }\n\n.new-background-color {\n  --background: #b31117; }\n\n.header-title {\n  color: #fff;\n  text-align: center; }\n\n.back-arrow {\n  color: #fff; }\n\nion-content.home-content {\n  background: #f4f8fc;\n  --background: var(--ion-background-color,#f4f8fc)\n; }\n\nion-card.product-card {\n  -webkit-margin-start: 0;\n  margin-inline-start: 0px;\n  background: #fff;\n  border-radius: 3px;\n  --background: var(--ion-item-background,#fff);\n  box-shadow: none;\n  margin-bottom: 0;\n  min-height: 200px;\n  -webkit-margin-end: 10px;\n  margin-inline-end: 10px;\n  margin-bottom: 10px;\n  margin-top: 0px; }\n\n.product-card-media {\n  -webkit-padding-start: 0px;\n  padding-inline-start: 0px;\n  -webkit-padding-end: 0px;\n  padding-inline-end: 0px;\n  padding-top: 0;\n  padding-bottom: 0;\n  height: 200px;\n  overflow: hidden; }\n\n.product-card-media img {\n  width: 100% !important;\n  max-height: none !important;\n  max-width: none !important; }\n\n.product-title {\n  color: #656c72;\n  font-size: 14px;\n  font-weight: 500;\n  line-height: 24px; }\n\n.product-content {\n  padding: 0;\n  margin-bottom: 20px; }\n\n.Products-slider {\n  height: auto;\n  /* slider height you want */ }\n\n.star-rating {\n  font-family: \"FontAwesome\";\n  font-size: 18px;\n  height: 20px;\n  overflow: hidden;\n  position: relative;\n  width: 78px; }\n\n.star-rating {\n  margin: 10px auto; }\n\n.star-rating::before {\n  color: #656c72;\n  content: \"\\2605\\2605\\2605\\2605\\2605\";\n  float: left;\n  font-size: 15px;\n  left: 0;\n  letter-spacing: 1px;\n  position: absolute;\n  top: 0; }\n\n.star-rating span {\n  float: left;\n  left: 0;\n  overflow: hidden;\n  padding-top: 1.5em;\n  position: absolute;\n  top: 0; }\n\n.star-rating span::before {\n  color: #fab902;\n  content: \"\\2605\\2605\\2605\\2605\\2605\";\n  font-size: 15px;\n  left: 0;\n  letter-spacing: 1px;\n  position: absolute;\n  top: 0; }\n\n.star-rating .rating {\n  display: none; }\n\n.product-price {\n  text-align: center; }\n\n.product-price span {\n  color: #656c72;\n  font-size: 16px;\n  font-weight: 600; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL3N1YmNhdGVnb3J5L3N1YmNhdGVnb3J5LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLGFBQWEsRUFBQTs7QUFGakI7RUFNSSxjQUFjO0VBQ2QseUJBQXlCO0VBQ3pCLG1CQUFtQjtFQUNuQiw0QkFBNEIsRUFBQTs7QUFJNUI7RUFDQSxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsZUFBZSxFQUFBOztBQUVmO0VBQ0EscUJBQWEsRUFBQTs7QUFFakI7RUFBYyxXQUFVO0VBQUUsa0JBQWtCLEVBQUE7O0FBQzVDO0VBQVksV0FBVSxFQUFBOztBQUN0QjtFQUNBLG1CQUFtQjtFQUNuQjtBQUFhLEVBQUE7O0FBRWI7RUFDQSx1QkFBdUI7RUFDdkIsd0JBQXdCO0VBQ3hCLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsNkNBQWE7RUFDYixnQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQix3QkFBd0I7RUFDeEIsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixlQUFjLEVBQUE7O0FBRWQ7RUFBcUIsMEJBQTBCO0VBQy9DLHlCQUF5QjtFQUN6Qix3QkFBd0I7RUFDeEIsdUJBQXVCO0VBQUUsY0FBYztFQUN2QyxpQkFBaUI7RUFBSSxhQUFhO0VBQ2xDLGdCQUFnQixFQUFBOztBQUNoQjtFQUNBLHNCQUFzQjtFQUN0QiwyQkFBMkI7RUFDM0IsMEJBQTBCLEVBQUE7O0FBQzFCO0VBQW1CLGNBQWM7RUFDakMsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixpQkFBaUIsRUFBQTs7QUFDakI7RUFBaUIsVUFBVTtFQUFLLG1CQUFtQixFQUFBOztBQUVuRDtFQUNBLFlBQVk7RUFBRSwyQkFBQSxFQUE0Qjs7QUFFMUM7RUFDQSwwQkFBMEI7RUFDMUIsZUFBZTtFQUNmLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFdBQVcsRUFBQTs7QUFHWDtFQUNBLGlCQUFpQixFQUFBOztBQUdqQjtFQUNBLGNBQWM7RUFDZCxvQ0FBb0M7RUFDcEMsV0FBVztFQUNYLGVBQWM7RUFDZCxPQUFPO0VBQ1AsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixNQUFNLEVBQUE7O0FBR047RUFDQSxXQUFXO0VBQ1gsT0FBTztFQUNQLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLE1BQU0sRUFBQTs7QUFFTjtFQUNBLGNBQWE7RUFDYixvQ0FBb0M7RUFDcEMsZUFBZTtFQUNmLE9BQU87RUFDUCxtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLE1BQU0sRUFBQTs7QUFHTjtFQUNBLGFBQWEsRUFBQTs7QUFFYjtFQUFlLGtCQUFrQixFQUFBOztBQUNqQztFQUF3QixjQUFjO0VBQ3RDLGVBQWU7RUFDZixnQkFBZ0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3N1YmNhdGVnb3J5L3N1YmNhdGVnb3J5LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXIge1xuICAgIDo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxuICAgIFxuICAgIC5jYXRlZ29yeS1zY3JvbGwge1xuICAgIG92ZXJmbG93OiBhdXRvO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNiMzExMTc7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICBwYWRkaW5nOiAxMHB4IDEycHggMTBweCAxMnB4O1xuICAgIH1cbiAgICB9XG4gICAgXG4gICAgLmNhdGVnb3J5LXNwYW4ge1xuICAgIG1hcmdpbi1ib3R0b206IGF1dG87XG4gICAgbWFyZ2luLWxlZnQ6IDVweDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgYmFja2dyb3VuZDogIzg0MTAxNDtcbiAgICBwYWRkaW5nOiA4cHggMTBweDtcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgfVxuICAgIC5uZXctYmFja2dyb3VuZC1jb2xvciB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjYjMxMTE3O1xuICAgIH1cbi5oZWFkZXItdGl0bGV7Y29sb3I6I2ZmZjsgdGV4dC1hbGlnbjogY2VudGVyfVxuLmJhY2stYXJyb3d7Y29sb3I6I2ZmZjt9XG5pb24tY29udGVudC5ob21lLWNvbnRlbnQge1xuYmFja2dyb3VuZDogI2Y0ZjhmYztcbi0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IsI2Y0ZjhmYylcbn1cbmlvbi1jYXJkLnByb2R1Y3QtY2FyZCB7XG4td2Via2l0LW1hcmdpbi1zdGFydDogMDtcbm1hcmdpbi1pbmxpbmUtc3RhcnQ6IDBweDtcbmJhY2tncm91bmQ6ICNmZmY7XG5ib3JkZXItcmFkaXVzOiAzcHg7XG4tLWJhY2tncm91bmQ6IHZhcigtLWlvbi1pdGVtLWJhY2tncm91bmQsI2ZmZik7XG5ib3gtc2hhZG93OiBub25lO1xubWFyZ2luLWJvdHRvbTogMDtcbm1pbi1oZWlnaHQ6IDIwMHB4O1xuLXdlYmtpdC1tYXJnaW4tZW5kOiAxMHB4O1xubWFyZ2luLWlubGluZS1lbmQ6IDEwcHg7XG5tYXJnaW4tYm90dG9tOiAxMHB4O1xubWFyZ2luLXRvcDowcHg7XG59XG4ucHJvZHVjdC1jYXJkLW1lZGlheyAtd2Via2l0LXBhZGRpbmctc3RhcnQ6IDBweDtcbnBhZGRpbmctaW5saW5lLXN0YXJ0OiAwcHg7XG4td2Via2l0LXBhZGRpbmctZW5kOiAwcHg7XG5wYWRkaW5nLWlubGluZS1lbmQ6IDBweDsgcGFkZGluZy10b3A6IDA7XG5wYWRkaW5nLWJvdHRvbTogMDsgICBoZWlnaHQ6IDIwMHB4O1xub3ZlcmZsb3c6IGhpZGRlbjt9XG4ucHJvZHVjdC1jYXJkLW1lZGlhIGltZ3tcbndpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG5tYXgtaGVpZ2h0OiBub25lICFpbXBvcnRhbnQ7XG5tYXgtd2lkdGg6IG5vbmUgIWltcG9ydGFudDsgICB9XG4ucHJvZHVjdC10aXRsZXsgICAgY29sb3I6ICM2NTZjNzI7XG5mb250LXNpemU6IDE0cHg7XG5mb250LXdlaWdodDogNTAwO1xubGluZS1oZWlnaHQ6IDI0cHg7fVxuLnByb2R1Y3QtY29udGVudHtwYWRkaW5nOiAwOyAgICBtYXJnaW4tYm90dG9tOiAyMHB4O31cblxuLlByb2R1Y3RzLXNsaWRlciB7XG5oZWlnaHQ6IGF1dG87IC8qIHNsaWRlciBoZWlnaHQgeW91IHdhbnQgKi9cbn1cbi5zdGFyLXJhdGluZ3tcbmZvbnQtZmFtaWx5OiBcIkZvbnRBd2Vzb21lXCI7XG5mb250LXNpemU6IDE4cHg7XG5oZWlnaHQ6IDIwcHg7XG5vdmVyZmxvdzogaGlkZGVuO1xucG9zaXRpb246IHJlbGF0aXZlO1xud2lkdGg6IDc4cHg7XG59XG5cbi5zdGFyLXJhdGluZ3tcbm1hcmdpbjogMTBweCBhdXRvOyAgICBcbn1cblxuLnN0YXItcmF0aW5nOjpiZWZvcmV7XG5jb2xvcjogIzY1NmM3MjtcbmNvbnRlbnQ6IFwiXFwyNjA1XFwyNjA1XFwyNjA1XFwyNjA1XFwyNjA1XCI7XG5mbG9hdDogbGVmdDtcbmZvbnQtc2l6ZToxNXB4O1xubGVmdDogMDtcbmxldHRlci1zcGFjaW5nOiAxcHg7XG5wb3NpdGlvbjogYWJzb2x1dGU7XG50b3A6IDA7XG59XG5cbi5zdGFyLXJhdGluZyBzcGFue1xuZmxvYXQ6IGxlZnQ7XG5sZWZ0OiAwO1xub3ZlcmZsb3c6IGhpZGRlbjtcbnBhZGRpbmctdG9wOiAxLjVlbTtcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcbnRvcDogMDtcbn1cbi5zdGFyLXJhdGluZyBzcGFuOjpiZWZvcmV7XG5jb2xvcjojZmFiOTAyO1xuY29udGVudDogXCJcXDI2MDVcXDI2MDVcXDI2MDVcXDI2MDVcXDI2MDVcIjtcbmZvbnQtc2l6ZTogMTVweDtcbmxlZnQ6IDA7XG5sZXR0ZXItc3BhY2luZzogMXB4O1xucG9zaXRpb246IGFic29sdXRlO1xudG9wOiAwO1xufVxuXG4uc3Rhci1yYXRpbmcgLnJhdGluZ3tcbmRpc3BsYXk6IG5vbmU7XG59XG4ucHJvZHVjdC1wcmljZXt0ZXh0LWFsaWduOiBjZW50ZXJ9XG4ucHJvZHVjdC1wcmljZSBzcGFueyAgICBjb2xvcjogIzY1NmM3MjtcbmZvbnQtc2l6ZTogMTZweDtcbmZvbnQtd2VpZ2h0OiA2MDA7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/subcategory/subcategory.page.ts":
/*!*************************************************!*\
  !*** ./src/app/subcategory/subcategory.page.ts ***!
  \*************************************************/
/*! exports provided: SubcategoryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubcategoryPage", function() { return SubcategoryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _Service_homepage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Service/homepage.service */ "./src/app/Service/homepage.service.ts");
/* harmony import */ var _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Service/aler-service.service */ "./src/app/Service/aler-service.service.ts");
/* harmony import */ var _Service_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Service/loader.service */ "./src/app/Service/loader.service.ts");







var SubcategoryPage = /** @class */ (function () {
    function SubcategoryPage(route, loadingService, homepageService, router, alert, navCtrl) {
        this.route = route;
        this.loadingService = loadingService;
        this.homepageService = homepageService;
        this.router = router;
        this.alert = alert;
        this.navCtrl = navCtrl;
        // console.log("home page data",HomePageData);
    }
    SubcategoryPage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    SubcategoryPage.prototype.getSubCategory = function (catID) {
        var _this = this;
        this.loadingService.present();
        this.homepageService.getCategoryProducts(catID).subscribe(function (response) {
            var resp = response.data;
            _this.subCategoryArray = resp.categories;
            console.log("this.subCategoryArray", _this.subCategoryArray);
            _this.productsArray = resp.products;
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            _this.alert.myAlertMethod(err.message, "Please try again later.", function (data) {
                console.log("hello getSubCategory");
            });
            _this.loadingService.dismiss();
        });
    };
    SubcategoryPage.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            console.log(params.id);
            _this.headerTitle = params.title;
            _this.getSubCategory(params.id);
        });
    };
    SubcategoryPage.prototype.showProducts = function (itemName) {
        console.log("showProducts", itemName);
        this.router.navigate(['productlisting'], { queryParams: { 'id': itemName } });
    };
    SubcategoryPage.prototype.goToProductDetail = function (product) {
        this.router.navigate(['product-detail'], { queryParams: { 'id': product } });
        console.log("response product", product);
    };
    SubcategoryPage.prototype.filterProduct = function (product) {
        console.log(product);
        this.getSubCategoryFilter(product);
    };
    SubcategoryPage.prototype.getSubCategoryFilter = function (catID) {
        var _this = this;
        this.loadingService.present();
        this.homepageService.getCategoryProducts(catID).subscribe(function (response) {
            var resp = response.data;
            _this.productsArray = resp.products;
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            _this.alert.myAlertMethod(err.message, "Please try again later.", function (data) {
                console.log("hello getSubCategory");
            });
            _this.loadingService.dismiss();
        });
    };
    SubcategoryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-subcategory',
            template: __webpack_require__(/*! ./subcategory.page.html */ "./src/app/subcategory/subcategory.page.html"),
            styles: [__webpack_require__(/*! ./subcategory.page.scss */ "./src/app/subcategory/subcategory.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _Service_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"], _Service_homepage_service__WEBPACK_IMPORTED_MODULE_4__["HomepageService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_5__["AlerServiceService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]])
    ], SubcategoryPage);
    return SubcategoryPage;
}());



/***/ })

}]);
//# sourceMappingURL=subcategory-subcategory-module.js.map