(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cart-cart-module"],{

/***/ "./src/app/Service/cart.service.ts":
/*!*****************************************!*\
  !*** ./src/app/Service/cart.service.ts ***!
  \*****************************************/
/*! exports provided: CartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartService", function() { return CartService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CartService = /** @class */ (function () {
    function CartService() {
        this.data = [
            {
                category: "Pizza",
                expanded: false,
                products: [
                    { id: 0, name: 'Salami', price: '8' },
                    { id: 1, name: 'Classic', price: '5' },
                    { id: 2, name: 'Tuna', price: '9' },
                    { id: 3, name: 'Hawai', price: '7' }
                ]
            },
            {
                category: 'Pasta',
                products: [
                    { id: 4, name: 'Mac & Cheese', price: '8' },
                    { id: 5, name: 'Bologenese', price: '6' }
                ]
            },
            {
                category: 'Salad',
                products: [
                    { id: 6, name: 'Ham & Egg', price: '8' },
                    { id: 7, name: 'Basic', price: '5' },
                    { id: 8, name: 'Ceaser', price: '9' }
                ]
            }
        ];
        this.cart = [];
    }
    CartService.prototype.getProducts = function () {
        return this.data;
    };
    CartService.prototype.getCart = function () {
        return this.cart;
    };
    CartService.prototype.addProduct = function (product) {
        this.cart.push(product);
    };
    CartService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CartService);
    return CartService;
}());



/***/ }),

/***/ "./src/app/cart/cart.module.ts":
/*!*************************************!*\
  !*** ./src/app/cart/cart.module.ts ***!
  \*************************************/
/*! exports provided: CartPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartPageModule", function() { return CartPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _cart_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./cart.page */ "./src/app/cart/cart.page.ts");







var routes = [
    {
        path: '',
        component: _cart_page__WEBPACK_IMPORTED_MODULE_6__["CartPage"]
    }
];
var CartPageModule = /** @class */ (function () {
    function CartPageModule() {
    }
    CartPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_cart_page__WEBPACK_IMPORTED_MODULE_6__["CartPage"]]
        })
    ], CartPageModule);
    return CartPageModule;
}());



/***/ }),

/***/ "./src/app/cart/cart.page.html":
/*!*************************************!*\
  !*** ./src/app/cart/cart.page.html ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"new-background-color\">\n    <ion-title class=\"header-title\">Cart</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-button color=\"dark\" (click)=\"goBack()\">\n        <ion-icon class=\"back-arrow\" name=\"arrow-back\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"home-content\">\n  <ion-list class=\"cart-item-content\">\n    <ion-item class=\"cart-item-info\" *ngFor=\"let item of selectedItems\" lines=\"inset\">\n      <ion-row class=\"cart-item-grid\">\n        <ion-col size=\"3\">\n          <div class=\"cart-item-info-media\">\n            <img src=\"{{imgUrl}}{{item.post_image_url}}\">\n          </div>\n          \n        </ion-col>\n        <ion-col size=\"8\">\n          <div class=\"cart-item-info-content\">\n            <h2>{{ item.post_title }}</h2>\n            <h4>{{ item.count }}x</h4>\n            <div class=\"item-price\">\n              <!-- {{ item.post_price | currency:'USD':'symbol' }} -->\n              <span slot=\"end\" text-right>{{ (item.post_price * item.count) | currency:'USD':'symbol' }}</span>\n            </div>\n          </div>\n        </ion-col>\n        <ion-col size=\"1\">\n          <ion-button class=\"close-icon\" (click)=\"removeItem(item)\">\n            <ion-icon name=\"close\"></ion-icon>\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n    <div class=\"btn-content-info\">\n      <button routerLink=\"/home\" class=\"btn-add-more\">+ ADD MORE ITEMS</button>\n    </div>\n    <!-- <ion-item>\n      Total: <span slot=\"end\">{{ total | currency:'USD':'symbol' }}</span>\n    </ion-item> -->\n  </ion-list>\n\n  <div class=\"apply-coupon-section\">\n    <ion-row class=\"apply-coupon-content\">\n      <ion-col size=\"9\">\n        <div class=\"apply-coupon-box\">\n          <img src=\"../../assets/imgs/discount.svg\" height=\"40\">\n          <button class=\"apply-coupon-btn\" ion-button (click)=\"hideMe = !hideMe\">Apply Coupon</button>\n        </div>\n      </ion-col>\n      <ion-col size=\"3\">\n        <div class=\"offer-btn-box\">\n          <button class=\"offer-btn\">Offers</button>\n        </div>\n      </ion-col>\n      <div *ngIf=\"hideMe\" class=\"apply-coupon-form\">\n        <ion-item class=\"password-input\">\n          <ion-input class=\"input-form-control\" (keydown.space)=\"$event.preventDefault();\" [type]=\"text\"\n            placeholder=\"Apply Coupon\">\n          </ion-input>\n          <button class=\"showBtn Apply-btn\" [name]=\"Apply\" item-right (click)='hideShowPassword()'>Apply</button>\n        </ion-item>\n      </div>\n    </ion-row>\n  </div>\n\n <div class=\"cart-total-section\">\n   <h2>Bill Details</h2>    \n    <ion-row>\n      <ion-col size=\"6\" >\n        <div class=\"cart-Subtotal-text\">Cart Sub Total:</div>\n      </ion-col>\n      <ion-col size=\"6\">\n        <div class=\"cart-Subtotal-value\">{{ total | currency:'USD':'symbol' }}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\">\n          <div class=\"cart-discount-text\">Offer & Discount:</div>\n      </ion-col>\n      <ion-col size=\"6\">\n        <div class=\"cart-discount-value\">{{ total | currency:'USD':'symbol' }}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\">\n          <div class=\"cart-tax-text\">Tax:</div>\n      </ion-col>\n      <ion-col size=\"6\">\n          <div class=\"cart-tax-value\">{{ total | currency:'USD':'symbol' }}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\">\n          <div class=\"cart-shipping-text\">Shipping Cost:</div>\n      </ion-col>\n      <ion-col size=\"6\">\n          <div class=\"cart-shipping-value\">{{ total | currency:'USD':'symbol' }}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"grand-total\">\n      <ion-col size=\"6\">\n          <div class=\"cart-total-text\">Grand Total:</div>\n      </ion-col>\n      <ion-col size=\"6\">\n          <div class=\"cart-total-value\">{{ total | currency:'USD':'symbol' }}</div>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>\n\n<ion-footer class=\"product-single-footer\">\n  <ion-button class=\"checkout-btn\">\n    CHECKOUT\n  </ion-button>\n</ion-footer>"

/***/ }),

/***/ "./src/app/cart/cart.page.scss":
/*!*************************************!*\
  !*** ./src/app/cart/cart.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .new-background-color {\n  --background: #b31117; }\n\n:host .header-title {\n  color: #fff;\n  text-align: center; }\n\n.back-arrow {\n  color: #fff; }\n\nion-content.home-content {\n  background: #f4f8fc;\n  --background: var(--ion-background-color,#f4f8fc)\n; }\n\n.cart-item-content {\n  padding: 20px;\n  background: #fff;\n  margin-bottom: 10px; }\n\n.cart-item-info {\n  border-bottom: 1px solid #eee;\n  --padding-start:0px;\n  --inner-padding-end: 0px; }\n\n.cart-item-info-content h2 {\n  color: #213051;\n  font-size: 14px;\n  font-weight: 600;\n  margin-top: 0;\n  padding-top: 0; }\n\n.cart-item-info-content h4 {\n  color: #a0a7b5;\n  font-size: 11px;\n  font-weight: 500;\n  margin: 0;\n  padding: 0; }\n\n.item-price {\n  padding: 10px 0;\n  text-align: left; }\n\n.item-price span {\n  font-size: 16px;\n  font-weight: bold;\n  color: #213051; }\n\n.btn-add-more {\n  background: #b31117;\n  padding: 10px 16px;\n  color: #fff;\n  text-align: center;\n  display: inline-block;\n  border-radius: 2px;\n  font-size: 13px; }\n\n.btn-content-info {\n  padding: 10px 0;\n  text-align: center; }\n\n.close-icon {\n  background: none;\n  --background: transparent;\n  --background-focused: transparent;\n  color: #b31117;\n  padding: 0;\n  -webkit-margin-start: 0;\n  margin-inline-start: 0;\n  -webkit-margin-end: 0;\n  margin-inline-end: 0;\n  --padding-start: 0;\n  --padding-end: 0; }\n\n.apply-coupon-section {\n  padding: 20px;\n  background: #fff;\n  margin-bottom: 10px; }\n\n.apply-coupon-box {\n  display: flex;\n  align-items: center;\n  height: 100%; }\n\n.offer-btn-box {\n  text-align: right;\n  padding: 10px 0; }\n\n.offer-btn {\n  background: none;\n  --background: transparent;\n  --background-focused: transparent;\n  color: #b31117;\n  padding: 0;\n  outline: none; }\n\n.checkout-btn {\n  color: #fff;\n  font-weight: bold;\n  text-transform: uppercase;\n  font-size: 14px;\n  --border-radius: 2px;\n  display: inline-block;\n  margin: 0;\n  height: auto;\n  --padding-top: 15px;\n  --padding-bottom: 15px;\n  --padding-start: 30px;\n  --padding-end: 30px;\n  --background: #b31117;\n  width: 100%; }\n\nion-footer.product-single-footer {\n  background: #fff;\n  padding: 10px 20px;\n  box-shadow: 0 -4px 12px 0 rgba(158, 169, 181, 0.55); }\n\n.apply-coupon-btn {\n  background: none;\n  --background: transparent;\n  --background-focused: transparent;\n  color: #213051;\n  padding: 0 0 0 10px;\n  font-weight: 600;\n  font-size: 14px;\n  outline: none; }\n\n.apply-coupon-form {\n  width: 100%; }\n\n.password-input {\n  padding: 0;\n  -webkit-margin-start: 0;\n  margin-inline-start: 0;\n  -webkit-margin-end: 0;\n  margin-inline-end: 0;\n  --padding-start: 0;\n  --padding-end: 0; }\n\n.Apply-btn {\n  background: #b31117;\n  padding: 10px 16px;\n  color: #fff;\n  text-align: center;\n  display: inline-block;\n  border-radius: 2px;\n  font-size: 13px;\n  outline: none; }\n\n.input-form-control {\n  font-size: 12px; }\n\n.cart-total-section {\n  padding: 20px;\n  background: #fff;\n  margin-bottom: 0px; }\n\n.cart-total-section h2 {\n  color: #b31117;\n  font-size: 14px;\n  font-weight: 600;\n  margin-top: 0;\n  padding-top: 0; }\n\n.cart-Subtotal-text {\n  color: #213051;\n  font-size: 13px;\n  font-weight: 500; }\n\n.cart-Subtotal-value {\n  color: #213051;\n  font-size: 13px;\n  font-weight: 500;\n  text-align: right; }\n\n.cart-discount-text {\n  color: #ff9800;\n  font-size: 11px;\n  font-weight: 500; }\n\n.cart-discount-value {\n  color: #ff9800;\n  font-size: 11px;\n  font-weight: 500;\n  text-align: right; }\n\n.cart-tax-text {\n  color: #a0a7b5;\n  font-size: 11px;\n  font-weight: 500; }\n\n.cart-tax-value {\n  color: #a0a7b5;\n  font-size: 11px;\n  font-weight: 500;\n  text-align: right; }\n\n.cart-shipping-text {\n  color: #a0a7b5;\n  font-size: 11px;\n  font-weight: 500; }\n\n.cart-shipping-value {\n  color: #a0a7b5;\n  font-size: 11px;\n  font-weight: 500;\n  text-align: right; }\n\n.grand-total {\n  border-top: 1px solid #eee; }\n\n.cart-total-text {\n  color: #213051;\n  font-size: 16px;\n  font-weight: bold; }\n\n.cart-total-value {\n  color: #213051;\n  font-size: 16px;\n  font-weight: bold;\n  text-align: right; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL2NhcnQvY2FydC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFSSxxQkFBYSxFQUFBOztBQUZqQjtFQUtRLFdBQVU7RUFDVixrQkFDSixFQUFBOztBQUdKO0VBQ0ksV0FBVyxFQUFBOztBQUlmO0VBQ0ksbUJBQW1CO0VBQ25CO0FBQWEsRUFBQTs7QUFJakI7RUFDSSxhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLG1CQUFtQixFQUFBOztBQUV2QjtFQUNJLDZCQUE2QjtFQUM3QixtQkFBZ0I7RUFDaEIsd0JBQW9CLEVBQUE7O0FBSXhCO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsYUFBYTtFQUNiLGNBQWMsRUFBQTs7QUFHbEI7RUFDSSxjQUFjO0VBQ2QsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixTQUFTO0VBQ1QsVUFBVSxFQUFBOztBQUVkO0VBQ0ksZUFBZTtFQUNmLGdCQUNKLEVBQUE7O0FBRUE7RUFDSSxlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLGNBQWMsRUFBQTs7QUFFbEI7RUFDSSxtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIscUJBQXFCO0VBQ3JCLGtCQUFrQjtFQUNsQixlQUFlLEVBQUE7O0FBRW5CO0VBQ0ksZUFBZTtFQUNmLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLGdCQUFnQjtFQUNoQix5QkFBYTtFQUNiLGlDQUFxQjtFQUNyQixjQUFjO0VBQ2QsVUFBVTtFQUNWLHVCQUF1QjtFQUN2QixzQkFBc0I7RUFDdEIscUJBQXFCO0VBQ3JCLG9CQUFvQjtFQUNwQixrQkFBZ0I7RUFDaEIsZ0JBQWMsRUFBQTs7QUFJbEI7RUFDSSxhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUloQjtFQUNJLGlCQUFpQjtFQUNqQixlQUFlLEVBQUE7O0FBR25CO0VBQ0ksZ0JBQWdCO0VBQ2hCLHlCQUFhO0VBQ2IsaUNBQXFCO0VBQ3JCLGNBQWM7RUFDZCxVQUFVO0VBQ1YsYUFBYSxFQUFBOztBQUlqQjtFQUNJLFdBQVc7RUFDWCxpQkFBaUI7RUFDakIseUJBQXlCO0VBQ3pCLGVBQWU7RUFDZixvQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLFNBQVM7RUFDVCxZQUFZO0VBQ1osbUJBQWM7RUFDZCxzQkFBaUI7RUFDakIscUJBQWdCO0VBQ2hCLG1CQUFjO0VBQ2QscUJBQWE7RUFDYixXQUFXLEVBQUE7O0FBRWY7RUFDSSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBRWxCLG1EQUFtRCxFQUFBOztBQUV2RDtFQUNJLGdCQUFnQjtFQUNoQix5QkFBYTtFQUNiLGlDQUFxQjtFQUNyQixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsYUFBYSxFQUFBOztBQUdqQjtFQUFtQixXQUFXLEVBQUE7O0FBQzlCO0VBQWdCLFVBQVU7RUFDdEIsdUJBQXVCO0VBQ3ZCLHNCQUFzQjtFQUN0QixxQkFBcUI7RUFDckIsb0JBQW9CO0VBQ3BCLGtCQUFnQjtFQUNoQixnQkFBYyxFQUFBOztBQUNsQjtFQUFlLG1CQUFtQjtFQUM5QixrQkFBa0I7RUFDbEIsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixhQUFhLEVBQUE7O0FBR2pCO0VBQXFCLGVBQWUsRUFBQTs7QUFFcEM7RUFDSSxhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixjQUFjLEVBQUE7O0FBRWxCO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDSSxjQUFjO0VBQ2QsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixpQkFBaUIsRUFBQTs7QUFHckI7RUFDSSxjQUFjO0VBQ2QsZUFBZTtFQUNmLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCLEVBQUE7O0FBRXBCO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsaUJBRUosRUFBQTs7QUFDQTtFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCLEVBQUE7O0FBRXBCO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsaUJBRUosRUFBQTs7QUFFQTtFQUFhLDBCQUEwQixFQUFBOztBQUV2QztFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2YsaUJBQWlCLEVBQUE7O0FBRXJCO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsaUJBQ0osRUFBQSIsImZpbGUiOiJzcmMvYXBwL2NhcnQvY2FydC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG4gICAgLm5ldy1iYWNrZ3JvdW5kLWNvbG9yIHtcbiAgICAtLWJhY2tncm91bmQ6ICNiMzExMTc7XG4gICAgfVxuICAgIC5oZWFkZXItdGl0bGV7XG4gICAgICAgIGNvbG9yOiNmZmY7IFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXJcbiAgICB9XG4gICAgXG59ICBcbi5iYWNrLWFycm93e1xuICAgIGNvbG9yOiAjZmZmO1xufVxuXG5cbmlvbi1jb250ZW50LmhvbWUtY29udGVudCB7XG4gICAgYmFja2dyb3VuZDogI2Y0ZjhmYztcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yLCNmNGY4ZmMpXG59XG5cblxuLmNhcnQtaXRlbS1jb250ZW50e1xuICAgIHBhZGRpbmc6IDIwcHg7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuLmNhcnQtaXRlbS1pbmZve1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWVlO1xuICAgIC0tcGFkZGluZy1zdGFydDowcHg7XG4gICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xufVxuXG5cbi5jYXJ0LWl0ZW0taW5mby1jb250ZW50IGgyeyBcbiAgICBjb2xvcjogIzIxMzA1MTtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBtYXJnaW4tdG9wOiAwO1xuICAgIHBhZGRpbmctdG9wOiAwO1xufVxuXG4uY2FydC1pdGVtLWluZm8tY29udGVudCBoNCB7XG4gICAgY29sb3I6ICNhMGE3YjU7XG4gICAgZm9udC1zaXplOiAxMXB4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgbWFyZ2luOiAwO1xuICAgIHBhZGRpbmc6IDA7XG59XG4uaXRlbS1wcmljZSB7XG4gICAgcGFkZGluZzogMTBweCAwO1xuICAgIHRleHQtYWxpZ246IGxlZnRcbn1cblxuLml0ZW0tcHJpY2Ugc3BhbiB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGNvbG9yOiAjMjEzMDUxO1xufVxuLmJ0bi1hZGQtbW9yZXtcbiAgICBiYWNrZ3JvdW5kOiAjYjMxMTE3O1xuICAgIHBhZGRpbmc6IDEwcHggMTZweDtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgICBmb250LXNpemU6IDEzcHg7XG59XG4uYnRuLWNvbnRlbnQtaW5mb3tcbiAgICBwYWRkaW5nOiAxMHB4IDA7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uY2xvc2UtaWNvbntcbiAgICBiYWNrZ3JvdW5kOiBub25lO1xuICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6IHRyYW5zcGFyZW50O1xuICAgIGNvbG9yOiAjYjMxMTE3O1xuICAgIHBhZGRpbmc6IDA7XG4gICAgLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IDA7XG4gICAgbWFyZ2luLWlubGluZS1zdGFydDogMDtcbiAgICAtd2Via2l0LW1hcmdpbi1lbmQ6IDA7XG4gICAgbWFyZ2luLWlubGluZS1lbmQ6IDA7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xuICAgIC0tcGFkZGluZy1lbmQ6IDA7XG5cbn1cblxuLmFwcGx5LWNvdXBvbi1zZWN0aW9ue1xuICAgIHBhZGRpbmc6IDIwcHg7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuXG4uYXBwbHktY291cG9uLWJveHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgaGVpZ2h0OiAxMDAlO1xufVxuXG5cbi5vZmZlci1idG4tYm94e1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIHBhZGRpbmc6IDEwcHggMDtcbn1cblxuLm9mZmVyLWJ0bntcbiAgICBiYWNrZ3JvdW5kOiBub25lO1xuICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6IHRyYW5zcGFyZW50O1xuICAgIGNvbG9yOiAjYjMxMTE3O1xuICAgIHBhZGRpbmc6IDA7XG4gICAgb3V0bGluZTogbm9uZTtcblxufVxuXG4uY2hlY2tvdXQtYnRuIHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDJweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWFyZ2luOiAwO1xuICAgIGhlaWdodDogYXV0bztcbiAgICAtLXBhZGRpbmctdG9wOiAxNXB4O1xuICAgIC0tcGFkZGluZy1ib3R0b206IDE1cHg7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAzMHB4O1xuICAgIC0tcGFkZGluZy1lbmQ6IDMwcHg7XG4gICAgLS1iYWNrZ3JvdW5kOiAjYjMxMTE3O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuaW9uLWZvb3Rlci5wcm9kdWN0LXNpbmdsZS1mb290ZXIge1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgcGFkZGluZzogMTBweCAyMHB4O1xuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMCAtNHB4IDEycHggMCByZ2JhKDE1OCwgMTY5LCAxODEsIDAuNTUpO1xuICAgIGJveC1zaGFkb3c6IDAgLTRweCAxMnB4IDAgcmdiYSgxNTgsIDE2OSwgMTgxLCAwLjU1KTtcbn1cbi5hcHBseS1jb3Vwb24tYnRue1xuICAgIGJhY2tncm91bmQ6IG5vbmU7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAtLWJhY2tncm91bmQtZm9jdXNlZDogdHJhbnNwYXJlbnQ7XG4gICAgY29sb3I6ICMyMTMwNTE7XG4gICAgcGFkZGluZzogMCAwIDAgMTBweDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBvdXRsaW5lOiBub25lO1xuXG59XG4uYXBwbHktY291cG9uLWZvcm17d2lkdGg6IDEwMCU7fVxuLnBhc3N3b3JkLWlucHV0e3BhZGRpbmc6IDA7XG4gICAgLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IDA7XG4gICAgbWFyZ2luLWlubGluZS1zdGFydDogMDtcbiAgICAtd2Via2l0LW1hcmdpbi1lbmQ6IDA7XG4gICAgbWFyZ2luLWlubGluZS1lbmQ6IDA7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xuICAgIC0tcGFkZGluZy1lbmQ6IDA7fVxuLkFwcGx5LWJ0bnsgICAgYmFja2dyb3VuZDogI2IzMTExNztcbiAgICBwYWRkaW5nOiAxMHB4IDE2cHg7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIG91dGxpbmU6IG5vbmU7XG59XG5cbi5pbnB1dC1mb3JtLWNvbnRyb2x7IGZvbnQtc2l6ZTogMTJweDt9XG5cbi5jYXJ0LXRvdGFsLXNlY3Rpb257XG4gICAgcGFkZGluZzogMjBweDtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIG1hcmdpbi1ib3R0b206IDBweDtcbn1cblxuLmNhcnQtdG90YWwtc2VjdGlvbiBoMnsgXG4gICAgY29sb3I6ICNiMzExMTc7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgbWFyZ2luLXRvcDogMDtcbiAgICBwYWRkaW5nLXRvcDogMDtcbn1cbi5jYXJ0LVN1YnRvdGFsLXRleHR7XG4gICAgY29sb3I6ICMyMTMwNTE7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG5cbi5jYXJ0LVN1YnRvdGFsLXZhbHVle1xuICAgIGNvbG9yOiAjMjEzMDUxO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xufVxuXG4uY2FydC1kaXNjb3VudC10ZXh0e1xuICAgIGNvbG9yOiAjZmY5ODAwO1xuICAgIGZvbnQtc2l6ZTogMTFweDtcbiAgICBmb250LXdlaWdodDogNTAwO1xufVxuXG4uY2FydC1kaXNjb3VudC12YWx1ZXtcbiAgICBjb2xvcjogI2ZmOTgwMDtcbiAgICBmb250LXNpemU6IDExcHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbn1cblxuLmNhcnQtdGF4LXRleHR7XG4gICAgY29sb3I6ICNhMGE3YjU7XG4gICAgZm9udC1zaXplOiAxMXB4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG4uY2FydC10YXgtdmFsdWV7XG4gICAgY29sb3I6ICNhMGE3YjU7XG4gICAgZm9udC1zaXplOiAxMXB4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgdGV4dC1hbGlnbjogcmlnaHRcblxufVxuLmNhcnQtc2hpcHBpbmctdGV4dHtcbiAgICBjb2xvcjogI2EwYTdiNTtcbiAgICBmb250LXNpemU6IDExcHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cbi5jYXJ0LXNoaXBwaW5nLXZhbHVle1xuICAgIGNvbG9yOiAjYTBhN2I1O1xuICAgIGZvbnQtc2l6ZTogMTFweDtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIHRleHQtYWxpZ246IHJpZ2h0XG5cbn1cblxuLmdyYW5kLXRvdGFse2JvcmRlci10b3A6IDFweCBzb2xpZCAjZWVlO31cblxuLmNhcnQtdG90YWwtdGV4dHtcbiAgICBjb2xvcjogIzIxMzA1MTtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FydC10b3RhbC12YWx1ZXtcbiAgICBjb2xvcjogIzIxMzA1MTtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgdGV4dC1hbGlnbjogcmlnaHRcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/cart/cart.page.ts":
/*!***********************************!*\
  !*** ./src/app/cart/cart.page.ts ***!
  \***********************************/
/*! exports provided: CartPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartPage", function() { return CartPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Service_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Service/cart.service */ "./src/app/Service/cart.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");




var CartPage = /** @class */ (function () {
    function CartPage(cartService, navCtrl) {
        this.cartService = cartService;
        this.navCtrl = navCtrl;
        this.selectedItems = [];
        this.total = 0;
        this.imgUrl = "https://www.niletechinnovations.com/eq8tor";
    }
    CartPage.prototype.hide = function () {
        this.hideMe = true;
    };
    CartPage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    CartPage.prototype.ngOnInit = function () {
        // let items = [{
        // "id": 1,
        // "author_id": "1",
        // "post_content": "&lt;ul&gt;&lt;li&gt;4K UHD Wide-Angle 10x Zoom Lens with Image Stabilizer.&lt;/li&gt;&lt;li&gt;12 Megapixel One-inch 4K UHD-compatible CMOS Image Sensor.\r\n&lt;/li&gt;&lt;li&gt;Compact and Ergonomic Design.\r\n&lt;/li&gt;&lt;li&gt;DIGIC DV 5 Image Processor.\r\n&lt;/li&gt;&lt;li&gt;Advanced AF Performance&lt;/li&gt;&lt;/ul&gt;",
        // "post_title": "Canon XC15 4K UHD Professional Camcorder",
        // "post_slug": "canon-xc15-4k-uhd-professional-camcorder",
        // "post_status": "1",
        // "post_sku": null,
        // "post_regular_price": "1999.00",
        // "post_sale_price": "0.00",
        // "post_price": "1999.00",
        // "post_stock_qty": "0",
        // "post_stock_availability": "in_stock",
        // "post_type": "simple_product",
        // "post_image_url": "/public/uploads/1555487053-h-250-81ZpVG+s7AL.jpg",
        // "_product_related_images_url": {
        // "product_image": "/public/uploads/1555487053-h-250-81ZpVG+s7AL.jpg",
        // "product_gallery_images": [
        // {
        // "id": "14F4F615-0AF9-43FF-A90C-5AF97F200089",
        // "url": "/public/uploads/01555487068-h-250-71XzMbr5GkL.jpg"
        // },
        // {
        // "id": "B52235AA-701C-462C-F1B5-B3AED77A1CA5",
        // "url": "/public/uploads/11555487069-h-250-71B6Z3q1EHL.jpg"
        // },
        // {
        // "id": "3B30FD4F-C70A-4B67-D20A-DE8842880653",
        // "url": "/public/uploads/01555487092-h-250-711Hy7UeEOL.jpg"
        // }
        // ],
        // "shop_banner_image": ""
        // },
        // "product_related_img_json": "{\"product_image\":\"/public/uploads/1555487053-h-250-81ZpVG+s7AL.jpg\",\"product_gallery_images\":[{\"id\":\"14F4F615-0AF9-43FF-A90C-5AF97F200089\",\"url\":\"/public/uploads/01555487068-h-250-71XzMbr5GkL.jpg\"},{\"id\":\"B52235AA-701C-462C-F1B5-B3AED77A1CA5\",\"url\":\"/public/uploads/11555487069-h-250-71B6Z3q1EHL.jpg\"},{\"id\":\"3B30FD4F-C70A-4B67-D20A-DE8842880653\",\"url\":\"/public/uploads/01555487092-h-250-711Hy7UeEOL.jpg\"}],\"shop_banner_image\":\"\"}",
        // "_product_sale_price_start_date": "",
        // "_product_sale_price_end_date": "",
        // "_product_manage_stock": "no",
        // "_product_manage_stock_back_to_order": "not_allow",
        // "_product_extra_features": "",
        // "_product_enable_as_recommended": "yes",
        // "_product_enable_as_features": "yes",
        // "_product_enable_as_latest": "yes",
        // "_product_enable_as_related": "yes",
        // "_product_enable_as_custom_design": "yes",
        // "_product_enable_as_selected_cat": "yes",
        // "_product_enable_taxes": "no",
        // "_product_custom_designer_settings": {
        // "canvas_dimension": {
        // "small_devices": {
        // "width": 280,
        // "height": 300
        // },
        // "medium_devices": {
        // "width": 480,
        // "height": 480
        // },
        // "large_devices": {
        // "width": 500,
        // "height": 550
        // }
        // },
        // "enable_layout_at_frontend": "no",
        // "enable_global_settings": "no"
        // },
        // "_product_custom_designer_data": null,
        // "product_custom_designer_json": null,
        // "_product_enable_reviews": "yes",
        // "_product_enable_reviews_add_link_to_product_page": "yes",
        // "_product_enable_reviews_add_link_to_details_page": "yes",
        // "_product_enable_video_feature": "yes",
        // "_product_video_feature_display_mode": "popup",
        // "_product_video_feature_title": null,
        // "_product_video_feature_panel_size": {
        // "width": null,
        // "height": null
        // },
        // "_product_video_feature_source": "",
        // "_product_video_feature_source_embedded_code": null,
        // "_product_video_feature_source_online_url": null,
        // "_product_enable_manufacturer": "no",
        // "_product_enable_visibility_schedule": "no",
        // "_product_seo_title": "Canon XC15 4K UHD Professional Camcorder",
        // "_product_seo_description": null,
        // "_product_seo_keywords": null,
        // "_product_compare_data": null,
        // "_is_role_based_pricing_enable": "no",
        // "_role_based_pricing": {
        // "administrator": {
        // "regular_price": null,
        // "sale_price": ""
        // },
        // "site-user": {
        // "regular_price": null,
        // "sale_price": ""
        // },
        // "vendor": {
        // "regular_price": null,
        // "sale_price": ""
        // }
        // },
        // "_downloadable_product_files": [],
        // "_downloadable_product_download_limit": "",
        // "_downloadable_product_download_expiry": "",
        // "_upsell_products": "a:0:{}",
        // "_crosssell_products": "a:0:{}",
        // "_selected_vendor": "1",
        // "_total_sales": "3",
        // "_rating": 0
        // },{
        // "id": 2,
        // "author_id": "1",
        // "post_content": "&lt;p&gt;&lt;span style=&quot;color: rgb(76, 76, 76); font-family: Lato, sans-serif; font-size: 14px;&quot;&gt;orem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.&lt;/span&gt;&lt;br&gt;&lt;/p&gt;",
        // "post_title": "IMac App Pc – 11.6 Corporation",
        // "post_slug": "imac-app-pc-116-corporation",
        // "post_status": "1",
        // "post_sku": "120",
        // "post_regular_price": "150.00",
        // "post_sale_price": "120.00",
        // "post_price": "120.00",
        // "post_stock_qty": "0",
        // "post_stock_availability": "in_stock",
        // "post_type": "simple_product",
        // "post_image_url": "/public/uploads/1554270112-h-250-c1.jpg",
        // "_product_related_images_url": {
        // "product_image": "/public/uploads/1554270112-h-250-c1.jpg",
        // "product_gallery_images": [],
        // "shop_banner_image": ""
        // },
        // "product_related_img_json": "{\"product_image\":\"/public/uploads/1554270112-h-250-c1.jpg\",\"product_gallery_images\":[],\"shop_banner_image\":\"\"}",
        // "_product_sale_price_start_date": "2019-04-04",
        // "_product_sale_price_end_date": "2019-04-06",
        // "_product_manage_stock": "no",
        // "_product_manage_stock_back_to_order": "not_allow",
        // "_product_extra_features": "",
        // "_product_enable_as_recommended": "yes",
        // "_product_enable_as_features": "yes",
        // "_product_enable_as_latest": "yes",
        // "_product_enable_as_related": "yes",
        // "_product_enable_as_custom_design": "yes",
        // "_product_enable_as_selected_cat": "yes",
        // "_product_enable_taxes": "no",
        // "_product_custom_designer_settings": {
        // "canvas_dimension": {
        // "small_devices": {
        // "width": 280,
        // "height": 300
        // },
        // "medium_devices": {
        // "width": 480,
        // "height": 480
        // },
        // "large_devices": {
        // "width": 500,
        // "height": 550
        // }
        // },
        // "enable_layout_at_frontend": "no",
        // "enable_global_settings": "no"
        // },
        // "_product_custom_designer_data": null,
        // "product_custom_designer_json": null,
        // "_product_enable_reviews": "yes",
        // "_product_enable_reviews_add_link_to_product_page": "yes",
        // "_product_enable_reviews_add_link_to_details_page": "yes",
        // "_product_enable_video_feature": "no",
        // "_product_video_feature_display_mode": "content",
        // "_product_video_feature_title": null,
        // "_product_video_feature_panel_size": {
        // "width": null,
        // "height": null
        // },
        // "_product_video_feature_source": "",
        // "_product_video_feature_source_embedded_code": null,
        // "_product_video_feature_source_online_url": null,
        // "_product_enable_manufacturer": "no",
        // "_product_enable_visibility_schedule": "no",
        // "_product_seo_title": "IMac App Pc – 11.6 Corporation",
        // "_product_seo_description": null,
        // "_product_seo_keywords": null,
        // "_product_compare_data": null,
        // "_is_role_based_pricing_enable": "no",
        // "_role_based_pricing": {
        // "administrator": {
        // "regular_price": null,
        // "sale_price": ""
        // },
        // "site-user": {
        // "regular_price": null,
        // "sale_price": ""
        // },
        // "vendor": {
        // "regular_price": null,
        // "sale_price": ""
        // }
        // },
        // "_downloadable_product_files": [],
        // "_downloadable_product_download_limit": "",
        // "_downloadable_product_download_expiry": "",
        // "_upsell_products": "a:0:{}",
        // "_crosssell_products": "a:0:{}",
        // "_selected_vendor": "1",
        // "_rating": 0
        // }]
        var items = this.cartService.getCart();
        console.log(items);
        var selected = {};
        for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
            var obj = items_1[_i];
            if (selected[obj.id]) {
                selected[obj.id].count++;
            }
            else {
                selected[obj.id] = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, obj, { count: 1 });
            }
        }
        this.selectedItems = Object.keys(selected).map(function (key) { return selected[key]; });
        this.total = this.selectedItems.reduce(function (a, b) { return a + (b.count * b.post_price); }, 0);
    };
    CartPage.prototype.removeItem = function (post) {
        var index = this.selectedItems.indexOf(post);
        if (index > -1) {
            this.selectedItems.splice(index, 1);
        }
        this.total = this.selectedItems.reduce(function (a, b) { return a + (b.count * b.post_price); }, 0);
    };
    CartPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cart',
            template: __webpack_require__(/*! ./cart.page.html */ "./src/app/cart/cart.page.html"),
            styles: [__webpack_require__(/*! ./cart.page.scss */ "./src/app/cart/cart.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_Service_cart_service__WEBPACK_IMPORTED_MODULE_2__["CartService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]])
    ], CartPage);
    return CartPage;
}());



/***/ })

}]);
//# sourceMappingURL=cart-cart-module.js.map