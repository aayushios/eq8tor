(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-menu-menu-module~pages-wishlist-wishlist-module"],{

/***/ "./src/app/Service/cart.service.ts":
/*!*****************************************!*\
  !*** ./src/app/Service/cart.service.ts ***!
  \*****************************************/
/*! exports provided: CartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartService", function() { return CartService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CartService = /** @class */ (function () {
    function CartService() {
        this.data = [
            {
                category: "Pizza",
                expanded: false,
                products: [
                    { id: 0, name: 'Salami', price: '8' },
                    { id: 1, name: 'Classic', price: '5' },
                    { id: 2, name: 'Tuna', price: '9' },
                    { id: 3, name: 'Hawai', price: '7' }
                ]
            },
            {
                category: 'Pasta',
                products: [
                    { id: 4, name: 'Mac & Cheese', price: '8' },
                    { id: 5, name: 'Bologenese', price: '6' }
                ]
            },
            {
                category: 'Salad',
                products: [
                    { id: 6, name: 'Ham & Egg', price: '8' },
                    { id: 7, name: 'Basic', price: '5' },
                    { id: 8, name: 'Ceaser', price: '9' }
                ]
            }
        ];
        this.cart = [];
    }
    CartService.prototype.getProducts = function () {
        return this.data;
    };
    CartService.prototype.getCart = function () {
        return this.cart;
    };
    CartService.prototype.addProduct = function (product) {
        this.cart.push(product);
    };
    CartService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CartService);
    return CartService;
}());



/***/ }),

/***/ "./src/app/pages/wishlist/wishlist.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/wishlist/wishlist.module.ts ***!
  \***************************************************/
/*! exports provided: WishlistPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WishlistPageModule", function() { return WishlistPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _wishlist_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./wishlist.page */ "./src/app/pages/wishlist/wishlist.page.ts");







var routes = [
    {
        path: '',
        component: _wishlist_page__WEBPACK_IMPORTED_MODULE_6__["WishlistPage"]
    }
];
var WishlistPageModule = /** @class */ (function () {
    function WishlistPageModule() {
    }
    WishlistPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_wishlist_page__WEBPACK_IMPORTED_MODULE_6__["WishlistPage"]]
        })
    ], WishlistPageModule);
    return WishlistPageModule;
}());



/***/ }),

/***/ "./src/app/pages/wishlist/wishlist.page.html":
/*!***************************************************!*\
  !*** ./src/app/pages/wishlist/wishlist.page.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"new-background-color\">\n    <ion-title class=\"header-title\">Wishlist</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"color: white\" autoHide=\"false\"></ion-menu-button>\n    </ion-buttons>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"openCart()\">\n        <ion-badge class=\"badge-color\" *ngIf=\"cart.length > 0\">{{ cart.length }}</ion-badge>\n        <ion-icon slot=\"icon-only\" name=\"cart\" style=\"color: white\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"home-content\">\n  <ion-list class=\"cart-item-content\">\n    <ion-item class=\"cart-item-info\" *ngFor=\"let products of wishListProductsArray\">\n      <ion-row class=\"cart-item-grid\">\n        <ion-col size=\"3\">\n          <div class=\"cart-item-info-media\">\n            <img src=\"{{products.image_url}}\">\n          </div>\n        </ion-col>\n        <ion-col size=\"9\">\n          <div class=\"cart-item-info-content\">\n            <h2>{{products.slug}}</h2>\n            <h4>{{products.price}}</h4>\n            <div class=\"star-rating\">\n              <span style=\"width:50%\"></span>\n            </div>\n            <ion-button class=\"move-cart-btn\">\n                Move to Cart\n            </ion-button>\n            <ion-button class=\"move-cart-btn\" (click)=\"removeAction(products)\">\n              Remove from Wishlist\n          </ion-button>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n  </ion-list>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/wishlist/wishlist.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/wishlist/wishlist.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".new-background-color {\n  --background: #b31117; }\n\n.header-title {\n  color: #fff;\n  text-align: center; }\n\n.badge-color {\n  --background: #b31117;\n  color: white; }\n\n.star-rating {\n  font-family: \"FontAwesome\";\n  font-size: 18px;\n  height: 20px;\n  overflow: hidden;\n  position: relative;\n  width: 78px; }\n\n.star-rating {\n  margin: 10px 0; }\n\n.star-rating::before {\n  color: #656c72;\n  content: \"\\2605\\2605\\2605\\2605\\2605\";\n  float: left;\n  font-size: 17px;\n  left: 0;\n  letter-spacing: 2px;\n  position: absolute;\n  top: 0; }\n\n.star-rating span {\n  float: left;\n  left: 0;\n  overflow: hidden;\n  padding-top: 1.5em;\n  position: absolute;\n  top: 0; }\n\n.star-rating span::before {\n  color: #fab902;\n  content: \"\\2605\\2605\\2605\\2605\\2605\";\n  font-size: 17px;\n  left: 0;\n  letter-spacing: 2px;\n  position: absolute;\n  top: 0; }\n\n.star-rating .rating {\n  display: none; }\n\nion-content.home-content {\n  background: #f4f8fc;\n  --background: var(--ion-background-color,#f4f8fc)\n; }\n\n.cart-item-content {\n  padding: 20px;\n  background: #fff;\n  margin-bottom: 10px; }\n\n.cart-item-info {\n  --padding-start:0px;\n  --inner-padding-end: 0px; }\n\n.cart-item-info-content h2 {\n  color: #213051;\n  font-size: 14px;\n  font-weight: 600;\n  margin-top: 0;\n  padding-top: 0; }\n\n.cart-item-info-content h4 {\n  color: #a0a7b5;\n  font-size: 11px;\n  font-weight: 500;\n  margin: 0;\n  padding: 0; }\n\n.move-cart-btn {\n  background: #b31117;\n  color: #fff;\n  text-align: center;\n  display: inline-block;\n  border-radius: 2px;\n  font-size: 13px;\n  --background: #b31117;\n  --background-focused: #b31117;\n  --box-shadow: none;\n  --background-activated: transparent; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL3BhZ2VzL3dpc2hsaXN0L3dpc2hsaXN0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHFCQUFhLEVBQUE7O0FBR2pCO0VBQ0ksV0FBVztFQUNYLGtCQUNKLEVBQUE7O0FBRUE7RUFDSSxxQkFBYTtFQUNiLFlBQVksRUFBQTs7QUFFaEI7RUFDSSwwQkFBMEI7RUFDMUIsZUFBZTtFQUNmLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFdBQVcsRUFBQTs7QUFHYjtFQUNFLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSxjQUFjO0VBQ2Qsb0NBQW9DO0VBQ3BDLFdBQVc7RUFDWCxlQUFjO0VBQ2QsT0FBTztFQUNQLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsTUFBTSxFQUFBOztBQUdQO0VBQ0MsV0FBVztFQUNYLE9BQU87RUFDUCxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixNQUFNLEVBQUE7O0FBRVA7RUFDQyxjQUFhO0VBQ2Isb0NBQW9DO0VBQ3BDLGVBQWU7RUFDZixPQUFPO0VBQ1AsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixNQUFNLEVBQUE7O0FBR1A7RUFDQyxhQUFhLEVBQUE7O0FBSWY7RUFDRSxtQkFBbUI7RUFDbkI7QUFBYSxFQUFBOztBQUVqQjtFQUNFLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsbUJBQW1CLEVBQUE7O0FBR3JCO0VBQ0UsbUJBQWdCO0VBQ2hCLHdCQUFvQixFQUFBOztBQUd0QjtFQUNFLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixjQUFjLEVBQUE7O0FBR2hCO0VBQ0UsY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsU0FBUztFQUNULFVBQVUsRUFBQTs7QUFFWjtFQUNFLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLHFCQUFhO0VBQ2IsNkJBQXFCO0VBQ3JCLGtCQUFhO0VBQ2IsbUNBQXVCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy93aXNobGlzdC93aXNobGlzdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubmV3LWJhY2tncm91bmQtY29sb3Ige1xuICAgIC0tYmFja2dyb3VuZDogI2IzMTExNztcbn1cblxuLmhlYWRlci10aXRsZSB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyXG59XG5cbi5iYWRnZS1jb2xvciB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjYjMxMTE3O1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cbi5zdGFyLXJhdGluZ3tcbiAgICBmb250LWZhbWlseTogXCJGb250QXdlc29tZVwiO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBoZWlnaHQ6IDIwcHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgd2lkdGg6IDc4cHg7XG4gIH1cbiAgXG4gIC5zdGFyLXJhdGluZ3tcbiAgICBtYXJnaW46IDEwcHggMDsgICAgXG4gIH1cbiAgXG4gIC5zdGFyLXJhdGluZzo6YmVmb3Jle1xuICAgIGNvbG9yOiAjNjU2YzcyO1xuICAgIGNvbnRlbnQ6IFwiXFwyNjA1XFwyNjA1XFwyNjA1XFwyNjA1XFwyNjA1XCI7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgZm9udC1zaXplOjE3cHg7XG4gICAgbGVmdDogMDtcbiAgICBsZXR0ZXItc3BhY2luZzogMnB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gIH1cbiAgXG4gICAuc3Rhci1yYXRpbmcgc3BhbntcbiAgICBmbG9hdDogbGVmdDtcbiAgICBsZWZ0OiAwO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgcGFkZGluZy10b3A6IDEuNWVtO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gIH1cbiAgIC5zdGFyLXJhdGluZyBzcGFuOjpiZWZvcmV7XG4gICAgY29sb3I6I2ZhYjkwMjtcbiAgICBjb250ZW50OiBcIlxcMjYwNVxcMjYwNVxcMjYwNVxcMjYwNVxcMjYwNVwiO1xuICAgIGZvbnQtc2l6ZTogMTdweDtcbiAgICBsZWZ0OiAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAycHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbiAgfVxuICBcbiAgIC5zdGFyLXJhdGluZyAucmF0aW5ne1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cblxuXG4gIGlvbi1jb250ZW50LmhvbWUtY29udGVudCB7XG4gICAgYmFja2dyb3VuZDogI2Y0ZjhmYztcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yLCNmNGY4ZmMpXG59XG4uY2FydC1pdGVtLWNvbnRlbnR7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG5cbi5jYXJ0LWl0ZW0taW5mb3tcbiAgLS1wYWRkaW5nLXN0YXJ0OjBweDtcbiAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xufVxuXG4uY2FydC1pdGVtLWluZm8tY29udGVudCBoMnsgXG4gIGNvbG9yOiAjMjEzMDUxO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIG1hcmdpbi10b3A6IDA7XG4gIHBhZGRpbmctdG9wOiAwO1xufVxuXG4uY2FydC1pdGVtLWluZm8tY29udGVudCBoNCB7XG4gIGNvbG9yOiAjYTBhN2I1O1xuICBmb250LXNpemU6IDExcHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogMDtcbn1cbi5tb3ZlLWNhcnQtYnRue1xuICBiYWNrZ3JvdW5kOiAjYjMxMTE3O1xuICBjb2xvcjogI2ZmZjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgZm9udC1zaXplOiAxM3B4O1xuICAtLWJhY2tncm91bmQ6ICNiMzExMTc7XG4gIC0tYmFja2dyb3VuZC1mb2N1c2VkOiAjYjMxMTE3O1xuICAtLWJveC1zaGFkb3c6IG5vbmU7XG4gIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6IHRyYW5zcGFyZW50O1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/pages/wishlist/wishlist.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/wishlist/wishlist.page.ts ***!
  \*************************************************/
/*! exports provided: WishlistPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WishlistPage", function() { return WishlistPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Service_homepage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../Service/homepage.service */ "./src/app/Service/homepage.service.ts");
/* harmony import */ var _Service_cart_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../Service/cart.service */ "./src/app/Service/cart.service.ts");
/* harmony import */ var _Service_loader_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../Service/loader.service */ "./src/app/Service/loader.service.ts");
/* harmony import */ var _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../Service/aler-service.service */ "./src/app/Service/aler-service.service.ts");






var WishlistPage = /** @class */ (function () {
    function WishlistPage(homePageService, alert, loadingService, cartService) {
        this.homePageService = homePageService;
        this.alert = alert;
        this.loadingService = loadingService;
        this.cartService = cartService;
        this.cart = [];
        this.getWishlist();
    }
    WishlistPage.prototype.ngOnInit = function () {
        this.cart = this.cartService.getCart();
    };
    WishlistPage.prototype.getWishlist = function () {
        var _this = this;
        this.loadingService.present();
        this.homePageService.getWishlistProducts().subscribe(function (resp) {
            _this.wishListProductsArray = resp.data.wishlist;
            console.log(_this.wishListProductsArray);
            _this.loadingService.dismiss();
        }, function (err) {
            console.log("error", err);
            _this.loadingService.dismiss();
        });
    };
    WishlistPage.prototype.addToCart = function (product) {
        this.cartService.addProduct(product);
        console.log(product);
    };
    WishlistPage.prototype.removeAction = function (product) {
        var _this = this;
        this.loadingService.present();
        this.homePageService.removeFromWishList(product.id).subscribe(function (resp) {
            console.log(resp.success);
            if (resp.success == true) {
                var index = _this.wishListProductsArray.indexOf(product);
                if (index > -1) {
                    _this.wishListProductsArray.splice(index, 1);
                }
            }
            _this.loadingService.dismiss();
        }, function (err) {
            console.log("error", err);
            _this.loadingService.dismiss();
            _this.alert.myAlertMethod("Network Error", err.message, function (data) {
                console.log("wishlist remove error");
            });
        });
    };
    WishlistPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-wishlist',
            template: __webpack_require__(/*! ./wishlist.page.html */ "./src/app/pages/wishlist/wishlist.page.html"),
            styles: [__webpack_require__(/*! ./wishlist.page.scss */ "./src/app/pages/wishlist/wishlist.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_Service_homepage_service__WEBPACK_IMPORTED_MODULE_2__["HomepageService"], _Service_aler_service_service__WEBPACK_IMPORTED_MODULE_5__["AlerServiceService"], _Service_loader_service__WEBPACK_IMPORTED_MODULE_4__["LoaderService"], _Service_cart_service__WEBPACK_IMPORTED_MODULE_3__["CartService"]])
    ], WishlistPage);
    return WishlistPage;
}());



/***/ })

}]);
//# sourceMappingURL=default~pages-menu-menu-module~pages-wishlist-wishlist-module.js.map