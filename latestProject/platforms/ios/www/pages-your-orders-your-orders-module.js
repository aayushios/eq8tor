(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-your-orders-your-orders-module"],{

/***/ "./src/app/pages/your-orders/your-orders.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/your-orders/your-orders.module.ts ***!
  \*********************************************************/
/*! exports provided: YourOrdersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "YourOrdersPageModule", function() { return YourOrdersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _your_orders_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./your-orders.page */ "./src/app/pages/your-orders/your-orders.page.ts");







var routes = [
    {
        path: '',
        component: _your_orders_page__WEBPACK_IMPORTED_MODULE_6__["YourOrdersPage"]
    }
];
var YourOrdersPageModule = /** @class */ (function () {
    function YourOrdersPageModule() {
    }
    YourOrdersPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_your_orders_page__WEBPACK_IMPORTED_MODULE_6__["YourOrdersPage"]]
        })
    ], YourOrdersPageModule);
    return YourOrdersPageModule;
}());



/***/ }),

/***/ "./src/app/pages/your-orders/your-orders.page.html":
/*!*********************************************************!*\
  !*** ./src/app/pages/your-orders/your-orders.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"new-background-color\">\n    <ion-title class=\"header-title\">Orders</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"color: white\" autoHide=\"false\"></ion-menu-button>\n    </ion-buttons>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"openCart()\">\n        <!-- <ion-badge class=\"badge-color\" *ngIf=\"cart.length > 0\">{{ cart.length }}</ion-badge> -->\n        <ion-icon slot=\"icon-only\" name=\"cart\" style=\"color: white\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"orders-content\">\n  <ion-grid *ngFor=\"let order of ordersArray\" class=\"orders-list-content\">\n    <ion-row class=\"orders-item-content\" >\n      <ion-col size=\"6\">\n        <div class=\"orders-id\">\n          <h2>Order Id: #{{order._post_id}}</h2>\n        </div>\n      </ion-col>\n      <ion-col size=\"6\">\n        <div class=\"button-status\">\n          <ion-button class=\"btn-view\" (click)=\"viewOrderDetails(order)\">\n            VIEW\n          </ion-button>\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"orders-item-content\">\n      <ion-col size=\"4\">\n        <div class=\"orders-date\">\n          <h2>Order Date</h2>\n          <p>{{order._order_date}}</p>\n        </div>\n      </ion-col>\n      <ion-col size=\"4\">\n        <div class=\"orders-status\">\n          <h2>Order status</h2>\n          <p>{{order._order_status}}</p>\n        </div>\n      </ion-col>\n      <ion-col size=\"4\">\n        <div class=\"orders-price\">\n          <h2>Order Price</h2>\n          <p>{{order._order_currency}} {{order._final_order_total}}</p>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/your-orders/your-orders.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/your-orders/your-orders.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".new-background-color {\n  --background: #b31117; }\n\n.header-title {\n  color: #fff;\n  text-align: center; }\n\n.badge-color {\n  --background: #b31117;\n  color: white; }\n\n.star-rating {\n  font-family: \"FontAwesome\";\n  font-size: 18px;\n  height: 20px;\n  overflow: hidden;\n  position: relative;\n  width: 78px; }\n\n.star-rating {\n  margin: 10px auto; }\n\n.star-rating::before {\n  color: #656c72;\n  content: \"\\2605\\2605\\2605\\2605\\2605\";\n  float: left;\n  font-size: 17px;\n  left: 0;\n  letter-spacing: 2px;\n  position: absolute;\n  top: 0; }\n\n.star-rating span {\n  float: left;\n  left: 0;\n  overflow: hidden;\n  padding-top: 1.5em;\n  position: absolute;\n  top: 0; }\n\n.star-rating span::before {\n  color: #fab902;\n  content: \"\\2605\\2605\\2605\\2605\\2605\";\n  font-size: 17px;\n  left: 0;\n  letter-spacing: 2px;\n  position: absolute;\n  top: 0; }\n\n.header-arrow {\n  color: white; }\n\n.star-rating .rating {\n  display: none; }\n\n.orders-content {\n  background: #f4f8fc;\n  --background: var(--ion-background-color,#f4f8fc); }\n\n.orders-id {\n  border: 2px dotted #FFC107;\n  background: #fff0c4;\n  width: 100%;\n  display: inline-block;\n  padding: 10px; }\n\n.orders-id h2 {\n  font-size: 14px;\n  font-weight: 600;\n  margin: 0;\n  padding: 0;\n  color: #4e5b6a; }\n\nion-list.orders-list-content {\n  margin-bottom: 10px; }\n\n.orders-item-content {\n  padding: 10px;\n  background: #fff;\n  border: none; }\n\n.button-status {\n  text-align: right; }\n\n.orders-date h2 {\n  color: #213051;\n  font-size: 14px;\n  font-weight: 600;\n  padding-top: 0;\n  margin-top: 0; }\n\n.orders-date p {\n  color: #213051;\n  font-size: 14px;\n  font-weight: 500;\n  line-height: 24px; }\n\n.orders-status h2 {\n  color: #213051;\n  font-size: 14px;\n  font-weight: 600;\n  padding-top: 0;\n  margin-top: 0; }\n\n.orders-status p {\n  color: #213051;\n  font-size: 14px;\n  font-weight: 500;\n  line-height: 24px; }\n\n.orders-price h2 {\n  color: #213051;\n  font-size: 14px;\n  font-weight: 600;\n  padding-top: 0;\n  margin-top: 0; }\n\n.orders-price p {\n  color: #213051;\n  font-size: 14px;\n  font-weight: 500;\n  line-height: 24px; }\n\n.btn-view {\n  color: #fff;\n  font-weight: bold;\n  text-transform: uppercase;\n  font-size: 14px;\n  --border-radius: 2px;\n  display: inline-block;\n  margin: 0;\n  height: auto;\n  --padding-top: 15px;\n  --padding-bottom: 15px;\n  --padding-start: 30px;\n  --padding-end: 30px;\n  --background: #b31117;\n  --background-activated:#b31117; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hYXl1c2hrYXRpeWFyL0Rlc2t0b3AvZXE4dG9yTmlsZS9lcTh0b3IvbGF0ZXN0UHJvamVjdC9zcmMvYXBwL3BhZ2VzL3lvdXItb3JkZXJzL3lvdXItb3JkZXJzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHFCQUFhLEVBQUE7O0FBR2pCO0VBQ0ksV0FBVztFQUNYLGtCQUNKLEVBQUE7O0FBRUE7RUFDSSxxQkFBYTtFQUNiLFlBQVksRUFBQTs7QUFFaEI7RUFDSSwwQkFBMEI7RUFDMUIsZUFBZTtFQUNmLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFdBQVcsRUFBQTs7QUFHYjtFQUNFLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLGNBQWM7RUFDZCxvQ0FBb0M7RUFDcEMsV0FBVztFQUNYLGVBQWM7RUFDZCxPQUFPO0VBQ1AsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixNQUFNLEVBQUE7O0FBR1A7RUFDQyxXQUFXO0VBQ1gsT0FBTztFQUNQLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLE1BQU0sRUFBQTs7QUFFUDtFQUNDLGNBQWE7RUFDYixvQ0FBb0M7RUFDcEMsZUFBZTtFQUNmLE9BQU87RUFDUCxtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLE1BQU0sRUFBQTs7QUFFUjtFQUVFLFlBQVksRUFBQTs7QUFFYjtFQUNDLGFBQWEsRUFBQTs7QUFHakI7RUFDRSxtQkFBbUI7RUFDakIsaURBQWEsRUFBQTs7QUFHZjtFQUNFLDBCQUEwQjtFQUMxQixtQkFBbUI7RUFDbkIsV0FBVztFQUNYLHFCQUFxQjtFQUNyQixhQUFhLEVBQUE7O0FBRWpCO0VBQWMsZUFBYztFQUFFLGdCQUFnQjtFQUFFLFNBQVM7RUFBQyxVQUFVO0VBQUUsY0FBYSxFQUFBOztBQUVuRjtFQUNFLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLGFBQWE7RUFDYixnQkFBZ0I7RUFDbEIsWUFBWSxFQUFBOztBQUVaO0VBQWUsaUJBQWlCLEVBQUE7O0FBRWhDO0VBQWdCLGNBQWM7RUFDNUIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsYUFBYSxFQUFBOztBQUVmO0VBQW1CLGNBQWM7RUFDL0IsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixpQkFBaUIsRUFBQTs7QUFFakI7RUFBa0IsY0FBYztFQUM5QixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxhQUFhLEVBQUE7O0FBRWY7RUFBcUIsY0FBYztFQUNqQyxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGlCQUFpQixFQUFBOztBQUVqQjtFQUFpQixjQUFjO0VBQzdCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGFBQWEsRUFBQTs7QUFFZjtFQUFvQixjQUFjO0VBQ2hDLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsaUJBQWlCLEVBQUE7O0FBR25CO0VBQWMsV0FBVztFQUN2QixpQkFBaUI7RUFDakIseUJBQXlCO0VBQ3pCLGVBQWU7RUFDZixvQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLFNBQVM7RUFDVCxZQUFZO0VBQ1osbUJBQWM7RUFDZCxzQkFBaUI7RUFDakIscUJBQWdCO0VBQ2hCLG1CQUFjO0VBQ2QscUJBQWE7RUFDYiw4QkFBdUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3lvdXItb3JkZXJzL3lvdXItb3JkZXJzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5uZXctYmFja2dyb3VuZC1jb2xvciB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjYjMxMTE3O1xufVxuXG4uaGVhZGVyLXRpdGxlIHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXJcbn1cblxuLmJhZGdlLWNvbG9yIHtcbiAgICAtLWJhY2tncm91bmQ6ICNiMzExMTc7XG4gICAgY29sb3I6IHdoaXRlO1xufVxuLnN0YXItcmF0aW5ne1xuICAgIGZvbnQtZmFtaWx5OiBcIkZvbnRBd2Vzb21lXCI7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGhlaWdodDogMjBweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB3aWR0aDogNzhweDtcbiAgfVxuICBcbiAgLnN0YXItcmF0aW5ne1xuICAgIG1hcmdpbjogMTBweCBhdXRvOyAgICBcbiAgfVxuICBcbiAgLnN0YXItcmF0aW5nOjpiZWZvcmV7XG4gICAgY29sb3I6ICM2NTZjNzI7XG4gICAgY29udGVudDogXCJcXDI2MDVcXDI2MDVcXDI2MDVcXDI2MDVcXDI2MDVcIjtcbiAgICBmbG9hdDogbGVmdDtcbiAgICBmb250LXNpemU6MTdweDtcbiAgICBsZWZ0OiAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAycHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbiAgfVxuICBcbiAgIC5zdGFyLXJhdGluZyBzcGFue1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIGxlZnQ6IDA7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBwYWRkaW5nLXRvcDogMS41ZW07XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbiAgfVxuICAgLnN0YXItcmF0aW5nIHNwYW46OmJlZm9yZXtcbiAgICBjb2xvcjojZmFiOTAyO1xuICAgIGNvbnRlbnQ6IFwiXFwyNjA1XFwyNjA1XFwyNjA1XFwyNjA1XFwyNjA1XCI7XG4gICAgZm9udC1zaXplOiAxN3B4O1xuICAgIGxlZnQ6IDA7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDJweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICB9XG4gIC5oZWFkZXItYXJyb3d7XG5cbiAgICBjb2xvcjogd2hpdGU7XG59XG4gICAuc3Rhci1yYXRpbmcgLnJhdGluZ3tcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG5cbi5vcmRlcnMtY29udGVudHtcbiAgYmFja2dyb3VuZDogI2Y0ZjhmYztcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yLCNmNGY4ZmMpO1xufVxuXG4gIC5vcmRlcnMtaWQge1xuICAgIGJvcmRlcjogMnB4IGRvdHRlZCAjRkZDMTA3O1xuICAgIGJhY2tncm91bmQ6ICNmZmYwYzQ7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHBhZGRpbmc6IDEwcHg7XG59XG4ub3JkZXJzLWlkIGgye2ZvbnQtc2l6ZToxNHB4OyBmb250LXdlaWdodDogNjAwOyBtYXJnaW46IDA7cGFkZGluZzogMDsgY29sb3I6IzRlNWI2YTt9XG5cbmlvbi1saXN0Lm9yZGVycy1saXN0LWNvbnRlbnQge1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuXG4ub3JkZXJzLWl0ZW0tY29udGVudHsgICAgXG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG5ib3JkZXI6IG5vbmV9XG5cbi5idXR0b24tc3RhdHVze3RleHQtYWxpZ246IHJpZ2h0fVxuXG4ub3JkZXJzLWRhdGUgaDJ7Y29sb3I6ICMyMTMwNTE7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgcGFkZGluZy10b3A6IDA7XG4gIG1hcmdpbi10b3A6IDA7fVxuXG4ub3JkZXJzLWRhdGUgcHsgICAgY29sb3I6ICMyMTMwNTE7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbGluZS1oZWlnaHQ6IDI0cHg7fVxuXG4gIC5vcmRlcnMtc3RhdHVzIGgye2NvbG9yOiAjMjEzMDUxO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIHBhZGRpbmctdG9wOiAwO1xuICAgIG1hcmdpbi10b3A6IDA7fVxuICBcbiAgLm9yZGVycy1zdGF0dXMgcHsgICAgY29sb3I6ICMyMTMwNTE7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7fVxuXG4gICAgLm9yZGVycy1wcmljZSBoMntjb2xvcjogIzIxMzA1MTtcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICBwYWRkaW5nLXRvcDogMDtcbiAgICAgIG1hcmdpbi10b3A6IDA7fVxuICAgIFxuICAgIC5vcmRlcnMtcHJpY2UgcHsgICAgY29sb3I6ICMyMTMwNTE7XG4gICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgbGluZS1oZWlnaHQ6IDI0cHg7fVxuXG4gICAgICBcbiAgICAuYnRuLXZpZXd7ICAgIGNvbG9yOiAjZmZmO1xuICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgLS1ib3JkZXItcmFkaXVzOiAycHg7XG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICBtYXJnaW46IDA7XG4gICAgICBoZWlnaHQ6IGF1dG87XG4gICAgICAtLXBhZGRpbmctdG9wOiAxNXB4O1xuICAgICAgLS1wYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgICAgIC0tcGFkZGluZy1zdGFydDogMzBweDtcbiAgICAgIC0tcGFkZGluZy1lbmQ6IDMwcHg7XG4gICAgICAtLWJhY2tncm91bmQ6ICNiMzExMTc7XG4gICAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiNiMzExMTc7XG4gICAgfVxuXG4gICAgIl19 */"

/***/ }),

/***/ "./src/app/pages/your-orders/your-orders.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/your-orders/your-orders.page.ts ***!
  \*******************************************************/
/*! exports provided: YourOrdersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "YourOrdersPage", function() { return YourOrdersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Service_homepage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../Service/homepage.service */ "./src/app/Service/homepage.service.ts");
/* harmony import */ var _Service_loader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../Service/loader.service */ "./src/app/Service/loader.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _order_details_order_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../order-details/order-details.page */ "./src/app/order-details/order-details.page.ts");







var YourOrdersPage = /** @class */ (function () {
    function YourOrdersPage(homepageService, router, navCtrl, loadingService, modalController) {
        this.homepageService = homepageService;
        this.router = router;
        this.navCtrl = navCtrl;
        this.loadingService = loadingService;
        this.modalController = modalController;
        this.getAllOrders();
    }
    YourOrdersPage.prototype.ngOnInit = function () {
    };
    YourOrdersPage.prototype.viewOrderDetails = function (order) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _order_details_order_details_page__WEBPACK_IMPORTED_MODULE_6__["OrderDetailsPage"],
                            componentProps: {
                                "process": order._order_process_key,
                                "id": order._post_id
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (sizeChartResponse) {
                            if (sizeChartResponse !== null) {
                                //this.sizeChartResponse = sizeChartResponse.data;
                            }
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    YourOrdersPage.prototype.getAllOrders = function () {
        var _this = this;
        this.loadingService.present();
        this.homepageService.getAllOrders().subscribe(function (resp) {
            _this.ordersArray = resp.data.orders_list_data;
            console.log(_this.ordersArray);
            _this.loadingService.dismiss();
        }, function (err) {
            console.log(err);
            _this.loadingService.dismiss();
        });
    };
    YourOrdersPage.prototype.goBack = function () {
        this.navCtrl.back();
    };
    YourOrdersPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-your-orders',
            template: __webpack_require__(/*! ./your-orders.page.html */ "./src/app/pages/your-orders/your-orders.page.html"),
            styles: [__webpack_require__(/*! ./your-orders.page.scss */ "./src/app/pages/your-orders/your-orders.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_Service_homepage_service__WEBPACK_IMPORTED_MODULE_2__["HomepageService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], _Service_loader_service__WEBPACK_IMPORTED_MODULE_3__["LoaderService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]])
    ], YourOrdersPage);
    return YourOrdersPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-your-orders-your-orders-module.js.map