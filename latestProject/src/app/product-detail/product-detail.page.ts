import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HomepageService } from '../Service/homepage.service';
import { NavController, IonSlides, ModalController } from '@ionic/angular';
import { SizechartModalPage } from '../sizechart-modal/sizechart-modal.page';
import { JsonPipe } from '@angular/common';
import { AuthService } from '../Service/auth.service';
import { CartService } from '../Service/cart.service';
import { ToastController } from '@ionic/angular';
import { LoaderService } from '../Service/loader.service';
import { AlerServiceService } from '../Service/aler-service.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.page.html',
  styleUrls: ['./product-detail.page.scss'],
})
export class ProductDetailPage implements OnInit {
  bannerSlider = {
    initialSlide: 0,
    speed: 2000,
    slidesPerView: 1,
    autoplay: true,
  };

  isSelected: boolean;
  cartAdded: boolean;
  @ViewChild(IonSlides) slides: IonSlides;
  private selecteTextId: string;
  detailsContent: any;
  detailImageList: any;
  sizeChartResponse: any;
  qty:any;
  apiUrl = "https://www.niletechinnovations.com/eq8tor"
  sizeArray = [
    {
      "title": "S",
      "isSelected": false
    },
    {
      "title": "M",
      "isSelected": false
    },
    {
      "title": "L",
      "isSelected": false
    },
    {
      "title": "XL",
      "isSelected": false
    },
    {
      "title": "XXL",
      "isSelected": false
    }
  ];
  constructor(private alert: AlerServiceService, public loadingService: LoaderService, private route: ActivatedRoute, public toastController: ToastController, private cartService: CartService, private homepageService: HomepageService, private navCtrl: NavController, public modalController: ModalController) {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      let productName = params.id;
      console.log("product name", productName);
      this.getProductDetail(productName);
    });
    this.qty = 1;
  }

  ngOnInit() {
  }
  getProductDetail(product) {
    this.loadingService.present();
    this.homepageService.productDetail(product).subscribe(response => {
      let resp = response.data.single_product_details;
      this.detailsContent = resp;
      this.detailImageList = resp._product_related_images_url.product_gallery_images;
      console.log("strHtml detail", this.detailsContent);
      this.loadingService.dismiss();
    }, (err) => {
      console.log(err);
      this.alert.myAlertMethod("Network Error", err.error.message, data => {
        console.log("hello alert")
      });
      this.loadingService.dismiss();
    })

  }

  goBack() {
    this.navCtrl.back();
  }

  getCartContent() {
    console.log("getCartContent");

    this.homepageService.getCartProducts().subscribe(resp => {
      console.log("getCartContent",JSON.stringify(resp));
    }, err => {
      console.log("getCartContent error",err);
    });
  }
  async openSizeChart() {
    const modal = await this.modalController.create({
      component: SizechartModalPage,
      componentProps: {
        "testParam": 123,
        "testurl": "https://www.google.com"
      }
    });
    modal.onDidDismiss().then((sizeChartResponse) => {
      if (sizeChartResponse !== null) {
        this.sizeChartResponse = sizeChartResponse.data;
      }
    });
    return await modal.present();
  }

  btnActivate(ind) {
    for (let i = 0; i < this.sizeArray.length; i++) {
      this.sizeArray[i].isSelected = false;
    }
    this.sizeArray[ind].isSelected = true;
  }
  incrementQty() {
    console.log(this.qty+1);
    this.qty += 1;
    }

    decrementQty() {
      if(this.qty-1 < 1 ){
      this.qty = 1
      console.log('1->'+this.qty);
      }else{
      this.qty -= 1;
      console.log('2->'+this.qty);
      }
      }


  addToCart(productID, Quantity) {
    if (productID) {
      this.homepageService.addToCart(productID, Quantity).subscribe(resp => {
        console.log("response add to cart", resp);
        this.presentToast();
      }, (err) => {
        console.log("error add to cart", err);
      })
      // this.cartService.addProduct(product);
      // this.presentToast();
      // console.log(product);
       this.cartAdded = true;
    }
  }


  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Product Added to cart.',
      duration: 2000
    });
    toast.present();
  }

}

