import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
// import {AuthGuard} from './Service/auth.guard';
const routes: Routes = [
  { path: '', redirectTo: 'user-login', pathMatch: 'full' },
  // { 
  //   path: 'home', 
  //   canActivate: [AuthGuard],
  //   loadChildren: './home/home.module#HomePageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'user-login', loadChildren: './user-login/user-login.module#UserLoginPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  { path: 'menu', loadChildren: './pages/menu/menu.module#MenuPageModule' },
  { path: 'categories', loadChildren: './pages/categories/categories.module#CategoriesPageModule' },
  { path: 'wishlist', loadChildren: './pages/wishlist/wishlist.module#WishlistPageModule' },
  { path: 'cart', loadChildren: './cart/cart.module#CartPageModule' },
  { path: 'forgotpassword', loadChildren: './forgotpassword/forgotpassword.module#ForgotpasswordPageModule' },
  { path: 'searchpage', loadChildren: './searchpage/searchpage.module#SearchpagePageModule' },
  { path: 'subcategory', loadChildren: './subcategory/subcategory.module#SubcategoryPageModule' },
  { path: 'productlisting', loadChildren: './productlisting/productlisting.module#ProductlistingPageModule' },
  { path: 'product-detail', loadChildren: './product-detail/product-detail.module#ProductDetailPageModule' },
  { path: 'sizechart-modal', loadChildren: './sizechart-modal/sizechart-modal.module#SizechartModalPageModule' },
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
  { path: 'your-orders', loadChildren: './pages/your-orders/your-orders.module#YourOrdersPageModule' },
  { path: 'address', loadChildren: './address/address.module#AddressPageModule' },
  { path: 'order-details', loadChildren: './order-details/order-details.module#OrderDetailsPageModule' },
  { path: 'edit-address', loadChildren: './edit-address/edit-address.module#EditAddressPageModule' },
  { path: 'stripe-checkout', loadChildren: './stripe-checkout/stripe-checkout.module#StripeCheckoutPageModule' },
  //  { path: 'menu-routing', loadChildren: './pages/menu/menu-routing.module#MenuRoutingPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
