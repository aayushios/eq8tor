import { Component, OnInit } from '@angular/core';
import { CartService } from '../Service/cart.service';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
selector: 'app-cart',
templateUrl: './cart.page.html',
styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
selectedItems = [];
total = 0;
hideMe :boolean;
imgUrl ="https://www.niletechinnovations.com/eq8tor";

constructor(private cartService: CartService,public navCtrl: NavController,private router: Router) {
}
hide() {
this.hideMe = true;
}
goBack() {
    this.navCtrl.back();
  }
  
ngOnInit() {
let items = this.cartService.getCart();
console.log(items)
let selected = {};
for (let obj of items) {
if (selected[obj.id]) {
selected[obj.id].count++
} else {
selected[obj.id] = { ...obj, count: 1 };
}
}
this.selectedItems = Object.keys(selected).map(key => selected[key]);
this.total = this.selectedItems.reduce((a, b) => a + (b.count * b.post_price), 0);
}
removeItem(post){
let index = this.selectedItems.indexOf(post);
if(index > -1){
this.selectedItems.splice(index, 1);
}
this.total = this.selectedItems.reduce((a, b) => a + (b.count * b.post_price), 0);
}

goToCheckout(){
  this.router.navigate(['stripe-checkout']);
}
}