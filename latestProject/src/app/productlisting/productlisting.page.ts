import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { LoaderService } from '../Service/loader.service';
import { NavController } from '@ionic/angular';
import { HomepageService } from '../Service/homepage.service';
import { AlerServiceService } from '../Service/aler-service.service';

@Component({
  selector: 'app-productlisting',
  templateUrl: './productlisting.page.html',
  styleUrls: ['./productlisting.page.scss'],
})
export class ProductlistingPage implements OnInit {
  productArray: any;

  constructor(private alert: AlerServiceService, private route: ActivatedRoute, private homepageService: HomepageService, private router: Router, public navCtrl: NavController, private loadingService: LoaderService, ) {
    this.route.queryParams.subscribe(params => {
      console.log("test",JSON.stringify(params.id));

      this.getProducts(params.id)

    });
  }

  ngOnInit() {

  }

  goBack() {
    this.navCtrl.back();
  }
  
  getProducts(itemName) {
    console.log("getProducts",itemName);
    this.homepageService.getCategoryProducts(itemName).subscribe(response => {
      let resp = response.data;
      this.productArray = resp.products;
    }, (err) => {
      this.alert.myAlertMethod(err.message, "Please try again later.", data => {
        console.log("hello showProducts")
      });
      this.loadingService.dismiss();
    });
  }
  goToDetail(id){
    console.log("product id",id);
    for (let item of this.productArray){
      if (item.id == id){
        let navigationextras:NavigationExtras={
          queryParams:{
            detail:JSON.stringify(item)
          }
        }
        console.log(item);
        this.router.navigate(['product-detail'],navigationextras);
      }
    }
    // let data = this.productArray[id];
    // console.log("data data",data);
  }
}
