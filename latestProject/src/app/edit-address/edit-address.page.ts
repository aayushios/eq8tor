import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, NavController } from '@ionic/angular';
import { HomepageService } from '../Service/homepage.service';
import { LoaderService } from '../Service/loader.service';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { AlerServiceService } from '../Service/aler-service.service';

@Component({
  selector: 'app-edit-address',
  templateUrl: './edit-address.page.html',
  styleUrls: ['./edit-address.page.scss'],
})
export class EditAddressPage implements OnInit {

  account_bill_company_name = "";
  account_bill_adddress_line_1 = "";
  account_bill_town_or_city = "";
  account_bill_zip_or_postal_code = "";
  account_bill_select_country = "";
  account_bill_phone_number = "";
  account_bill_email_address = "";
  account_bill_first_name = "";
  account_bill_last_name = "";
  account_bill_title = '';
  account_shipping_company_name = "";
  account_shipping_adddress_line_1 = "";
  account_shipping_town_or_city = "";
  account_shipping_zip_or_postal_code = "";
  account_shipping_select_country = "";
  account_shipping_phone_number = "";
  account_shipping_email_address = "";
  account_shipping_first_name = "";
  account_shipping_last_name = "";
  account_shipping_title = '';
  countryArray = [];
  editAddress: FormGroup;
  constructor(public homePageService: HomepageService,private alertService:AlerServiceService, private formBuilder: FormBuilder, public modalController: ModalController, private loadingService: LoaderService, private navCtrl: NavController) {
    this.homePageService.getCountries().subscribe(resp => {
      this.countryArray = resp.data;
    });
    this.editAddress = this.formBuilder.group({
      account_bill_adddress_line_1: ["", Validators.required],
      account_bill_title: ["", Validators.required],
      account_bill_company_name: ["", Validators.required],
      account_bill_select_country: ["", Validators.required],
      account_bill_last_name: ["", Validators.required],
      account_bill_town_or_city: ["", Validators.required],
      account_bill_zip_or_postal_code: ["", Validators.required],
      account_bill_first_name: ["", Validators.required],
      account_bill_phone_number: ["", Validators.required],
      account_bill_email_address: ["", Validators.required],
      account_shipping_company_name: ["", Validators.required],
      account_shipping_adddress_line_1: ["", Validators.required],
      account_shipping_town_or_city: ["", Validators.required],
      account_shipping_zip_or_postal_code: ["", Validators.required],
      account_shipping_select_country: ["", Validators.required],
      account_shipping_phone_number: ["", Validators.required],
      account_shipping_email_address: ["", Validators.required],
      account_shipping_first_name: ["", Validators.required],
      account_shipping_last_name: ["", Validators.required],
      account_shipping_title: ["", Validators.required],
    });
  }

  ngOnInit() {
    this.getWishlist();
  }

  getWishlist() {
    this.loadingService.present();
    this.homePageService.getWishlistProducts().subscribe(resp => {
      this.editAddress.controls.account_bill_title.setValue(resp.data.address_details.account_bill_title);
      this.editAddress.controls.account_bill_company_name.setValue(resp.data.address_details.account_bill_company_name);
      this.editAddress.controls.account_bill_adddress_line_1.setValue(resp.data.address_details.account_bill_adddress_line_1);
      this.editAddress.controls.account_bill_town_or_city.setValue(resp.data.address_details.account_bill_town_or_city);
      this.editAddress.controls.account_bill_zip_or_postal_code.setValue(resp.data.address_details.account_bill_zip_or_postal_code);
      this.editAddress.controls.account_bill_select_country.setValue(resp.data.address_details.account_bill_select_country);
      this.editAddress.controls.account_bill_phone_number.setValue(resp.data.address_details.account_bill_phone_number);
      this.editAddress.controls.account_bill_email_address.setValue(resp.data.address_details.account_bill_email_address);
      this.editAddress.controls.account_bill_first_name.setValue(resp.data.address_details.account_bill_first_name);
      this.editAddress.controls.account_bill_last_name.setValue(resp.data.address_details.account_bill_last_name);
      this.editAddress.controls.account_shipping_company_name.setValue(resp.data.address_details.account_shipping_company_name);
      this.editAddress.controls.account_shipping_adddress_line_1.setValue(resp.data.address_details.account_shipping_adddress_line_1);
      this.editAddress.controls.account_shipping_town_or_city.setValue(resp.data.address_details.account_shipping_town_or_city);
      this.editAddress.controls.account_shipping_zip_or_postal_code.setValue(resp.data.address_details.account_shipping_zip_or_postal_code);
      this.editAddress.controls.account_shipping_select_country.setValue(resp.data.address_details.account_shipping_select_country);
      this.editAddress.controls.account_shipping_phone_number.setValue(resp.data.address_details.account_shipping_phone_number);
      this.editAddress.controls.account_shipping_email_address.setValue(resp.data.address_details.account_shipping_email_address);
      this.editAddress.controls.account_shipping_first_name.setValue(resp.data.address_details.account_shipping_first_name);
      this.editAddress.controls.account_shipping_last_name.setValue(resp.data.address_details.account_shipping_last_name);
      this.editAddress.controls.account_shipping_title.setValue(resp.data.address_details.account_shipping_title);

      this.loadingService.dismiss();
    },
      (err) => {
        console.log("error", err);
        this.loadingService.dismiss();
      })
  }
  async closeModal() {
    const onClosedData: string = "wrapped up";
    await this.modalController.dismiss(onClosedData);
  }
  // UpdateAddress() {
  //   this.homePageService.updateAddress(this.billingAddressFirstName, this.billingAddressLastName, this.billEmail, this.billPhone, this.billCountry, this.billAddress, this.billCity, this.billPostCode, this.shippingAddressFirstName, this.shippingAddressLastName, this.shipEmail, this.shipPhone, this.shipCountry, this.shipAddress, this.shipCity, this.shipPostCode).subscribe(resp => {
  //     console.log('updateAddress', resp);
  //   }, (err) => {
  //     console.log('updateAddress', err);
  //   })
  // }
  
  updateFirmwareList() {
    console.log('Event Called');
  }
  FilterItems(code: any) {
    this.editAddress.controls.account_bill_select_country.setValue(code);
    console.log(this.editAddress.controls.billCountry.value, "bill country1", this.account_bill_select_country)
    this.account_bill_select_country = this.editAddress.controls.account_bill_select_country.value;
    console.log("bill country2", this.account_bill_select_country)
  }

  FilterItemsShipping(code: any) {
    this.editAddress.controls.account_shipping_select_country.setValue(code);
    this.account_shipping_select_country = this.editAddress.controls.account_shipping_select_country.value;    
  }
  onFormSubmit(form: NgForm) {
    this.loadingService.present();
    console.log(form)
    this.homePageService.updateAddressPost(form).subscribe(res => {
      this.alertService.myAlertMethod(res.message,"",data => {
           console.log("",data);
         });
      this.loadingService.dismiss();
    }, (err) => {
      console.log(err);
      this.alertService.myAlertMethod(err.message,"",data => {
        console.log("",data);
      });
      this.loadingService.dismiss();
    })
  }

}
