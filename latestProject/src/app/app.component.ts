import { Component, ViewChild,QueryList } from '@angular/core';

import { Platform,Events,IonRouterOutlet,ModalController,MenuController,ActionSheetController,PopoverController,ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HomepageService } from './Service/homepage.service';
import { HomePage } from './home/home.page';
import { Router } from '@angular/router';
import {Storage} from '@ionic/storage'
import {BehaviorSubject, Observable, Subscription} from 'rxjs'
import { AuthService } from './Service/auth.service';
import { UserLoginPage } from './user-login/user-login.page';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['../app/app.scss'],
})
export class AppComponent {
  showSubmenu: boolean = false;
  private homepageService: HomepageService;
  @ViewChild(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;
  lastTimeBackPress = 0;
  timePeriodToExit = 2000;
  categories: any;
  authState$: BehaviorSubject<boolean> = new BehaviorSubject(null);
  menuArrayHome= [];
  pages = [
    {
      title: 'Home',
      icon: 'home'
    },
    // {
    //   title: 'Categories',
    //   icon: 'arrow-dropright',
    // },
      {
        title: 'Wishlist',
        icon: 'arrow-dropright'
      }, {
        title: 'Your Orders',
        icon: 'arrow-dropright'
      }, {
        title: 'My Profile',
        icon: 'arrow-dropright'
      },
     {
      title: 'LogOut',
      icon: 'log-out'
    }
  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public events: Events,
    private actionSheetCtrl: ActionSheetController,
    private popoverCtrl: PopoverController,
    private router: Router,
    public modalCtrl: ModalController,
    private toast: ToastController,
    private menu: MenuController,
    private storage: Storage,
    private auth:AuthService
  ) {
    this.menuArrayHome=[];
    //events;
    console.log("events",this.events);
    this.initializeApp();
    this.backButtonEvent();
  }
  authState1$: Observable<boolean>;
  initializeApp() {
    console.log(this.events);
    this.platform.ready().then(() => {
      let token = localStorage.getItem("access_token");
      if (token){
        this.router.navigate(['/cart']);
      } else {
        this.router.navigate(['']);
      }
     // this.auth.checkToken();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.events.subscribe('user:created', (menuArray) => {
        // user and time are the same arguments passed in `events.publish(user, time)`
        // console.log('Welcome', menuArray);
        this.menuArrayHome = menuArray; 
        console.log("menuArrayHome",[this.menuArrayHome])
      });
    });
  }
  backButtonEvent() {
    this.platform.backButton.subscribe(async () => {
        // close action sheet
        try {
            const element = await this.actionSheetCtrl.getTop();
            if (element) {
                element.dismiss();
                return;
            }
        } catch (error) {
        }

        // close popover
        try {
            const element = await this.popoverCtrl.getTop();
            if (element) {
                element.dismiss();
                return;
            }
        } catch (error) {
        }

        // close modal
        try {
            const element = await this.modalCtrl.getTop();
            if (element) {
                element.dismiss();
                return;
            }
        } catch (error) {
            console.log(error);

        }

        // close side menua
        try {
            const element = await this.menu.getOpen();
            if (element) {
                this.menu.close();
                return;

            }

        } catch (error) {

        }
        this.routerOutlets.forEach((outlet: IonRouterOutlet) => {
            if (outlet && outlet.canGoBack()) {
                outlet.pop();

            } else if (this.router.url === '/home') {
                if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
                    // this.platform.exitApp(); // Exit from app
                    navigator['app'].exitApp(); // work in ionic 4

                } else {

                    // this.toast.create(
                    //     `Press back again to exit App.`,
                    //     '2000',
                    //     'center')
                    //     .subscribe(toast => {
                    //         // console.log(JSON.stringify(toast));
                    //     });
                    // this.lastTimeBackPress = new Date().getTime();
                }
            }
        });
    });
}
menuClick(title){
console.log('log out called',title);
if(title == 'LogOut'){
  this.storage.set('access_token',null);
  localStorage.clear();
  // this.authState$.next(false);
  this.router.navigate(['/user-login']);
} else if (title == "Your Orders"){
  this.router.navigate(['/your-orders']);
} else if(title == "Wishlist") {
  this.router.navigate(['/wishlist']);
} else if (title == "My Profile") {
  this.router.navigate(['/profile']);
}
else if(title == "Home") {
  this.router.navigate(['/home']);
} 

}

async presentToast() {
  const toast = await this.toast.create({
    message: 'Press back again to exit App.',
    duration: 2000,
  });
  toast.present();
}
}
