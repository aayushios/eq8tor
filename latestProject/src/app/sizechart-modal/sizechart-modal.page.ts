import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-sizechart-modal',
  templateUrl: './sizechart-modal.page.html',
  styleUrls: ['./sizechart-modal.page.scss'],
})
export class SizechartModalPage implements OnInit {
  modalTitle:string;
  modelId:number;

  constructor(private modalController:ModalController,private navParams:NavParams) {

   }

  ngOnInit() {
    //console.table(this.navParams);
  this.modelId = this.navParams.data.testParam;
  this.modalTitle = this.navParams.data.testurl;

  }
async closeModal(){
  const onClosedData:string = "wrapped up";
  await this.modalController.dismiss(onClosedData); 
}
}
