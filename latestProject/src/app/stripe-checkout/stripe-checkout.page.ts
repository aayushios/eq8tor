import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import {angular} from 'angular-credit-cards'
import { FormGroup, FormBuilder,Validators, NgForm } from '@angular/forms';

@Component({
  selector: 'app-stripe-checkout',
  templateUrl: './stripe-checkout.page.html',
  styleUrls: ['./stripe-checkout.page.scss'],
})
export class StripeCheckoutPage implements OnInit {
  cardNumber = "";
  Month = "";
  Year = "";
  cvv = '';
  editAddress: FormGroup;
  constructor(public navCtrl: NavController,private formBuilder: FormBuilder) { 
    angular.module('Eq8tor', [
      require('angular-credit-cards')
    ]);
    this.editAddress = this.formBuilder.group({
      cardNumber:["", Validators.required],
      Month :['',Validators.required],
      cvv:['',Validators.required],
      Year:['',Validators.required]
    });
  }

  ngOnInit() {
  
  }
  sendDetails(form: NgForm){
    console.log(form)
    console.log(this.cardNumber,this.Month,this.Year,this.cvv);
  }

  goBack() {
    this.navCtrl.back();
  }
}
