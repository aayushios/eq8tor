import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlerServiceService {

  constructor(private alertCtrl:AlertController) { }
  async  myAlertMethod(title: string, message: string, handler: any) {
    let confirm = await this.alertCtrl.create({
      header: title,
      message: message,
      backdropDismiss: false,
      buttons: [
        {
          text: "OK",
          handler: handler
        }
      ]
    });
    await confirm.present();
  }
}
