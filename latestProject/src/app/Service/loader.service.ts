import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular'
import { timeout } from 'q';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
isLoading = false;

  constructor(private loadingCtrl:LoadingController) {}

   async present(){
     this.isLoading = true;
     return await this.loadingCtrl.create({duration:5000,}).then(a=>{
       a.present().then(()=>{
         if(!this.isLoading){
           a.dismiss().then(()=>console.log('abort presenting'));
         }
       });
     });
   }

   async dismiss(){
     this.isLoading = false;
     return await this.loadingCtrl.dismiss().then(()=> console.log('dismissed'));
   }
}
