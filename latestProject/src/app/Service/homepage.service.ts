import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap, delay } from 'rxjs/operators';
import { HomePageData } from '../models/home.model';
import { del } from 'selenium-webdriver/http';

@Injectable({
  providedIn: 'root'
})
export class HomepageService {
  apiUrl = 'https://www.niletechinnovations.com/eq8tor/api/';
  constructor(private http: HttpClient) {

  }

  showCategories(): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'categories', '')
      .pipe(
        tap(_ => this.log('categories')),
      // catchError(this.handleError('login', []))
    );
  }
  showSubCategory(id): Observable<any> {

    return this.http.post<any>(this.apiUrl + 'categories?' + 'parent_id=' + id, '').pipe(tap(_ => this.log('subcategories')), );

  }
  private log(message: string) {
    console.log(message);
  }
  getHomePageData() {
    return this.http.get<HomePageData[]>('https://www.niletechinnovations.com/eq8tor/api/home').pipe(delay(200));
  }

  getCategoryProducts(name) {
    return this.http.post<any>(this.apiUrl + 'cat-products?' + 'slug=' + name, '').pipe(tap(_ => this.log('subcategories')), );

  }
  productDetail(product) {
    return this.http.post<any>(this.apiUrl + 'products/detail?' + 'slug=' + product, '').pipe(tap(_ => this.log('Detail')), )
  }
  searchApi(textSearch) {
    return this.http.post<any>(this.apiUrl + 'search?' + 'srch_term=' + textSearch, '').pipe(tap(_ => this.log('search api call')), );
  }
  loadMoreData(textSearch) {
    return this.http.post<any>(textSearch, '').pipe(tap(_ => this.log('search more api call')), );

  }
  addToWishlist(textSearch) {
    return this.http.post<any>(this.apiUrl + 'products/add/wishlist?' + 'product_id=' + textSearch, '').pipe(tap(_ => this.log('search api call')), );
  }
  getWishlistProducts() {
    return this.http.get<any>(this.apiUrl + 'user/wishlist-content').pipe(delay(100));
  }
  getAllOrders() {
    return this.http.get<any>(this.apiUrl + 'user/orders-content').pipe(delay(100));
  }
  removeFromWishList(productId) {
    return this.http.post<any>(this.apiUrl + 'product/remove/wishlist?' + 'product_id=' + productId, '').pipe(delay(100));
  }
  getCartProducts() {
    return this.http.get<any>(this.apiUrl + 'cart/products').pipe(delay(100));
  }
  addToCart(product_id: any, qty: any) {
    return this.http.post<any>(this.apiUrl + 'product/addtocart?' + 'product_id=' + product_id + '&qty=' + qty, '').pipe(delay(100));
  }
  viewOrderDetails(processId,orderid){
    return this.http.get<any>(this.apiUrl + 'user/order/details?'+ 'order_process_id='+processId +'&order_id='+orderid).pipe(delay(100));
  }
  updateProfile(displayName,Username,emailId,password){
    return this.http.post<any>(this.apiUrl +'user/profile/update?'+'display_name='+displayName+'&user_name='+Username +'&email_id='+emailId+'&password='+password,'').pipe(delay(100));
  }
  updateAddress(billFirstName,billLastName,billEmailAdd,billPhoneNumber,billCountry,billAddress1,billTownCity,billPostalCode,shipFirstName,shipLastName,shipEmailAddress,shipPhoneNumber,shipCountry,shipAddress1,shipCity,shipPostalcode){
    return this.http.post<any>(this.apiUrl + 'user/address?'+'account_bill_first_name='+billFirstName +'&account_bill_last_name='+billLastName + '&account_bill_email_address='+billEmailAdd +'&account_bill_phone_number='+billPhoneNumber +'&account_bill_select_country='+billCountry+'&account_bill_adddress_line_1='+billAddress1+'&account_bill_town_or_city='+billTownCity+'&account_bill_zip_or_postal_code='+billPostalCode+'&account_shipping_first_name='+shipFirstName +'&account_shipping_last_name='+shipLastName+'&account_shipping_email_address='+shipEmailAddress+'&account_shipping_phone_number='+shipPhoneNumber+'&account_shipping_select_country='+shipCountry+'&account_shipping_adddress_line_1='+shipAddress1+'&account_shipping_town_or_city='+shipCity+'&account_shipping_zip_or_postal_code='+shipPostalcode,'').pipe(delay(100));
  }
  updateAddressPost(data){
    return this.http.post<any>(this.apiUrl + 'user/address',data).pipe(delay(100));
  }
  getCountries(){
    return this.http.get<any>(this.apiUrl +'country/lists').pipe(delay(100));
  }
}
