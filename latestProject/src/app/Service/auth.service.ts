import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Options } from 'selenium-webdriver/ie';
import { Platform } from '@ionic/angular';
import {Storage} from '@ionic/storage'
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public apiUrl = 'https://www.niletechinnovations.com/eq8tor/api/';
  
  authState$= new Subject<any>();
  constructor(private http: HttpClient,private platform:Platform,private router:Router,private storage: Storage) {
    this.platform.ready().then( _ => {
      //this.checkToken();
    })
  }
  headers = new HttpHeaders({
    "Accept": 'application/json',
    'Content-Type': 'application/json'
  });

  login(data): Observable<any> {
    console.log(data)
    return this.http.post<any>(this.apiUrl + 'login', data, { headers: this.headers })
      .pipe(
        tap(_ => {this.log('login')}),
      // catchError(this.handleError('login', []))
    );
  }
  // public checkToken() {
  //   console.log("inside check token")
  //   this.storage.get('access_token').then(res => {
  //     if (res) {
  //       console.log(res);
  //       this.authState$.next(true);
  //     } else {
  //       this.router.navigate(['/user-login']); 
  //     }
  //   })
  // }

  public getAuthStateObserver(): Observable<boolean> {

    return this.authState$.asObservable();
  }

  // public isAuthenticated() {
  //   return this.authState$.;
  // }

  logout(): Observable<any> {
    return this.http.get<any>(this.apiUrl + 'logout')
      .pipe(
        tap(_ => this.log('logout')),
        catchError(this.handleError('logout', []))
      );
      this.storage.set('access_token', null);
  }

  forgot(data): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'forget-password', data).pipe(
      tap(_ => this.log('forgot password')),
    );
  }

  register(data): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'register', data)
      .pipe(
        tap(_ => this.log('signup'))
        // catchError(this.handleError('register Failed',[]))
      );
  }
  profile(): Observable<any> {

    return this.http.get<any>(this.apiUrl + 'user')
      .pipe(
        tap(_ => this.log('user'))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error);
      console.log(error.error.message) // log to console instead
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    console.log(message);
  }
}
