import { TestBed } from '@angular/core/testing';

import { AlerServiceService } from './aler-service.service';

describe('AlerServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AlerServiceService = TestBed.get(AlerServiceService);
    expect(service).toBeTruthy();
  });
});
