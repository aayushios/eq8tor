import { Component, ViewChild } from '@angular/core';
import { NavController, ToastController, IonSlides, Events } from '@ionic/angular';
import { Router, RouterEvent } from '@angular/router'

import { IonicSelectableModule, IonicSelectableComponent } from 'ionic-selectable';
import { UserServiceService } from '../Service/user-service.service';
import { User } from '../models/user.model';
import { CartService } from '../Service/cart.service';
import { HomepageService } from '../Service/homepage.service';
import { AlerServiceService } from '../Service/aler-service.service';
import { LoaderService } from '../Service/loader.service';
import { HomePageData } from '../models/home.model';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  sliderConfig = {
    initialSlide: 0,
    speed: 400,
    slidesPerView: 2,
    autoplay: false,
  };
  bannerSlider = {
    initialSlide: 0,
    speed: 2000,
    slidesPerView: 1,
    autoplay: true,
  };
  selectedPath = '';
  textToSearch = '';
  @ViewChild(IonSlides) slides: IonSlides;
  @ViewChild('mySelect') selectComponent: IonicSelectableComponent;
  users: User[] = [];
  homePageModel: HomePageData[] = [];
  featuredItems: any;
  latestProducts: any;
  bannerList: any;
  public selectedCategory: any[] = [];
  dynamicColor: string
  categories: any;
  public showLeftButton: boolean;
  public showRightButton: boolean;
  public wishListAdded: boolean = false;
  // menuArray = [];
  cart = [];
  items = [];
  liked: boolean = false;
  constructor(public navCtrl: NavController, public events: Events, public loadingService: LoaderService, public toastController: ToastController, private alert: AlerServiceService, private homepageService: HomepageService, private cartService: CartService, private router: Router, private toastCtrl: ToastController, private service: UserServiceService) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
    });
    this.dynamicColor = 'redish';
    this.showCategories();
    this.getHomePage();
    this.showCategories123();
    
  }

  openSearchPage() {
    this.router.navigate(['searchpage']);
  }

  ngOnInit() {
    this.items = this.cartService.getProducts();
    this.cart = this.cartService.getCart();
    this.getCartContent();
  }

  addToCart(product) {
    this.cartService.addProduct(product);
    console.log(product);
  }
  RefreshPage() {
    this.showCategories();
    this.getHomePage();
  }

  openCart() {
    this.router.navigate(['cart']);
  }

  userChanged(event: { component: IonicSelectableModule, value: any }) {
    console.log('event', event);
  }

  goToProductDetail(product) {
    this.router.navigate(['product-detail'], { queryParams: { 'id': product } });
    console.log("response product", product);
  }
  goBack() {
    this.navCtrl.back();
  }

  getUsers() {
    console.log("resppppppp");
    this.service.getUsers()
      .subscribe(resp => {
        console.log("resppppppp", resp);
        // this.users = resp
      }, (err) => { console.log(err) });
  }
  like(productId, id) {
    console.log(productId);
    this.homepageService.addToWishlist(productId).subscribe(resp => {
      console.log(resp);
      this.presentToast(resp.message);
      this.latestProducts[id]._wishlist = true;
    }, (err) => {
      console.log(err);
      this.presentToast(err.message);
    });
  }

  likeFeatured(productId, id) {
    this.homepageService.addToWishlist(productId).subscribe(resp => {
      console.log(resp);
      this.presentToast(resp.message);
      this.featuredItems[id]._wishlist = true;
    }, (err) => {
      console.log(err);
      this.presentToast(err.message);
    });
  }
  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }


  getHomePage() {
    this.loadingService.present();
    this.homepageService.getHomePageData().subscribe(response => {
      this.homePageModel = response['data'];
      this.featuredItems = this.homePageModel["features_items"];
      this.latestProducts = this.homePageModel["latest_items"];
      console.log("featuredItems",this.featuredItems);
      // for(let i = 0;i < this.latestProducts.length; i++){
      //   this.wishListAdded = this.latestProducts[i]._wishlist;
      // }

      this.bannerList = this.homePageModel["slider"];
      this.loadingService.dismiss();
    }, (err) => {
      console.log(err);
      this.alert.myAlertMethod("Network Error", err.error.message, data => {
        console.log("hello alert")
      });
      this.loadingService.dismiss();
    })
  }
  userSearch(event) {
    this.getUsers();
    const textSearch = event.target.value;
    this.textToSearch = textSearch;
    console.log(textSearch);
  }

  getCartContent() {
    console.log("getCartContent");

    this.homepageService.getCartProducts().subscribe(resp => {
      console.log("getCartContent",JSON.stringify(resp));
    }, err => {
      console.log("getCartContent error",err);
    });
  }

  public showCategories() {
    this.loadingService.present();
    this.homepageService.showCategories().subscribe(resp => {
      console.log("resp", resp)
      let data = resp.data.categories;
      this.categories = data;
      this.loadingService.dismiss();
    }, (err) => {
      console.log(err);
      this.alert.myAlertMethod("Network Error", err.error.message, data => {
        console.log("hello alert")
      });
      this.loadingService.dismiss();
    });
  }

  public slideChanged(): void {
    const currentIndex = this.slides.getActiveIndex();
    // this.showLeftButton = currentIndex !=== 0;
    // this.showRightButton = currentIndex !=== Math.ceil(this.slides.length['length'] / 3);
  }

  // Method that shows the next slide
  public slideNext(): void {
    this.slides.slideNext();
  }

  // Method that shows the previous slide
  public slidePrev(): void {
    this.slides.slidePrev();
  }
  ionViewDidEnter() {
    document.removeEventListener("backbutton", function (e) {
      console.log("disable back button")
    }, true);
  }

  categoryClick(text, name) {
    console.log("hello", text);
    this.router.navigate(['subcategory'], { queryParams: { 'id': text, 'title': name }, });
  }

  showCategories123() {
    this.homepageService.showCategories().subscribe(resp => {
      let menuArray = [];
      let data = resp.data.categories;
      this.categories = data;
      for (let i = 0; i < this.categories.length; i++) {
        console.log(this.categories[i]);
        menuArray.push({
          "title": this.categories[i].name,
          "icon": "arrow-dropright"
        });
      }
      this.events.publish('user:created', menuArray);
      console.log(menuArray);
    }, (err) => {
      console.log(err);
    });
  }
}
