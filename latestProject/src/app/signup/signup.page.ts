import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { AuthService } from '../Service/auth.service';
import { AlerServiceService } from '../Service/aler-service.service';
import { NavController } from '@ionic/angular';
import { LoaderService } from '../Service/loader.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  signUpForm: FormGroup;


  constructor(private formBuilder: FormBuilder,private router: Router, public loadingService: LoaderService, private navCtrl: NavController, private authService: AuthService, private alertService: AlerServiceService) {
    this.signUpForm = this.formBuilder.group({
      email_id: ["", Validators.required],
      password: ["", Validators.required],
      user_name: ["", Validators.required],
      user_display_name: ["", Validators.required],
      password_confirmation: ["", Validators.required]
    });
  }

  ngOnInit() {

  }

  onFormSubmit(form: NgForm) {
    this.loadingService.present();
    this.authService.register(form)
      .subscribe(successResponse => {
        console.log("success");
        this.alertService.myAlertMethod(successResponse.message, "", data => {
          console.log("error")
          this.router.navigate(['home']);
        });
        this.loadingService.dismiss();
      }, (err) => {
        console.log(err);
        this.alertService.myAlertMethod("Error", err.error.message, data => {
          console.log("error")
        });
        this.loadingService.dismiss();
      });
  }
  goBack() {
    this.navCtrl.back();
  }
}
