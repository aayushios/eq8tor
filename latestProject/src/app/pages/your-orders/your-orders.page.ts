import { Component, OnInit } from '@angular/core';
import { HomepageService } from '../../Service/homepage.service';
import { LoaderService } from '../../Service/loader.service';
import { NavController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { OrderDetailsPage } from '../../order-details/order-details.page';

@Component({
  selector: 'app-your-orders',
  templateUrl: './your-orders.page.html',
  styleUrls: ['./your-orders.page.scss'],
})
export class YourOrdersPage implements OnInit {

  ordersArray: any;

  constructor(private homepageService: HomepageService, private router: Router, private navCtrl: NavController, private loadingService: LoaderService,private modalController:ModalController) {
    this.getAllOrders();
  }

  ngOnInit() {

  }
  
  async viewOrderDetails(order) {
    const modal = await this.modalController.create({
      component: OrderDetailsPage,
      componentProps: {
        "process": order._order_process_key,
        "id": order._post_id
      }
    });
    
    modal.onDidDismiss().then((sizeChartResponse) => {
      if (sizeChartResponse !== null) {
        //this.sizeChartResponse = sizeChartResponse.data;
      }
    });
    return await modal.present();
  }
  getAllOrders() {
    this.loadingService.present();
    this.homepageService.getAllOrders().subscribe(resp => {
      this.ordersArray = resp.data.orders_list_data;
      console.log(this.ordersArray);
      this.loadingService.dismiss();
    }, (err) => {
      console.log(err);
      this.loadingService.dismiss();
    });
  }
  goBack() {
    this.navCtrl.back();
  }
}
