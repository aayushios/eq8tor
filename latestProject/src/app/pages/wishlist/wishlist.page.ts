import { Component, OnInit } from '@angular/core';
import { HomepageService } from '../../Service/homepage.service';
import { CartService } from '../../Service/cart.service';
import { LoaderService } from '../../Service/loader.service';
import { AlerServiceService } from '../../Service/aler-service.service';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.page.html',
  styleUrls: ['./wishlist.page.scss'],
})
export class WishlistPage implements OnInit {

  wishListProductsArray: any;
  cart = [];
  constructor(public homePageService: HomepageService, private alert: AlerServiceService,private loadingService: LoaderService, public cartService: CartService) {
    this.getWishlist();
  }

  ngOnInit() {
    this.cart = this.cartService.getCart();
  }

  getWishlist() {
    this.loadingService.present();
    this.homePageService.getWishlistProducts().subscribe(resp => {
      this.wishListProductsArray = resp.data.wishlist;
      console.log(this.wishListProductsArray)
      this.loadingService.dismiss();
    },
      (err) => {
        console.log("error", err);
        this.loadingService.dismiss();
      })
  }

  addToCart(product) {
    this.cartService.addProduct(product);
    console.log(product);
  }
  removeAction(product) {
    this.loadingService.present();
    this.homePageService.removeFromWishList(product.id).subscribe(resp => {
      console.log(resp.success);
      if (resp.success == true) {
        let index = this.wishListProductsArray.indexOf(product);
        if (index > -1) {
          this.wishListProductsArray.splice(index, 1);
        }
      }
      this.loadingService.dismiss();
    }, (err) => {
      console.log("error", err);
      this.loadingService.dismiss();
      this.alert.myAlertMethod("Network Error", err.message, data => {
        console.log("wishlist remove error")
      });
    })
  }
}
