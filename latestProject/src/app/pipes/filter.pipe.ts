import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../models/user.model';
import { Results } from '../models/search.model';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(users : Results[], texto: string): Results[] {
    if (texto.length === 0){
      return users;
    }
    texto = texto.toLocaleLowerCase();

    return users.filter(user =>{
      return user['title'].toLocaleLowerCase().includes(texto);
    })
  }
}
