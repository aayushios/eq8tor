import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../Service/loader.service';
import { HomepageService } from '../Service/homepage.service';
import { NavController, ModalController } from '@ionic/angular';
import { EditAddressPage } from '../edit-address/edit-address.page';

@Component({
  selector: 'app-address',
  templateUrl: './address.page.html',
  styleUrls: ['./address.page.scss'],
})
export class AddressPage implements OnInit {

  billCompanyName = "";
  billAddress = "";
  billCity = "";
  billPostCode = "";
  billCountry = "";
  billPhone = "";
  billEmail = "";
  billingAddressFirstName = "";
  billingAddressLastName = "";
  shipCompanyName = "";
  shipAddress = "";
  shipCity = "";
  shipPostCode = "";
  shipCountry = "";
  shipPhone = "";
  shipEmail = "";
  shippingAddressFirstName = "";
  shippingAddressLastName = "";

  constructor(public homePageService: HomepageService, private modalController: ModalController, private loadingService: LoaderService, private navCtrl: NavController) {
    this.getWishlist();
  }

  ngOnInit() {
  }

  goBack() {
    this.navCtrl.back();
  }

  getWishlist() {
    this.loadingService.present();
    this.homePageService.getWishlistProducts().subscribe(resp => {

      this.billCompanyName = resp.data.address_details.account_bill_company_name;
      this.billAddress = resp.data.address_details.account_bill_adddress_line_1;
      this.billCity = resp.data.address_details.account_bill_title;
      this.billPostCode = resp.data.address_details.account_bill_zip_or_postal_code;
      this.billCountry = resp.data.address_details.account_bill_select_country;
      this.billPhone = resp.data.address_details.account_bill_phone_number;
      this.billEmail = resp.data.address_details.account_bill_email_address;
      this.billingAddressFirstName = resp.data.address_details.account_bill_first_name;
      this.billingAddressLastName = resp.data.address_details.account_bill_last_name;

      this.shipCompanyName = resp.data.address_details.account_shipping_company_name;
      this.shipAddress = resp.data.address_details.account_shipping_adddress_line_1;
      this.shipCity = resp.data.address_details.account_shipping_title;
      this.shipPostCode = resp.data.address_details.account_shipping_zip_or_postal_code;
      this.shipCountry = resp.data.address_details.account_shipping_select_country;
      this.shipPhone = resp.data.address_details.account_shipping_phone_number;
      this.shipEmail = resp.data.address_details.account_shipping_email_address;
      this.shippingAddressFirstName = resp.data.address_details.account_shipping_first_name;
      this.shippingAddressLastName = resp.data.address_details.account_shipping_last_name;
      this.loadingService.dismiss();
    },
      (err) => {
        console.log("error", err);
        this.loadingService.dismiss();
      });
  }
  async editAddress() {
    const modal = await this.modalController.create({
      component: EditAddressPage,
    });

    modal.onDidDismiss().then((sizeChartResponse) => {
      if (sizeChartResponse !== null) {
        //this.sizeChartResponse = sizeChartResponse.data;
      }
    });
    return await modal.present();
  }
}
