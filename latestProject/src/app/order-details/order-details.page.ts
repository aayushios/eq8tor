import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, RouterEvent } from '@angular/router';
import { HomepageService } from '../Service/homepage.service';
import { LoaderService } from '../Service/loader.service';
import { NavController, ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.page.html',
  styleUrls: ['./order-details.page.scss'],
})
export class OrderDetailsPage implements OnInit {
  productID: string;
  processID: string;
  data: any;
  constructor(private route: ActivatedRoute, private navParams: NavParams, public modalController: ModalController, public navCtrl: NavController, private loadingService: LoaderService, private homePageService: HomepageService) {
    this.route.queryParams.subscribe((event: RouterEvent) => {
      console.log(event);
    })
  }

  ngOnInit() {
    this.productID = this.navParams.data.id;
    this.processID = this.navParams.data.process;
    this.getOrderDetails()
  }
  goBack() {
    this.navCtrl.back();
  }
  getOrderDetails() {
    this.loadingService.present();
    this.homePageService.viewOrderDetails(this.processID, this.productID).subscribe(resp => {

      this.data = resp.data.order_details_by_order_id;
      console.log(this.data);
      this.loadingService.present();
    }, (err) => {
      console.log("order details", err);
      this.loadingService.present();
    })
  }
  async closeModal() {
    const onClosedData: string = "wrapped up";
    await this.modalController.dismiss(onClosedData);
  }
}
