import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, ToastController, NavParams } from '@ionic/angular';
import { HomepageService } from '../Service/homepage.service';
import { AlerServiceService } from '../Service/aler-service.service';
import { LoaderService } from '../Service/loader.service';
import { HomePageData } from '../models/home.model';

@Component({
  selector: 'app-subcategory',
  templateUrl: './subcategory.page.html',
  styleUrls: ['./subcategory.page.scss'],
})
export class SubcategoryPage implements OnInit {

  data: any;
  subCategoryArray: any;
  checkData: HomePageData[];
  productsArray: any;
  headerTitle: string;

  constructor(private route: ActivatedRoute, private loadingService: LoaderService, private homepageService: HomepageService, private router: Router, private alert: AlerServiceService, public navCtrl: NavController) {

    // console.log("home page data",HomePageData);
  }

  goBack() {
    this.navCtrl.back();
  }

  getSubCategory(catID) {
    this.loadingService.present();
    this.homepageService.getCategoryProducts(catID).subscribe(response => {
      let resp = response.data;
      this.subCategoryArray = resp.categories;
      console.log("this.subCategoryArray",this.subCategoryArray);
      this.productsArray = resp.products;
      this.loadingService.dismiss();
    }, (err) => {
      console.log(err);
      this.alert.myAlertMethod(err.message, "Please try again later.", data => {
        console.log("hello getSubCategory")
      });
      this.loadingService.dismiss();
    });
  }


  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      console.log(params.id);
      this.headerTitle = params.title;
      this.getSubCategory(params.id)
    });
  }

  showProducts(itemName) {
    console.log("showProducts", itemName);
    this.router.navigate(['productlisting'], { queryParams: { 'id': itemName } });
  }
  goToProductDetail(product) {
    this.router.navigate(['product-detail'], { queryParams: { 'id': product } });
    console.log("response product", product);
  }
  filterProduct(product) {
    console.log(product)
    this.getSubCategoryFilter(product);
  }
  
  getSubCategoryFilter(catID) {
    this.loadingService.present();
    this.homepageService.getCategoryProducts(catID).subscribe(response => {
      let resp = response.data;
      this.productsArray = resp.products;
      this.loadingService.dismiss();
    }, (err) => {
      console.log(err);
      this.alert.myAlertMethod(err.message, "Please try again later.", data => {
        console.log("hello getSubCategory")
      });
      this.loadingService.dismiss();
    });
  }
}
